<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dashboard extends CI_Model {

  public function get_cust_ditangani(){
    if ($this->session->userdata('tipene') == 2) {
    $this->db->select('0_3_user_cust.*, 0_1_data_customer.*');
    $this->db->from('0_1_data_customer');
    $this->db->join('0_3_user_cust', '0_3_user_cust.NO_ID_CUST = 0_1_data_customer.NO_ID_CUSTOMER');
    $this->db->where('0_3_user_cust.NO_ID_USR',$this->session->userdata('id'));
    return $this->db->get();
    } else {
    return $this->db->get('0_1_data_customer');
    }
  }
  
}
