<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BahanCetak extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('Ciqrcode');
		$this->load->library('Zend');
	}
	public function penyebut($nilai) {
			$nilai = abs($nilai);
			$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
			$temp = "";
			if ($nilai < 12) {
				$temp = " ". $huruf[$nilai];
			} else if ($nilai <20) {
				$temp = penyebut($nilai - 10). " belas";
			} else if ($nilai < 100) {
				$temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
			} else if ($nilai < 200) {
				$temp = " seratus" . penyebut($nilai - 100);
			} else if ($nilai < 1000) {
				$temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
			} else if ($nilai < 2000) {
				$temp = " seribu" . penyebut($nilai - 1000);
			} else if ($nilai < 1000000) {
				$temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
			} else if ($nilai < 1000000000) {
				$temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
			} else if ($nilai < 1000000000000) {
				$temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
			} else if ($nilai < 1000000000000000) {
				$temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
			}
			return $temp;
		}

		public function penyebutBulan($nilai) {
			$nilai = abs($nilai);
			$huruf = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
			$temp = "";
			if ($nilai == 1) {
				$temp = $huruf[1];
			}else if ($nilai == 2) {
				$temp = $huruf[2];
			}elseif ($nilai == 3) {
				$temp = $huruf[3];
			}elseif ($nilai == 4) {
				$temp = $huruf[4];
			}elseif ($nilai == 5) {
				$temp = $huruf[5];
			}elseif ($nilai == 6) {
				$temp = $huruf[6];
			}elseif ($nilai == 7) {
				$temp = $huruf[7];
			}elseif ($nilai == 8) {
				$temp = $huruf[8];
			}else {
				$temp = "sadasdas";
			}
			return $temp;
		}

		public function terbilang($nilai) {
			if($nilai<0) {
				$hasil = "minus ". trim(penyebut($nilai));
			} else {
				$hasil = trim(penyebut($nilai));
			}
			return $hasil;
		}

		public function terbilangBulan($nilai) {
			if($nilai<0) {
				$hasil = "minus ". trim(penyebutBulan($nilai));
			} else {
				$hasil = trim(penyebutBulan($nilai));
			}
			return $hasil;
		}


		public function hari_ini(){
			$hari = date ("D");

			switch($hari){
				case 'Sun':
				$hari_ini = "Minggu";
				break;

				case 'Mon':
				$hari_ini = "Senin";
				break;

				case 'Tue':
				$hari_ini = "Selasa";
				break;

				case 'Wed':
				$hari_ini = "Rabu";
				break;

				case 'Thu':
				$hari_ini = "Kamis";
				break;

				case 'Fri':
				$hari_ini = "Jumat";
				break;

				case 'Sat':
				$hari_ini = "Sabtu";
				break;

				default:
				$hari_ini = "Tidak di ketahui";
				break;
			}
			return $hari_ini;
		}
}
