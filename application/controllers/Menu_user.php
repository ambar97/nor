<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_user extends CI_Controller {

	public function index()
	{
		// $data['isi'] = $this->M_model->selectwhere('0_user');
		$data['user'] = $this->M_model->selectwhere('0_user',array('id_insert'=>$this->session->userdata('id')));
		$this->load->view('user/v_user',$data);
	}

	public function hapus($id){
		$where = array('ID_USER'=>$id);
		$this -> M_model -> delete($where,'0_user');
		header('location:'.base_url('Menu_user'));
	}
	function getBulan($bln){
		switch ($bln){
		 case 1:
		  return "Januari";
		  break;
		 case 2:
		  return "Februari";
		  break;
		 case 3:
		  return "Maret";
		  break;
		 case 4:
		  return "April";
		  break;
		 case 5:
		  return "Mei";
		  break;
		 case 6:
		  return "Juni";
		  break;
		 case 7:
		  return "Juli";
		  break;
		 case 8:
		  return "Agustus";
		  break;
		 case 9:
		  return "September";
		  break;
		 case 10:
		  return "Oktober";
		  break;
		 case 11:
		  return "November";
		  break;
		 case 12:
		  return "Desember";
		  break;
		}
	   }
	   function bln_aja($bulan_a){
		$bulan = $this->getBulan(substr($bulan_a,5,2));
		return $bulan;  
	  } 

	public function insertNU(){
		$namauser = $this->input->post('nmUser');
		$alamat = $this->input->post('alamat');
		$tmpat = $this->input->post('tempat_lahir');
		$tgllahiran = $this->input->post('tgl_lahir');
		$isiketerangan = 'Nyonya '.$namauser.' lahir di '.$tmpat.', tanggal '.substr($tgllahiran,8,2).' '.$this->bln_aja($tgllahiran).' '.substr($tgllahiran,0,4).', Warga Negara Indonesia, Karyawan Notaris, bertempat tinggal di '.$tmpat;
		// die(var_dump($isiketerangan));
		$data = array('NAMA_USER' =>$namauser ,
					'LOGIN_USER'=>$this->input->post('lgnUser'),
					'PSWD_USER'=>md5($this->input->post('psw')),
					'STATUS_LOGIN'=>0,
					'ADDRS_USER'=>$alamat,
					'BSCSLR_USER'=>0,
					'PHONE_USER'=>$this->input->post('telp'),
					'STATUS_USER'=>'AKTIF',
					'id_insert'=>$this->session->userdata('id'),
					'TYPE_USER'=>$this->input->post('type'),
					'tempat_lahir'=>$tmpat,
					'tanggal_lahir'=>$tgllahiran,
					'keterangan'=>$isiketerangan);
		$this->M_model->insert('0_user',$data);
		redirect(base_url('Menu_user'));
	}

	public function update(){
		$data = array('NAMA_USER' =>$this->input->post('nmUser') ,
					'LOGIN_USER'=>$this->input->post('lgnUser'),
					'PSWD_USER'=>md5($this->input->post('psw')),
					'STATUS_LOGIN'=>0,
					'ADDRS_USER'=>$this->input->post('alamat'),
					'BSCSLR_USER'=>0,
					'PHONE_USER'=>$this->input->post('telp'),
					'tempat_lahir'=>$this->input->post('tempat_lahir'),
					'tanggal_lahir'=>$this->input->post('tgl_lahir'));
				$this->db->update('0_user',$data, array('ID_USER' =>$this->input->post('idNe')));
				redirect(base_url('Menu_user'));
	}

	public function inputMenuUser(){
		$id = $this->uri->segment(3);
		$data['user'] = $this->M_model->selectwhere('0_user', array('ID_USER'=>$id));
		$data['menu'] = $this->M_model->select('menu');
		$data['member_menu'] = $this->M_model->selectwhere('member_menu', array('MEMBER_ID'=>$id));
		$this->load->view('user/input_menu_user', $data);
	}

	public function prosesInputMenuUser(){
		$id = $this->input->post('MEMBER_ID');
		$data = array(
			'MEMBER_ID'=>$id,
			'MENU_ID'=>$this->input->post('MENU_ID')
		);
		$this->M_model->insert('member_menu',$data);
		return redirect(base_url('Menu_user/inputMenuUser/'.$id));
	}

	public function prosesHapusMenuUser(){
		$id_user = $this->uri->segment(3);
		$id_member_menu = $this->uri->segment(4);
		$this->M_model->delete(array('MEMBER_ID'=>$id_user, 'NO_ID_MEMBER_MENU'=>$id_member_menu), 'member_menu');
		return redirect(base_url('Menu_user/inputMenuUser/'.$id_user));
	}
}
