<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
			parent::__construct();
			$this->load->model('M_dashboard');
		}
	public function index(){
		$data['customer'] = $this->M_dashboard->get_cust_ditangani()->result();
		$this->load->view('home.php',$data);
	}

	function profile(){
		$data['usr'] = $this->db->get_where('0_user', array('ID_USER' =>$this->session->userdata('id') ));
		$data['error_password'] = "";
		$this->load->view('profile',$data);
	}

	function perbarui(){
		$data = array('LOGIN_USER'=>$this->input->post('LOGIN_USER'),
									// 'PSWD_USER'=>md5($this->input->post('pws'))
			'NAMA_USER'=>$this->input->post('NAMA_USER'),
			'ADDRS_USER'=>$this->input->post('ADDRS_USER'),
			'PHONE_USER'=>$this->input->post('PHONE_USER')
		);
		$this->db->update('0_user',$data,array('ID_USER' =>$this->session->userdata('id')));
		redirect(base_url('Home/profile'));
	}

	function perbarui_password(){
		$password_lama = $this->input->post('password_lama');
		$password_baru = $this->input->post('password_baru');
		if ($password_lama == $password_baru) {
			$data = array('PSWD_USER'=>md5($password_baru));
			$this->db->update('0_user', $data, array('ID_USER' =>$this->session->userdata('id')));
			$data['error_password'] = '
			<div class="alert alert-success">
			<strong>Sukses!</strong> Password sudah diperbarui.
			</div>
			';
		}else {
			$data['error_password'] = '
			<div class="alert alert-danger">
			<strong>Peringatan!</strong> Password tidak cocok. Silahkan coba kembali.
			</div>
			';
		}
		$data['usr'] = $this->db->get_where('0_user', array('ID_USER' =>$this->session->userdata('id') ));
		$this->load->view('profile',$data);
	}

	function upload(){
		$id = $this->input->post('ID_USER');
		$old_photo = $this->input->post('PICT_USER');

		$config['upload_path']          = './gallery/photo_profile';
		$config['allowed_types']        = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
		$config['file_name']						= 'foto_'.$id;

		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('userfile'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo $config['upload_path'];
			echo json_encode($error);
            // $this->load->view('upload_form', $error);
		}
		else
		{
			unlink('./gallery/photo_profile/'.$old_photo);
						// unlink("./gallery/photo_profile/foto_00001.jpg");
			$fileName = $this->upload->data();
			$ext = $fileName['file_ext'];
			$fileName = $fileName['file_name'];
			$this->db->update('0_user', array('PICT_USER'=>$fileName), array('ID_USER'=>$id));
			return redirect(base_url('Home/profile'));
		}
	}
}
