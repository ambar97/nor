<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saksi extends CI_Controller {

	public function index()
	{
		// $data['isi'] = $this->M_model->selectwhere('0_user');
		$data['saksi'] = $this->M_model->select('0_2_data_saksi');
		$this->load->view('saksi/v_saksi',$data);
	}

	public function hapus($id){
		$where = array('ID_USER'=>$id);
		$this -> M_model -> delete($where,'0_user');
		header('location:'.base_url('Menu_user'));
	}

	public function insertNU(){
		$data = array('NAMA_USER' =>$this->input->post('nmUser') ,
					'LOGIN_USER'=>$this->input->post('lgnUser'),
					'PSWD_USER'=>md5($this->input->post('psw')),
					'STATUS_LOGIN'=>0,
					'ADDRS_USER'=>$this->input->post('alamat'),
					'BSCSLR_USER'=>0,
					'PHONE_USER'=>$this->input->post('telp'));
		$this->M_model->insert('0_user',$data);
		redirect(base_url('Menu_user'));
	}

	public function update(){
		$data = array('NAMA_USER' =>$this->input->post('nmUser') ,
					'LOGIN_USER'=>$this->input->post('lgnUser'),
					'PSWD_USER'=>$this->input->post('psw'),
					'STATUS_LOGIN'=>0,
					'ADDRS_USER'=>$this->input->post('alamat'),
					'BSCSLR_USER'=>0,
					'PHONE_USER'=>$this->input->post('telp'));
	}

	public function inputMenuUser(){
		$id = $this->uri->segment(3);
		$data['user'] = $this->M_model->selectwhere('0_user', array('ID_USER'=>$id));
		$data['menu'] = $this->M_model->select('menu');
		$data['member_menu'] = $this->M_model->selectwhere('member_menu', array('MEMBER_ID'=>$id));
		$this->load->view('user/input_menu_user', $data);
	}

	public function prosesInputMenuUser(){
		$id = $this->input->post('MEMBER_ID');
		$data = array(
			'MEMBER_ID'=>$id,
			'MENU_ID'=>$this->input->post('MENU_ID')
		);
		$this->M_model->insert('member_menu',$data);
		return redirect(base_url('Menu_user/inputMenuUser/'.$id));
	}

	public function prosesHapusMenuUser(){
		$id_user = $this->uri->segment(3);
		$id_member_menu = $this->uri->segment(4);
		$this->M_model->delete(array('MEMBER_ID'=>$id_user, 'NO_ID_MEMBER_MENU'=>$id_member_menu), 'member_menu');
		return redirect(base_url('Menu_user/inputMenuUser/'.$id_user));
	}
}
