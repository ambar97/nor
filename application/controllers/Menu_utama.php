<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_utama extends CI_Controller {

	public function index()
	{
		$data['menu']=$this->M_model->select('menu')->result();
		// $hh = $this->M_model->ambyar()->result();
		// die(var_dump($hh));
		$data['data']= $this->M_model->selectwhereorder('menu',array('parent_id'=>0),'menu_order','ASC')->result();
		$this->load->view('menu/v_menu_utama',$data);
	}

	public function prosesEditMenu(){
		$data = array(
			'id' => $this->input->post('id_menu'),
			'parent_id' => $this->input->post('parent_id'),
			'title' => $this->input->post('nm_menu'),
			'url' => $this->input->post('almt_menu'),
			'menu_order' => $this->input->post('nmr_urut'),
			'icon' => $this->input->post('nm_icon')
		);
		$this->M_model->update('menu',$data,array('id'=>$this->input->post('id_menu')));
		return redirect(base_url('Menu_utama'));
	}

	public function tambah(){
		$data = array('parent_id' =>$this->input->post('parent'),
						'title'=>$this->input->post('nm_menu'),
						'url'=>$this->input->post('almt_menu'),
						'menu_order'=>$this->input->post('nmr_urut'),
						'icon'=>$this->input->post('nm_icon'));
		$this->M_model->insert('menu',$data);
		redirect(base_url('Menu_utama'));
	}


	public function hapus(){
		$where = array('id'=>$this->input->post('id'));
		$this->M_model->delete($where,'menu');
		header('location:'.base_url('Menu_utama/subMenu/'.$this->input->post('parent_id')));
	}
	public function hapusmenu($id){
		$where = array('id'=>$id);
		$this->M_model->delete($where,'menu');
		redirect(base_url('Menu_utama'));
}
	public function subMenu(){
		$id = $this->uri->segment('3');
		$data['sub']=$this->M_model->selectwhere('menu',array('parent_id'=>$id))->result();
		$data['id'] = $this->M_model->selectwhere('menu',array('id'=>$id))->result();
		$this->load->view('menu/v_sub_menu',$data);
	}

	public function prosesTambahSubMenu(){
		$data = array(
			'parent_id' => $this->input->post('parent_id'),
			'title' => $this->input->post('nm_menu'),
			'url' => $this->input->post('almt_menu'),
			'menu_order' => $this->input->post('nmr_urut'),
			'icon' => $this->input->post('nm_icon')
		);
		$this->M_model->insert('menu', $data);
		return redirect(base_url('Menu_utama/subMenu/'.$this->input->post('parent_id')));
	}

	public function prosesEditSubMenu(){
		$data = array(
			'id' => $this->input->post('id_menu'),
			'parent_id' => $this->input->post('parent_id'),
			'title' => $this->input->post('nm_menu'),
			'url' => $this->input->post('almt_menu'),
			'menu_order' => $this->input->post('nmr_urut'),
			'icon' => $this->input->post('nm_icon')
		);
		$this->M_model->update('menu',$data,array('id'=>$this->input->post('id_menu')));
		return redirect(base_url('Menu_utama'));
	}
}
