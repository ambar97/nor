<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'vendor/autoload.php';
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Writer\Word2007;

class Word extends CI_Controller {

	public function index()
	{
		$phpWord = new PhpWord();
		$phpWord->setDefaultFontName('Bookman Old Style');
		$phpWord->setDefaultFontSize(12);

		$phpWord->addParagraphStyle(
			'pStyle',
			array(
				'spaceAfter'=>150,
			)
        ); // Style #1
		$phpWord->addFontStyle(
			'fStyle',
			array(
				'size'    => '12',
				'bold'    => false,
				'allCaps' => false,
				'spaceAfter'=>150,
			)
        ); // Style #2

		$section = $phpWord->addSection(
			array('marginLeft' => 3165, 'marginRight' => 850,
				'marginTop' => 1140, 'marginBottom' => 575)
		);



		function penyebut($nilai) {
			$nilai = abs($nilai);
			$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
			$temp = "";
			if ($nilai < 12) {
				$temp = " ". $huruf[$nilai];
			} else if ($nilai <20) {
				$temp = penyebut($nilai - 10). " belas";
			} else if ($nilai < 100) {
				$temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
			} else if ($nilai < 200) {
				$temp = " seratus" . penyebut($nilai - 100);
			} else if ($nilai < 1000) {
				$temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
			} else if ($nilai < 2000) {
				$temp = " seribu" . penyebut($nilai - 1000);
			} else if ($nilai < 1000000) {
				$temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
			} else if ($nilai < 1000000000) {
				$temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
			} else if ($nilai < 1000000000000) {
				$temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
			} else if ($nilai < 1000000000000000) {
				$temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
			}
			return $temp;
		}

		function penyebutBulan($nilai) {
			$nilai = abs($nilai);
			$huruf = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
			$temp = "";
			if ($nilai == 1) {
				$temp = $huruf[1];
			}else if ($nilai == 2) {
				$temp = $huruf[2];
			}elseif ($nilai == 3) {
				$temp = $huruf[3];
			}elseif ($nilai == 4) {
				$temp = $huruf[4];
			}elseif ($nilai == 5) {
				$temp = $huruf[5];
			}elseif ($nilai == 6) {
				$temp = $huruf[6];
			}elseif ($nilai == 7) {
				$temp = $huruf[7];
			}elseif ($nilai == 8) {
				$temp = $huruf[8];
			}else {
				$temp = "sadasdas";
			}
			return $temp;
		}

		function terbilang($nilai) {
			if($nilai<0) {
				$hasil = "minus ". trim(penyebut($nilai));
			} else {
				$hasil = trim(penyebut($nilai));
			}
			return $hasil;
		}

		function terbilangBulan($nilai) {
			if($nilai<0) {
				$hasil = "minus ". trim(penyebutBulan($nilai));
			} else {
				$hasil = trim(penyebutBulan($nilai));
			}
			return $hasil;
		}


		function hari_ini(){
			$hari = date ("D");

			switch($hari){
				case 'Sun':
				$hari_ini = "Minggu";
				break;

				case 'Mon':
				$hari_ini = "Senin";
				break;

				case 'Tue':
				$hari_ini = "Selasa";
				break;

				case 'Wed':
				$hari_ini = "Rabu";
				break;

				case 'Thu':
				$hari_ini = "Kamis";
				break;

				case 'Fri':
				$hari_ini = "Jumat";
				break;

				case 'Sat':
				$hari_ini = "Sabtu";
				break;

				default:
				$hari_ini = "Tidak di ketahui";
				break;
			}
			return $hari_ini;
		}

    //=============================== Variables
		$nomor = "159/";
		$Sk_PPAT = "156/KEP-17.3//VII/2014";
		$nyonya;


    //---------------------STYLE
		$phpWord->addFontStyle('r3Style', array('name' => 'Bookman Old Style','bold'=>false, 'italic'=>false, 'size'=>12));
		$phpWord->addFontStyle('rJarak', array('name' => 'Bookman Old Style','bold'=>false, 'italic'=>false, 'size'=>1));
		$phpWord->addFontStyle('rLINE', array('name' => 'Bookman Old Style','bold'=>false, 'italic'=>false, 'size'=>1, 'color'=>'FFFFFF'));
		$phpWord->addFontStyle('r2Style', array('name' => 'Bookman Old Style','bold'=>true, 'italic'=>false, 'size'=>20));
		$phpWord->addParagraphStyle('p2Style', array('align'=>'center', 'spaceAfter'=>100));
		$phpWord->addParagraphStyle('p3RataKiri', array('align'=>'both', 'spaceAfter'=>150, 'spacing' => 150));
		$phpWord->addFontStyle('r4Style', array('name' => 'Bookman Old Style','bold'=>true, 'italic'=>false, 'size'=>12));
		$phpWord->addFontStyle('r5Style', array('name' => 'Bookman Old Style','bold'=>false, 'italic'=>true, 'size'=>12));

		//------------------ style $footer
		$phpWord->addFontStyle('rStyleFooterAtas', array('name' => 'Bookman Old Style','bold'=>true, 'italic'=>true, 'size'=>8));
		$phpWord->addFontStyle('rStyleHalamanFooter', array('name' => 'Times New Roman','bold'=>false, 'italic'=>false, 'size'=>8));
		$phpWord->addFontStyle('rStyleFooterTengah', array('name' => 'Bookman Old Style','bold'=>true, 'italic'=>false, 'size'=>8));
		$phpWord->addFontStyle('rStyleFooterBawah', array('name' => 'Bookman Old Style','bold'=>false, 'italic'=>false, 'size'=>8));
		$phpWord->addParagraphStyle('p2StyleFooter', array('align'=>'left', 'spaceAfter'=>1));

		$phpWord->addParagraphStyle('myBorderStyle', array(
			'borderSize' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(1),
			'borderColor' => 'ffffff',
			'borderBottomSize' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(1),
			'borderTopColor' => '0000'
		));
    //------------------Text
		$text = 'NOTARIS<w:br/>NUR AINI MAULIDA, S.H.,M.Kn.';
		$text2 = 'DAERAH KERJA KABUPATEN BANYUWANGI<w:br/>
		SK.MENTERI HUKUM DAN HAK ASASI MANUSIA REPUBLIK INDONESIA<w:br/>
		Nomor AHU.418.AH.02.01 Tahun 2013, Tanggal 30-08-2013<w:br/>
		Jl.Brawijaya Nomor 18 Banyuwangi<w:br/>
		Telp./ Fax :  0333 – 423411
		';
		$text3 = 'SURAT KUASA<w:br/>MEMBEBANKAN HAK TANGGUNGAN';
		$text4 = 'Nomor : 13/'.date('Y');
		$text5 = 'Lembar Kedua';
		$text6 = 'Pada hari ini, '.hari_ini().', tanggal '.date('d-m-Y').' ('.terbilang(date('d')).''.terbilangBulan(date('m')).' Tahun '.terbilang(date('Y')).') pukul '.date('h').'.'.date('i').' W.I.B ('.terbilang(date('h')).' '.terbilang(date('i')).' Waktu Indonesia Barat).-------------------------------------------';
		$text7 = 'Menghadap kepada saya, NUR AINI MAULIDA, Sarjana Hukum,--- Magister Kenotariatan , yang berdasarkan Surat Keputusan---- Menteri Hukum dan Hak Asasi Manusia Republik Indonesia--- Nomor AHU.418.AH.02.01 Tahun 2013, Tanggal 30-08-2013,---- diangkat sebagai Notaris di Kabupaten Banyuwangi, dengan---- dihadiri oleh saksi-saksi yang nama-namanya akan disebutkan--- pada bagian akhir akta ini: ------------------------------------------------';
		$footer_text_atas = "Akta Jual Beli";
		$footer_text_tengah = "NUR AINI MAULIDA, S.H, M.Kn";
		$footer_text_bawah = "Daerah Kerja Sekabupaten Banyuwangi";

    //----------------ADD TEXT
		$section->addText($text,'r2Style', 'p2Style');
		$section->addText($text2,'r3Style','p2Style');
		$section->addText("AS",'rJarak','p2Style');
		$section->addText("AS",'rLINE','myBorderStyle');
		$section->addText($text3, 'r4Style','p2Style');
		$section->addText($text4,'r3Style','p2Style');
		$section->addText($text5,'r5Style','p2Style');
		$section->addText();
		$section->addText($text6,'r3Style','p3RataKiri');
		$section->addText($text7,'r3Style','p3RataKiri');

		$phpWord->addNumberingStyle(
			'multilevel',
			array(
				'type' => 'multilevel',
				'levels' => array(
					array('format' => 'upperRoman', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
					array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720),
				)
			),
			array(
				'type' => 'multilevel2',
				'levels' => array(
					array('format' => 'bullet', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
					
				)
			)
		);
		// ----------------------------pihak1---------------
		$pihakPertama = "Tuan HARIYANTO, Lahir di Banyuwangi, tanggal 15 Maret 1974, Warga Negara Indonesia, Wiraswasta, bertempat tinggal di Dusun Sidodadi, Rukun Tetangga 001, Rukun Warga 001, Desa Karetan, Kecamatan Purwoharjo, Kabupaten Banyuwangi,----- Pemilik Kartu Tanda Penduduk dengan Nomor Induk------- Kependudukan 3510031503740007.- -------------------------------- <w:br/>Penghadap tersebut diatas melakukan tindakan hukum dalam akta ini, telah mendapat persetujuan dari Istrinya yang sah, yang turut pula hadir dihadapan saya, Notaris dan menanda-tangani Akta ini, Nyonya SUHATUTIK,Lahir di Ponorogo pada tanggal 20 Oktober 1973, Warga Negara Indonesia, Wiraswasta, bertempat tinggal sama dengan Suaminya tersebut diatas,---- pemegang Kartu Tanda Penduduk dengan Nomor Induk------- Kependudukan 3510036010730002. - --------------------------------
Pemegang hak atas tanah yang akan dijadikan Obyek Hak ------Tanggungan. Selanjutnya disebut PEMBERI KUASA  -------------";
		$section->addListItem($pihakPertama, 0, 'fStyle', 'multilevel','p3RataKiri');

		$pihakKedua = 'Tuan HAPPY GANDA WANTORO, lahir di Pasuruan,tanggal 21-08-1984 (dua puluh satu Agustus tahun seribu Sembilan ratus delapan puluh empat),Warga Negara Indonesia, Karyawan Swasta, bertempat tinggal di Perumahan Bunga Residence Blok A 21, Rukun Tetangga 001, Rukun Warga 003, Kelurahan Kebalenan, Kecamatan Banyuwangi, Kabu-paten Banyuwangi Pemegang Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3575012108840002. Bertindak dalam ---------kedudukannya selaku SUB BRANCH HEAD PT. BANK TABUNGAN NEGARA (Persero), Tbk Cabang BANYUWANGI, berdasarkan Surat Keputusan Nomor 13/SK/BWI.III/OPS/II/ 2018, tanggal 21 Februari 2018, jo Akta Pernyataan Keputusan Rapat Umum Pemegang Saham Tahunan nomor 51, tanggal 24 Maret 2015, dibuat dihadapan FATHIAH- HELMI, Sarjana Hukum, Notaris di Jakarta, yang pemberitahuannya telah diterima oleh Menteri Hukum dan Hak Asasi Manusia Republik Indonesia berdasarkan Surat Nomor AHU-AH.01.03-0926094,--tanggal 21 April 2015 junco Akta Pernyataan Keputusan Rapat Umum Pemegang Saham Tahunan  nomor 40, tanggal 12 April 2016, yang pemberitahuannya telah diterima oleh Menteri Hukum dan Hak Asasi Manusia Republik Indonesia berdasar-kan Surat Nomor AHU-AH.01.03-0039336, tanggal 12 April 2016 dengan demikian sah bertindak untuk dan atas nama PT. BANK TABUNGAN NEGARA  (Persero), Tbk,.--------------------';
		$section->addListItem($pihakKedua, 0, 'fStyle', 'multilevel','p3RataKiri');
		$section->addListItem('- Selanjutnya disebut PENERIMA KUASA -----------------------------',0,'fStyle', 'multilevel2', 'p3RataKiri');
		$section->addListItem('- Penghadap dikenal oleh saya, Notaris. --------------------------------',0,'fStyle', 'multilevel2', 'p3RataKiri');
		$section->addListItem('- Pemberi Kuasa menerangkan dengan ini memberi kuasa kepada Penerima Kuasa. ----------------------------------------------------------',0,'fStyle', 'multilevel2', 'p3RataKiri');

		$section->addText('----------------------------------- K H U S U S ------------------------------');
		$pembebananhak1='- Untuk membebankan Hak Tanggungan Ke I (pertama) guna menjamin pelunasan hutang Tuan HARIYANTO, tersebut.- ---';
		$section->addListItem($pembebananhak1,0,'fStyle','multilevel2','p3RataKiri');

		$debitur='- Selaku Debitur sejumlah Rp. 110.000.000,- (seratus sepuluh juta rupiah), atau sejumlah uang yang dapat ditentukan------- dikemudian hari berdasarkan perjanjian utang piutang yang----- ditandatangani oleh Debitur/Pemberi Kuasa dengan: -------------';
		$section->addListItem($debitur,0,'fStyle','multilevel2','p3RataKiri');

		$kreditur='- Perseroan Terbatas PT. BANK TABUNGAN NEGARA (Persero) Tbk, yang berkedudukan dan berkantor pusat di Jakarta--------- melalui cabangnya di Banyuwangi. ------------------------------------selaku Kreditur dibuktikan dengan : ----------------------------------';
		$section->addListItem($kreditur,0,'fStyle','multilevel2','p3RataKiri');

		$bank='- Surat Perjanjian Kredit antara Perseroan Terbatas PT. BANK----- TABUNGAN NEGARA (Persero) Tbk dan HARIYANTO Nomor---- 0022420180730000008 yang dibuat dibawah tangan bermaterai cukup yang dilegalisasi oleh saya, Notaris tertanggal 19-08-2019 (Sembilan belas Agustus tahun dua ribu sembilan belas)------ dibawah Nomor 04/L/NA/VIII/2019, yang surat asli/salinan ----resminya diperlihatkan kepada saya dan penambahan, -----perubahan, perpanjangan serta pembaharuannya yang mungkin diadakan sampai sejumlah Nilai tanggungan sebesar Rp. 165.000.000 (Seratus enam puluh lima  juta rupiah)) atas obyek Hak Tanggungan berupa 1 (satu) bidang  Hak Atas Tanah/Hak Milik Satuan Rumah Susun yang diuraikan dibawah ini : --------';
		$section->addListItem($bank,0,'fStyle','multilevel2','p3RataKiri');
		//========================   FOOTER
		$footer = $section->addFooter();
		$footer->addText($footer_text_atas,'rStyleFooterAtas','p2StyleFooter');
		$footer->addText($footer_text_tengah,'rStyleFooterTengah','p2StyleFooter');
		$footer->addText($footer_text_bawah,'rStyleFooterBawah','p2StyleFooter');

		$textRun = $footer->addTextRun(array('alignment' => \PhpOffice\Phpword\SimpleType\Jc::END));
		$textRun->addText('Halaman ','rStyleHalamanFooter');
		$textRun->addField('PAGE', array('format' => 'Arabic'));
		$textRun->addText(' dari ','rStyleHalamanFooter');
		$textRun->addField('NUMPAGES', array('format' => 'Arabic'));
		$textRun->addText(' halaman','rStyleHalamanFooter');


		$writer = new Word2007($phpWord);
		$filename = 'simpleblabla';

		header('Content-Type: application/msword');
		header('Content-Disposition: attachment;filename="'. $filename .'.docx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}

}
