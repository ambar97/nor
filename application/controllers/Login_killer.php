<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_killer extends CI_Controller {
	function __construct(){
			parent::__construct();
			$this->load->model('M_model');
		}

	public function index(){
    $this->load->database();
    $jumlah_data = $this->M_model->jumlah_data();
		$this->load->library('pagination');
		$config['base_url'] = base_url().'login_killer/index/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
		$from = $this->uri->segment('3');
    // echo json_encode($from);
		$this->pagination->initialize($config);
		$data['user'] = $this->M_model->data($config['per_page'],$from);
		$this->load->view('login_killer', $data);
	}

  public function kill(){
    $this->M_model->update('0_user',array('STATUS_LOGIN'=>0),array('ID_USER'=>$this->uri->segment(3)));
    return redirect(base_url('login_killer'));
  }
}
