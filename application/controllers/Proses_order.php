<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proses_order extends CI_Controller {

	public function index()
	{
		$id = $this->uri->segment(3);
		$data['pros_ord']=$this->M_model->selectwhere('0_0_proses_order',array('NO_ID_JENIS_ORDER'=>$id))->result();
		//$data['pros_ord']=$this->M_model->select('0_0_proses_order')->result();
		$this->load->view('order/v_data_proses_order',$data);
	}
	public function tambah(){
		$this->load->view('order/i_proses_order');
	}
	public function tambahProsesOrder(){
		$data = array('NAMA_CUSTOMER' => $this->input->post('nm_cus'),
						'JENIS_CUSTOMER'=>$this->input->post('jenis'),
						'ALAMAT_CUSTOMER'=> $this->input->post('alamat'),
						'KABUPATEN_CUSTOMER'=>$this->input->post('kab'),
						'KECAMATAN_CUSTOMER'=>$this->input->post('kec'),
						'DESA_KELURAHAN_CUSTOMER'=>$this->input->post('des'),
						'RT_CUSTOMER'=>$this->input->post('rt'),
						'RW_CUSTOMER'=>$this->input->post('rw'),
						'NOMOR_TELEPHONE_CUSTOMER'=>$this->input->post('telp'),
						'PIC_CUSTOMER'=>$this->input->post('pic'));
		$this->M_model->insert('0_0_proses_order',$data);
		redirect(base_url('order/Proses_order'));
	}
	public function hapusProsesOrder($id){
		$where = array('NO_ID_PROSES_ORDER'=>$id);
		$this -> M_model -> delete($where,'0_0_proses_order');
		header('location:'.base_url('order/Proses_order'));
	}
	public function ediProsesOrder(){
		$id = $this->uri->segment(3);
		$data['dataCus']=$this->M_model->selectwhere('0_0_proses_order',array('NO_ID_PROSES_ORDER'=>$id))->result();
		$this->load->view('customer/e_customer',$data);
	}
	public function updateProsesOrder(){
		$where = array('NO_ID_CUSTOMER'=>$this->input->post('idCus'));
		$data = array('NAMA_CUSTOMER' => $this->input->post('nm_cus'),
						'JENIS_CUSTOMER'=>$this->input->post('jenis'),
						'ALAMAT_CUSTOMER'=> $this->input->post('alamat'),
						'KABUPATEN_CUSTOMER'=>$this->input->post('kab'),
						'KECAMATAN_CUSTOMER'=>$this->input->post('kec'),
						'DESA_KELURAHAN_CUSTOMER'=>$this->input->post('des'),
						'RT_CUSTOMER'=>$this->input->post('rt'),
						'RW_CUSTOMER'=>$this->input->post('rw'),
						'NOMOR_TELEPHONE_CUSTOMER'=>$this->input->post('telp'),
						'PIC_CUSTOMER'=>$this->input->post('pic'));
		// die(var_dump($data));
		$this->M_model->update('0_0_proses_order',$data,$where);
		redirect(base_url('order/Proses_order'));
	}
}
