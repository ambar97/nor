<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Customer
			<small>Data Dokumen</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Customer</a></li>
			<li class="active">Data Dokumen</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Data Customer</h3>
			</div>
			<div class="box-body">
				<button class="btn btn-primary" data-toggle="modal" data-target="#exampleModalinsert"> <i class="fa fa-plus">Tambah Dokumen</i></button>
				<div class="modal fade" id="exampleModalinsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			      <div class="modal-dialog" role="document">
			        <div class="modal-content">
			          <div class="modal-header">
			            <h5 class="modal-title" id="exampleModalLabel">Tambah Data Dokumen Customer</h5>
			            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			              <span aria-hidden="true">&times;</span>
			            </button>
			          </div>
			          <div class="modal-body">
			              <form autocomplete="off" method="post" action="<?php echo base_url("Customer/t_dokumen")?>" enctype="multipart/form-data" class="form-horizontal form-bordered">
			              <input type="text" name="id" hidden="" value="">
			              <div class="modal-body">
			              	<?php foreach ($detail_order->result() as $view ) { ?>
			              		<input type="" name="id_detOrd" value="<?php echo $view->NO_ID_DETAIL_ORDER ?>" hidden>
			              	<?php } ?>
			                <div class="form-group">
			                  <label >Jenis Dokumen Upload</label>
			                  <select class="form-control" name="jns_dok">
			                    <option value="KTP">KTP</option>
			                    <option value="KARTU KELUARGA">KARTU KELUARGA</option>
			                    <option value="SURAT NIKAH">SURAT NIKAH</option>
			                    <option value="SERTIFIKAT">SERTIFIKAT</option>
			                    <option value="SPPT/PBB">SPPT/PBB</option>
			                    <option value="AKTA CERAI">AKTA CERAI</option>
			                    <option value="KEPUTUSAN GONO-GINI">KEPUTUSAN GONO-GINI</option>
			                  </select>
			                </div>
			                <div class="form-group">
			                  <label >Gambar Dokumen</label>
			                  <input type="file" id="input-file-now" class="dropify" name="gambar_dok" />
			                </div>
			              </div>
			              <div class="modal-footer">
			                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
			                <button type="submit" value="upload" class="btn btn-primary">Simpan</button>
			              </div>
			            </form>
			          </div>
			        </div>
			      </div>
			    </div>
				<hr>
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Jenis Dokumen Upload</th>
							<th>gambar</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; foreach ($dokumen->result() as $menu): ?>
						<tr>
							<td><?= $no++ ?></td>
							<td><?php echo $menu->JENIS_DOKUMEN_UPLOAD ?></td>
							<td><img style="width: 30%" src="<?php echo base_url().$menu->FILE_NAME_GAMBAR ?>"></td>
							<td>
							<button class="btn btn-primary" data-toggle="modal" data-target="#exampleModaledit<?php echo $menu->NO_ID_DOKUMEN ?>"> Edit</i></button>
							<a href="<?php echo base_url() ?>" title="hapus" onclick="javascript: return confirm('Anda Yakin Akan Menghapus ?')" class="btn btn-social-icon"><i class="fa fa-trash" title="Hapus" style="color: red"></i></a>
							</td>
							<!-- modal Edit -->
							<div class="modal fade" id="exampleModaledit<?php echo $menu->NO_ID_DOKUMEN ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						      <div class="modal-dialog" role="document">
						        <div class="modal-content">
						          <div class="modal-header">
						            <h5 class="modal-title" id="exampleModalLabel">Edit Data Dokumen Customer</h5>
						            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						              <span aria-hidden="true">&times;</span>
						            </button>
						          </div>
						          <div class="modal-body">
						              <form autocomplete="off" method="post" action="<?php echo base_url("Customer/edit_dokumen")?>" enctype="multipart/form-data" class="form-horizontal form-bordered">
						              <div class="modal-body">
						              	<?php foreach ($detail_order->result() as $view ) { ?>
						              		<input type="" name="id_detOrd" value="<?php echo $view->NO_ID_DETAIL_ORDER ?>" hidden>
						              	<?php } ?>
						              	<input type="" name="id_dok" value="<?php echo $menu->NO_ID_DOKUMEN ?>" hidden>
						                <div class="form-group">
						                  <label >Jenis Dokumen Upload</label>
						                  <select class="form-control" name="jns_dok">
						                  	<option selected="" disabled=""><?php echo $menu->JENIS_DOKUMEN_UPLOAD; ?></option>
						                    <option value="KTP">KTP</option>
						                    <option value="KARTU KELUARGA">KARTU KELUARGA</option>
						                    <option value="SURAT NIKAH">SURAT NIKAH</option>
						                    <option value="SERTIFIKAT">SERTIFIKAT</option>
						                    <option value="SPPT/PBB">SPPT/PBB</option>
						                    <option value="AKTA CERAI">AKTA CERAI</option>
						                    <option value="KEPUTUSAN GONO-GINI">KEPUTUSAN GONO-GINI</option>
						                  </select>
						                </div>
						                <div class="form-group">
						                  <label >Gambar Dokumen Lama</label>
						                  <img class="form-control" style="width: 50%;height: 70%" src="<?php echo base_url().$menu->FILE_NAME_GAMBAR ?>">
						                </div>
						                <div class="form-group">
						                  <label >Masukan Gambar Dokumen Baru</label>
						                  <input type="file" id="input-file-now" class="dropify" name="gambar_dok" />
						                </div>
						              </div>
						              <div class="modal-footer">
						                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
						                <button type="submit" value="upload" class="btn btn-primary">Simpan</button>
						              </div>
						            </form>
						          </div>
						        </div>
						      </div>
						    </div>
						</tr>
					<?php endforeach ?>
				</tbody>
				
			</table>
		</div>
	</div>
</section>
</div>
<?php $this->load->view('side/footer') ?>

<?php $this->load->view('side/js') ?>

<script src="<?php echo base_url() ?>master/dropify/dist/js/dropify.min.js"></script>
<script type="text/javascript">
  
  $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });

</script>
