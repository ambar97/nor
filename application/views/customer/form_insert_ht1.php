
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label >Order Customer Atas Nama (PIHAK PERTAMA)</label>
                              <input type="text" class="form-control" name="order_cus_atas_nama">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label >NIK</label>
                              <input type="number" class="form-control" name="nik">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                              <label >Jenis Kelamin</label><br>
                              <label for="Laki-laki">Laki-laki</label><br>
                              <input type="radio" name="gender" value="Laki-laki" id="Laki-laki">
                              <label for="Perempuan">Perempuan</label><br>
                              <input type="radio" name="gender" value="Perempuan" id="Perempuan">
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label >Tempat Lahir</label>
                              <input type="text" class="form-control" name="tl">
                              </div
                              ></div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label >Tanggal Lahir</label>
                                  <input type="date" class="form-control" name="tgl">
                                  </div
                                  ></div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label >Kewarganegaraan</label>
                                      <select class="form-control" name="kwn">
                                        <option value="Warga Negara Indonesia"> WNI </option>
                                        <option value="Warga Negara Asing"> WNA </option>
                                      </select>
                                      </div
                                      ></div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label >Pekerjaan</label>
                                          <input type="text" class="form-control" name="pekerjaan">
                                          </div
                                          ></div>
                                        </div>
                                        
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-grub">
                                              <label >Alamat</label>
                                              <input type="text" class="form-control" name="alamat_cus">
                                            </div>
                                          </div>

                                          <div class="col-md-6">
                                            <label >RT</label>
                                            <input type="number" class="form-control" name="rt_cus">
                                          </div>
                                          <div class="col-md-6">
                                            <label >RW</label>
                                            <input type="number" class="form-control" name="rw_cus">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Desa/Kelurahan</label>
                                            <input type="text" class="form-control" name="desa_cus">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Kecamatan</label>
                                            <input type="text" class="form-control" name="kec_cus">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Kabupaten</label>
                                            <input type="text" class="form-control" name="kab_cus">
                                          </div>
                                        </div>
                                        <hr>
                                        <small>Data Pasangan</small>
                                        <br>
                                        <input type="checkbox" name="" id="" placeholder=""> <label for="">Tidak Memiliki Pasangan</label>
                                        <div class="row">
                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >Pasangan Order Customer Atas Nama</label>
                                          <input type="text" class="form-control" name="pas_order_cus_atas_nama">
                                        </div>
                                          </div>
                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >NIK Pasangan</label>
                                          <input type="text" class="form-control" name="nik_pas">
                                        </div>
                                          </div>

                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >Tempat Lahir</label>
                                          <input type="text" class="form-control" name="tl_pas">
                                        </div>
                                          </div>
                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >Tanggal Lahir</label>
                                          <input type="date" class="form-control" name="tgl_pas">
                                        </div>
                                          </div>
                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >Kewarganegaraan</label>
                                          <select class="form-control" name="kwn_pas">
                                            <option value="Warga Negara Indonesia">WNI</option>
                                            <option value="Warga Negara Asing">WNA</option>
                                          </select>
                                        </div>
                                          </div>
                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >Pekerjaan</label>
                                          <input type="text" class="form-control" name="pekerjaan_pas">
                                        </div>
                                          </div>
                                        </div>
                                        <hr>
                                        <small>Alamat Pasangan </small><br>
                                        <input type="checkbox" name="sama"> <label for="">Sama dengan data order customer</label>
                                        
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-grub">
                                              <label >Alamat</label>
                                              <input type="text" class="form-control" name="alamat_pas">
                                            </div>
                                          </div>

                                          <div class="col-md-6">
                                            <label >RT</label>
                                            <input type="text" class="form-control" name="rt_pas">
                                          </div>
                                          <div class="col-md-6">
                                            <label >RW</label>
                                            <input type="text" class="form-control" name="rw_pas">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Desa/Kelurahan</label>
                                            <input type="text" class="form-control" name="desa_pas">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Kecamatan</label>
                                            <input type="text" class="form-control" name="kec_pas">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Kabupaten</label>
                                            <input type="text" class="form-control" name="kab_pas">
                                          </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                        <div class="col-md-6">
                            <div class="form-group">
                              <label >Order Customer Atas Nama (PIHAK KEDUA)</label>
                              <input type="text" class="form-control" name="order_cus_atas_nama_kedua">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label >NIK</label>
                              <input type="number" class="form-control" name="nik_kedua">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label >Tempat Lahir</label>
                              <input type="text" class="form-control" name="tl_kedua">
                              </div
                              ></div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label >Tanggal Lahir</label>
                                  <input type="date" class="form-control" name="tgl_kedua">
                                  </div
                                  ></div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label >Kewarganegaraan</label>
                                      <select class="form-control" name="kwn_kedua">
                                        <option value="Warga Negara Indonesia"> WNI </option>
                                        <option value="Warga Negara Asing"> WNA </option>
                                      </select>
                                      </div
                                      ></div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label >Pekerjaan</label>
                                          <input type="text" class="form-control" name="pekerjaan_kedua">
                                          </div
                                          ></div>
                                        </div>
                                        
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-grub">
                                              <label >Alamat</label>
                                              <input type="text" class="form-control" name="alamat_cus_kedua">
                                            </div>
                                          </div>

                                          <div class="col-md-6">
                                            <label >RT</label>
                                            <input type="number" class="form-control" name="rt_cus_kedua">
                                          </div>
                                          <div class="col-md-6">
                                            <label >RW</label>
                                            <input type="number" class="form-control" name="rw_cus_kedua">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Desa/Kelurahan</label>
                                            <input type="text" class="form-control" name="desa_cus_kedua">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Kecamatan</label>
                                            <input type="text" class="form-control" name="kec_cus_kedua">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Kabupaten</label>
                                            <input type="text" class="form-control" name="kab_cus_kedua">
                                          </div>
                                        </div>
                                        </div>