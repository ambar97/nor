<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>
<?php foreach ($data_order_customer->result() as $value): ?>
<?php $id_order_customer = $value->NO_ID_ORDER_CUSTOMER; ?>
<?php endforeach; ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
			Customer
			<small>List Proses Pengikatan Obyek
				</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Customer</a></li>
			<li class="active">List Proses Pengikatan Obyek </li>
		</ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header">
        <div class="box-title" id="asd">
          <h3>List Proses Pengikatan</h3>
        </div>
      </div>
      <div class="box-body">
      <?php foreach ($data_customer->result() as $dc): ?>
                       <input type="hidden" name="id_customer" value="<?php echo $id_customer = $dc->NO_ID_CUSTOMER; ?>">
                     <?php endforeach; ?>
        <hr>
        <table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Proses Order</th>
							<th>Pegawai Yang Menangani</th>
							<th>Status Proses</th>
							<th>Tanggal Status</th>
              <th>Biaya Proses</th>
              <th>Action</th>
						</tr>
					</thead>
					<tbody>
            <?php $no = 1; ?>
            <?php foreach ($proses_order_customer->result() as $poc): ?>
              <tr>
                <td><?= $no++ ?></td>
                <td><?= $poc->NAMA_PROSES_ORDER; ?></td>
                <td>
                  <?php echo $id_user = $poc->ID_USER; ?><br>
                  <?php
                    $user = $this->M_model->selectwhere('0_user', array('ID_USER'=>$id_user));
                    if ($user->num_rows()==0) {
                      echo "Pilih Pegawai yang Menangani Proses ini";
                    }else {
                      foreach ($user->result() as $u) {
                        echo $u->NAMA_USER;
                      };
                    }
                  ?>
                </td>
                <td><?php echo $poc->STATUS_PROSES_ORDER_CUSTOMER; ?></td>
                <td>
                  <?php echo $poc->TGLSTTS_PROSES_ORDER_CUSTOMER; ?></td>
                <td>Rp.<?= number_format($poc->NOMINAL_BIAYA_PROSES) ; ?></td>
                <td>
                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal<?php echo $poc->NO_ID_PROSES_ORDER_CUSTOMER; ?>">Biaya</button>
                  <!-- Modal -->
                  <div class="modal fade" id="myModal<?php echo $poc->NO_ID_PROSES_ORDER_CUSTOMER; ?>">
                    <div class="modal-dialog">
                      <div class="modal-content">
                      <div class="modal-header">
                      <h4 class="modal-title"><strong>Nama Proses Order: </strong><?php echo $poc->NAMA_PROSES_ORDER; ?></h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <form action="<?php echo base_url('ProsesPengikatan/editBiayaUtkProses/'.$no); ?>" method="post" enctype="multipart/form-data">
                      <div class="modal-body">
                      <strong>No ID Proses Order Customer : </strong><?php echo $poc->NO_ID_PROSES_ORDER_CUSTOMER; ?><br><hr>
                      <div class="form-group">
                        <label>Biaya</label>
                        <br>
                        <input type="text" name="NO_ID_PROSES_ORDER_CUSTOMER<?php echo $no; ?>" value="<?php echo $poc->NO_ID_PROSES_ORDER_CUSTOMER; ?>" hidden>
                        <input type="text" name="NO_ID_OBYEK_ORDER_CUSTOMER<?php echo $no; ?>" value="<?php echo $poc->NO_ID_OBYEK_ORDER_CUSTOMER; ?>" hidden>
                        <input type="text" name="NO_ID_JENISORDER<?php echo $no; ?>" value="<?php echo $poc->NO_ID_JENISORDER; ?>" hidden>
                        <input type="text" name="NO_ID_PROSES_ORDER<?php echo $no; ?>" value="<?php echo $poc->NO_ID_PROSES_ORDER ?>" hidden>
                        <input type="text" name="ID_USER<?php echo $no; ?>" value="<?php echo $poc->ID_USER; ?>" hidden>
                        <input type="text" name="id_customer<?php echo $no; ?>" value="<?php echo $id_customer; ?>" hidden>
                        <input type="text" name="id_order_customer<?php echo $no; ?>" value="<?php echo $id_order_customer; ?>" hidden>
                        <input type="number" class="form-control" name="biaya<?php echo $no; ?>" value="<?php echo $poc->NOMINAL_BIAYA_PROSES; ?>">
                      </div>
                      <br>
                      <button type="submit">Simpan</button>
                      </div>
                      </form>
                      <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      </div>
                      </div>
                    </div>
                  </div>
                  <br>
                  <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal2<?php echo $poc->NO_ID_PROSES_ORDER_CUSTOMER; ?>">Status</button>
                  <!-- modal status -->
                    <div class="modal fade" id="myModal2<?php echo $poc->NO_ID_PROSES_ORDER_CUSTOMER; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Edit Status Proses</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                             <form method="post" action="<?php echo base_url("ProsesPengikatan/editStatusUtkProses/".$no)?>" enctype="multipart/form-data" class="form-horizontal form-bordered">
                              <div class="modal-body">
                                <input type="text" name="id_obyek_order_customer<?php echo $no; ?>" value="<?php echo $id_obyek_order_customer = $poc->NO_ID_OBYEK_ORDER_CUSTOMER; ?>" hidden>
                                <input type="text" name="id_customer<?php echo $no; ?>" value="<?php echo $id_customer; ?>" hidden>
                                <input type="text" name="id_order_customer<?php echo $no; ?>" value="<?php echo $id_order_customer; ?>" hidden>
                                <input type="text" name="id_ProsOrdCus<?php echo $no; ?>" value="<?php echo $poc->NO_ID_PROSES_ORDER_CUSTOMER ?>" hidden>
                                <div class="row">
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label >Status</label><br>
                                      <select class="form-control" name="status<?php echo $no; ?>">
                                        <option value="Dalam Proses" <?php if ($poc->STATUS_PROSES_ORDER_CUSTOMER=='Dalam Proses') {
                                          echo "selected";
                                        } ?>>Dalam Proses</option>
                                        <option value="Selesai" <?php if ($poc->STATUS_PROSES_ORDER_CUSTOMER=='Selesai') {
                                          echo "selected";
                                        } ?>>Selesai</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label >Tanggal Status</label><br>
                                      <input class="form-control" type="date" name="tgl_status<?php echo $no; ?>" value="<?php echo $poc->TGLSTTS_PROSES_ORDER_CUSTOMER; ?>">
                                    </div>
                                  </div>
                                </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                <button type="submit" value="upload" class="btn btn-primary">Simpan</button>
                              </div>
                            </form>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
                </td>
              </tr>
              <?php $no++; ?>
            <?php endforeach; ?>
		      </tbody>
				<tfoot>
					<tr>
            <th>ID Proses</th>
            <th>Proses Order</th>
            <th>Pegawai Yang Menangani</th>
            <th>Status Proses</th>
            <th>Tanggal Status</th>
            <th>Biaya Proses</th>
            <th>Action</th>
					</tr>
				</tfoot>
			</table>
      </div>
    </div>
  </section>
</div>
<?php $this->session->set_userdata('current_url', current_url()); ?>
<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>
