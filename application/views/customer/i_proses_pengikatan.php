<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
			Customer
			<small>Input Proses Pengikatan
				</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Customer</a></li>
			<li class="active">Input Proses Pengikatan </li>
		</ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header">
        <div class="box-title">
          <h3>Data Order</h3>
        </div>
      </div>
      <div class="box-body">
        <form action="<?php echo base_url('ProsesPengikatan/proses_list'); ?>" method="post" enctype="multipart/form-data">
        <button class="btn btn-primary" type="submit">Proses</button>
        <hr>
        <table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>ID Proses</th>
							<th>Proses Dalam Pengikatan</th>
							<th>Alur Proses Order</th>
							<th>Posisi Proses</th>
							<th>Biaya Per Proses</th>
						</tr>
					</thead>
					<tbody>
            <?php $no = 1; ?>
            <?php foreach ($proses_order->result() as $po): ?>
              <tr>
                <td>
                  <?php foreach ($detail_obyek_order_customer->result() as $do): ?>
                    <input type="text" name="NO_ID_OBYEK_ORDER_CUSTOMER[]" value="<?php echo $do->NO_ID_OBYEK_ORDER_CUSTOMER; ?>" hidden/>
                    <input type="text" name="NO_ID_DETAIL_ORDER" value="<?php echo $do->NO_ID_DETAIL_ORDER; ?>" hidden/>
                  <?php endforeach; ?>
                  <?php foreach ($detail_order_customer->result() as $dos): ?>
                    <input type="text" name="NO_ID_JENISORDER" value="<?php echo $dos->NO_ID_JENISORDER; ?>" hidden/>
                    <input type="text" name="NO_ID_ORDER_CUSTOMER" value="<?php echo $dos->NO_ID_ORDER_CUSTOMER; ?>" hidden/>
                  <?php endforeach; ?>
                  <?php foreach ($data_order_customer->result() as $doc): ?>
                    <input type="text" name="NO_ID_CUSTOMER" value="<?php echo $doc->NO_ID_CUSTOMER; ?>" hidden/>
                  <?php endforeach; ?>
                  <input type="text" name="NAMA_PROSES_ORDER[]" value="<?php echo $po->NAMA_PROSES_ORDER; ?>" hidden/>
                  <input type="text" name="NO_ID_PROSES_ORDER[]" value="<?php echo $po->NO_ID_PROSES_ORDER; ?>" hidden/>
                  <?php echo $no; $no++; ?>
                </td>
                <td><?php echo $po->NAMA_PROSES_ORDER; ?></td>
                <td><?php echo $po->POST_PROSES_ORDER ; ?></td>
                <td><?php echo $po->KETERANGAN_PROSES_ORDER; ?></td>
                <td><input type="number" name="perCost[]" value="0"></td>
              </tr>
            <?php endforeach; ?>
		      </tbody>
				<tfoot>
					<tr>
            <th>ID Proses</th>
            <th>Proses Dalam Pengikatan</th>
            <th>Alur Proses Order</th>
            <th>Posisi Proses</th>
            <th>Biaya Per Proses</th>
					</tr>
				</tfoot>
			</table>
      </form>
      </div>
    </div>
  </section>
</div>

<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>
