<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
     Customer
     <small>Data Order
     </small>
   </h1>
   <ol class="breadcrumb">
     <li><a href="#"><i class="fa fa-dashboard"></i> Customer</a></li>
     <li class="active">Data Order</li>
   </ol>
 </section>
 <section class="content">
  <div class="box">
    <div class="box-header">
      <div class="box-title">
        <?php foreach ($customer->result() as $c): ?>
          <h3>Data Order <?php echo $c->NAMA_CUSTOMER; ?></h3>
        <?php endforeach; ?>
      </div>
    </div>
    <div class="box-body">
      <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModalinsert"> <i class="fa fa-plus">Tambah</i></button>
      <!-- Modal Insert-->
      <div class="modal fade" id="exampleModalinsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah Data Order Customer</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
             <form autocomplete="off" method="post" action="<?php echo base_url("Customer/prosesTambahDataOrderCus")?>" enctype="multipart/form-data" class="form-horizontal form-bordered">
              <input type="text" name="id" hidden="" value="">
              <div class="modal-body">
                <?php foreach ($rincian1->result() as $view ) { ?>
                  <div class="form-group">
                    <label >No Surat Order : <?php echo $view->NOMOR_SURAT_ORDER_CUSTOMER; ?> || Tanggal : <?php echo date("d F Y", strtotime($view->TANGGAL_SURAT_ORDER_CUSTOMER)); ?> </label>
                  </div>
                <?php } ?>
                  <input type="" name="id_Cus" value="<?php echo $id_customer ?>" hidden>
                        <!-- <?php foreach ($rincian2->result() as $view) { ?>
                        <input type="" name="id_OrdCus" value="<?php echo $view->NO_ID_ORDER_CUSTOMER ?>" hidden>
                        <?php } ?> -->
                        <?php foreach ($rincian1->result() as $view) { ?>
                          <input type="" name="id_OrdCus" value="<?php echo $view->NO_ID_ORDER_CUSTOMER ?>" hidden>
                        <?php } ?>
                        <div class="form-group">
                          <label >Jenis Order</label>
                          <select class="form-control" name="jenis_ord">
                          <option value="null">-Pilih-</option>
                            <?php foreach ($jenisOrder->result() as $view) { ?>
                              <option value="<?php echo $view->NO_ID_JENISORDER ?>"><?php echo $view->NAMA_JENIS_ORDER; ?>-<?php echo $view->OBYEK_JENIS_ORDER; ?>-<?php echo $view->ASAL_OBYEK_JENIS_ORDER; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                        <div id="Tambah_Data_Order_Customer"></div>
                                        <small>Data Saksi</small>
                                        <div class="form-group">
                                          <label >Saksi 1</label>
                                          <select class="form-control" name="saksi1">
                                            <option>pilih saksi</option>
                                            <?php foreach ($saksi as $saks) { ?>
                                              <option value="<?= $saks->NAMA_USER ?>"><?= $saks->NAMA_USER ?></option>
                                            <?php }?>
                                          </select>
                                        </div>
                                        <div class="form-group">
                                          <label >Saksi 2</label>
                                          <select class="form-control" name="saksi2">
                                            <option>pilih saksi</option>
                                            <?php foreach ($saksi as $saks) { ?>
                                              <option value="<?= $saks->NAMA_USER ?>"><?= $saks->NAMA_USER ?></option>
                                            <?php }?>
                                          </select>
                                        </div>
                                        <div class="form-group">
                                          <label >Nominal Pengikatan</label>
                                          <input type="text" class="form-control" name="nominal_pengikatan">
                                        </div>
                                        <div class="form-group">
                                          <label >Keterangan Order Pengikatan</label>
                                          <input type="text" class="form-control" name="ket">
                                        </div>
                                        </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                        <button type="submit" value="upload" class="btn btn-primary">Simpan</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <hr>
                            <table id="example1" class="table table-bordered table-striped">
                             <thead>
                              <tr>
                               <th>#</th>
                               <th>Order Customer Atas Nama</th>
                               <th>Pasangan Order Customer Atas Nama</th>
                               <th>Saksi satu</th>
                               <th>Saksi dua</th>
                               <th>Nominal Pengikatan</th>
                               <th>Keterangan Order Pengikatan</th>
                               <th>Action</th>
                               <th>Cetak</th>
                             </tr>
                           </thead>
                           <tbody>
                            <?php $no=1; foreach ($rincian2->result() as $r): ?>
                            <tr>
                              <td><a type="button" class="btn btn-social-icon btn-sm" data-toggle="modal" data-target="#exampleModal<?php echo$r->NO_ID_DETAIL_ORDER ?>">
                                <i class="fa fa-pencil"></i>
                              </a>
                              <a class="btn btn-social-icon btn-sm" href="<?php echo base_url('Customer/deleteDetailOrderCustomer/').$r->NO_ID_DETAIL_ORDER.'/'.$id_order_customer.'/'.$id_customer; ?>"> <i class="fa fa-trash" style="color:red;"></i> </a></td>
                              <td><?php echo $r->DETAIL_ORDER_ATAS_NAMA; ?></td>
                              <td><?php echo $r->PASANGAN_DETAIL_ORDER; ?></td>
                              <td><?php echo $r->SAKSI_SATU; ?></td>
                              <td><?php echo $r->SAKSI_DUA; ?></td>
                              <td>Rp. <?php echo number_format($r->NOMINAL_DETAIL_ORDER); ?>,00</td>
                              <td><?php echo $r->KETERANGAN_DETAIL_ORDER; ?></td>
                              <td>
                                <?php foreach ($rincian1->result() as $r1): ?>
                                  <a class="btn btn-success btn-sm" href="<?php echo base_url('Customer/detailObyekOrderCus/').$r->NO_ID_DETAIL_ORDER.'/'.$r->NO_ID_ORDER_CUSTOMER.'/'.$r1->NO_ID_CUSTOMER; ?>">Detail Obyek</a>
                                <?php endforeach; ?>
                                <br>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('Customer/dokumen/').$r->NO_ID_DETAIL_ORDER; ?>">Dokumen</a>
                              </td>
                              <td>
                                <a class="btn btn-social-icon btn-sm" href=""> <i class="fa fa-print"></i></a>
                              </td>
                            </tr>
                            <!-- Modal Edit-->
                            <div class="modal fade" id="exampleModal<?php echo$r->NO_ID_DETAIL_ORDER ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Data Order Customer</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                   <form autocomplete="off" method="post" action="<?php echo base_url("Customer/updateDatOrderCus")?>" enctype="multipart/form-data" class="form-horizontal form-bordered">
                                    <input type="text" name="id" hidden="" value="">
                                    <div class="modal-body">
                                      <input type="text" name="id_DetOrd" value="<?php echo $r->NO_ID_DETAIL_ORDER ?>" hidden>
                                      <input type="text" name="id_OrdCus" value="<?php echo $r->NO_ID_ORDER_CUSTOMER ?>" hidden>
                                      <div class="form-group">
                                        <label >No ID Order Customer</label>
                                        <input type="text" class="form-control" name="no_id_ordCus" value="<?php echo $r->NO_ID_ORDER_CUSTOMER ?>" readonly>
                                      </div>
                                      <div class="form-group">
                                        <label >No ID Jenis Order</label>
                                        <select class="form-control" name="jenis_ord">
                                          <?php foreach ($jenisOrder->result() as $view) { ?>
                                            <option value="<?php echo $view->NO_ID_JENISORDER ?>"><?php echo $view->NAMA_JENIS_ORDER; ?>-<?php echo $view->OBYEK_JENIS_ORDER; ?>-<?php echo $view->ASAL_OBYEK_JENIS_ORDER; ?></option>
                                          <?php } ?>
                                        </select>
                                      </div>
                                      <div class="form-group">
                                        <label >Order Customer Atas Nama</label>
                                        <input type="text" class="form-control" name="order_cus_atas_nama" value="<?php echo $r->DETAIL_ORDER_ATAS_NAMA ?>">
                                      </div>
                                      <div class="form-group">
                                        <label >Pasangan Order Customer Atas Nama</label>
                                        <input type="text" class="form-control" name="pas_order_cus_atas_nama" value="<?php echo $r->PASANGAN_DETAIL_ORDER ?>">
                                      </div>
                                      <div class="form-group">
                                        <label >Biodata Pasangan Order Customer Atas Nama</label>
                                        <textarea class="form-control" name="bio_pas_order_cus_atas_nama"><?php echo $r->BIODATA_PASANGAN_DETAIL_ORDER; ?></textarea>
                                      </div>
                                      <div class="form-group">
                                        <label >Saksi 1</label>
                                        <select class="form-control" name="saksi1">
                                         <?php foreach ($saksi as $saksi) { ?>
                                           <option ><?= $saksi->NAMA_USER ?></option>
                                         <?php } ?>
                                       </select>
                                     </div>
                                     <div class="form-group">
                                      <label >Saksi 2</label>
                                      <select class="form-control" name="saksi2">
                                        
                                      </select>
                                    </div>
                                    <div class="form-group">
                                      <label >Nominal Pengikatan</label>
                                      <input type="text" class="form-control" name="nominal_pengikatan" value="<?php echo $r->NOMINAL_DETAIL_ORDER ?>">
                                    </div>
                                    <div class="form-group">
                                      <label >Keterangan Order Pengikatan</label>
                                      <input type="text" class="form-control" name="ket" value="<?php echo $r->KETERANGAN_DETAIL_ORDER ?>">
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                    <button type="submit" value="upload" class="btn btn-primary">Simpan</button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </section>
          </div>

          <?php $this->load->view('side/footer') ?>
          <?php $this->load->view('side/js') ?>
          <script type="text/javascript">
          
          $('select[name="jenis_ord"]').change(function(){
            if(this.value == 1 || this.value == 4) {
              const htmlku = `
              <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label >Order Customer Atas Nama</label>
                              <input type="text" class="form-control" name="order_cus_atas_nama">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label >NIK</label>
                              <input type="number" class="form-control" name="nik">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                        <div class="form-group">
                        <div class="col-md-12">
                              <label >Jenis Kelamin</label><br>
                        </div>
                          <div class="col-md-6">
                              <input type="radio" name="gender" value="Laki-laki" id="Laki-laki">
                              <label for="Laki-laki">Laki-laki</label>
                        </div>
                          <div class="col-md-6">
                              <input type="radio" name="gender" value="Perempuan" id="Perempuan">
                              <label for="Perempuan">Perempuan</label><br>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label >Tempat Lahir</label>
                              <input type="text" class="form-control" name="tl">
                              </div
                              ></div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label >Tanggal Lahir</label>
                                  <input type="date" class="form-control" name="tgl">
                                  </div
                                  ></div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label >Kewarganegaraan</label>
                                      <select class="form-control" name="kwn">
                                        <option value="Warga Negara Indonesia"> WNI </option>
                                        <option value="Warga Negara Asing"> WNA </option>
                                      </select>
                                      </div
                                      ></div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label >Pekerjaan</label>
                                          <input type="text" class="form-control" name="pekerjaan">
                                          </div
                                          ></div>
                                        </div>
                                        
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-grub">
                                              <label >Alamat</label>
                                              <input type="text" class="form-control" name="alamat_cus">
                                            </div>
                                          </div>

                                          <div class="col-md-6">
                                            <label >RT</label>
                                            <input type="number" class="form-control" name="rt_cus">
                                          </div>
                                          <div class="col-md-6">
                                            <label >RW</label>
                                            <input type="number" class="form-control" name="rw_cus">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Desa/Kelurahan</label>
                                            <input type="text" class="form-control" name="desa_cus">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Kecamatan</label>
                                            <input type="text" class="form-control" name="kec_cus">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Kabupaten</label>
                                            <input type="text" class="form-control" name="kab_cus">
                                          </div>
                                        </div>
                                        <hr>
                                        <small>Data Pasangan</small>
                                        <br>
                                        <input type="checkbox" name="" id="" placeholder=""> <label for="">Tidak Memiliki Pasangan</label>
                                        <div class="row">
                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >Pasangan Order Customer Atas Nama</label>
                                          <input type="text" class="form-control" name="pas_order_cus_atas_nama">
                                        </div>
                                          </div>
                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >NIK Pasangan</label>
                                          <input type="text" class="form-control" name="nik_pas">
                                        </div>
                                          </div>

                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >Tempat Lahir</label>
                                          <input type="text" class="form-control" name="tl_pas">
                                        </div>
                                          </div>
                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >Tanggal Lahir</label>
                                          <input type="date" class="form-control" name="tgl_pas">
                                        </div>
                                          </div>
                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >Kewarganegaraan</label>
                                          <select class="form-control" name="kwn_pas">
                                            <option value="Warga Negara Indonesia">WNI</option>
                                            <option value="Warga Negara Asing">WNA</option>
                                          </select>
                                        </div>
                                          </div>
                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >Pekerjaan</label>
                                          <input type="text" class="form-control" name="pekerjaan_pas">
                                        </div>
                                          </div>
                                        </div>
                                        <hr>
                                        <small>Alamat Pasangan </small><br>
                                        <input type="checkbox" name="sama"> <label for="">Sama dengan data order customer</label>
                                        
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-grub">
                                              <label >Alamat</label>
                                              <input type="text" class="form-control" name="alamat_pas">
                                            </div>
                                          </div>

                                          <div class="col-md-6">
                                            <label >RT</label>
                                            <input type="text" class="form-control" name="rt_pas">
                                          </div>
                                          <div class="col-md-6">
                                            <label >RW</label>
                                            <input type="text" class="form-control" name="rw_pas">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Desa/Kelurahan</label>
                                            <input type="text" class="form-control" name="desa_pas">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Kecamatan</label>
                                            <input type="text" class="form-control" name="kec_pas">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Kabupaten</label>
                                            <input type="text" class="form-control" name="kab_pas">
                                          </div>
                                        </div>
                                        <hr>
                                        
              `;
              $("#Tambah_Data_Order_Customer").html(htmlku);
            } else if (this.value == 2 || this.value == 3 || this.value == 5 || this.value == 6) {
              const htmlku = `
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label >Order Customer Atas Nama (PIHAK PERTAMA)</label>
                              <input type="text" class="form-control" name="order_cus_atas_nama">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label >NIK</label>
                              <input type="number" class="form-control" name="nik">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                        <div class="form-group">
                        <div class="col-md-12">
                              <label >Jenis Kelamin</label><br>
                        </div>
                          <div class="col-md-6">
                              <input type="radio" name="gender" value="Laki-laki" id="Laki-laki">
                              <label for="Laki-laki">Laki-laki</label>
                        </div>
                          <div class="col-md-6">
                              <input type="radio" name="gender" value="Perempuan" id="Perempuan">
                              <label for="Perempuan">Perempuan</label><br>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label >Tempat Lahir</label>
                              <input type="text" class="form-control" name="tl">
                              </div
                              ></div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label >Tanggal Lahir</label>
                                  <input type="date" class="form-control" name="tgl">
                                  </div
                                  ></div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label >Kewarganegaraan</label>
                                      <select class="form-control" name="kwn">
                                        <option value="Warga Negara Indonesia"> WNI </option>
                                        <option value="Warga Negara Asing"> WNA </option>
                                      </select>
                                      </div
                                      ></div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label >Pekerjaan</label>
                                          <input type="text" class="form-control" name="pekerjaan">
                                          </div
                                          ></div>
                                        </div>
                                        
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-grub">
                                              <label >Alamat</label>
                                              <input type="text" class="form-control" name="alamat_cus">
                                            </div>
                                          </div>

                                          <div class="col-md-6">
                                            <label >RT</label>
                                            <input type="number" class="form-control" name="rt_cus">
                                          </div>
                                          <div class="col-md-6">
                                            <label >RW</label>
                                            <input type="number" class="form-control" name="rw_cus">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Desa/Kelurahan</label>
                                            <input type="text" class="form-control" name="desa_cus">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Kecamatan</label>
                                            <input type="text" class="form-control" name="kec_cus">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Kabupaten</label>
                                            <input type="text" class="form-control" name="kab_cus">
                                          </div>
                                        </div>
                                        <hr>
                                        <small>Data Pasangan</small>
                                        <br>
                                        <input type="checkbox" name="" id="" placeholder=""> <label for="">Tidak Memiliki Pasangan</label>
                                        <div class="row">
                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >Pasangan Order Customer Atas Nama</label>
                                          <input type="text" class="form-control" name="pas_order_cus_atas_nama">
                                        </div>
                                          </div>
                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >NIK Pasangan</label>
                                          <input type="text" class="form-control" name="nik_pas">
                                        </div>
                                          </div>

                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >Tempat Lahir</label>
                                          <input type="text" class="form-control" name="tl_pas">
                                        </div>
                                          </div>
                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >Tanggal Lahir</label>
                                          <input type="date" class="form-control" name="tgl_pas">
                                        </div>
                                          </div>
                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >Kewarganegaraan</label>
                                          <select class="form-control" name="kwn_pas">
                                            <option value="Warga Negara Indonesia">WNI</option>
                                            <option value="Warga Negara Asing">WNA</option>
                                          </select>
                                        </div>
                                          </div>
                                          <div class="col-md-6">
                                          <div class="form-group">
                                          <label >Pekerjaan</label>
                                          <input type="text" class="form-control" name="pekerjaan_pas">
                                        </div>
                                          </div>
                                        </div>
                                        <hr>
                                        <small>Alamat Pasangan </small><br>
                                        <input type="checkbox" name="sama"> <label for="">Sama dengan data order customer</label>
                                        
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-grub">
                                              <label >Alamat</label>
                                              <input type="text" class="form-control" name="alamat_pas">
                                            </div>
                                          </div>

                                          <div class="col-md-6">
                                            <label >RT</label>
                                            <input type="text" class="form-control" name="rt_pas">
                                          </div>
                                          <div class="col-md-6">
                                            <label >RW</label>
                                            <input type="text" class="form-control" name="rw_pas">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Desa/Kelurahan</label>
                                            <input type="text" class="form-control" name="desa_pas">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Kecamatan</label>
                                            <input type="text" class="form-control" name="kec_pas">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Kabupaten</label>
                                            <input type="text" class="form-control" name="kab_pas">
                                          </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                        <div class="col-md-6">
                            <div class="form-group">
                              <label >Order Customer Atas Nama (PIHAK KEDUA)</label>
                              <input type="text" class="form-control" name="order_cus_atas_nama_kedua">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label >NIK</label>
                              <input type="number" class="form-control" name="nik_kedua">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                        <div class="form-group">
                        <div class="col-md-12">
                              <label >Jenis Kelamin</label><br>
                        </div>
                          <div class="col-md-6">
                              <input type="radio" name="gender_kedua" value="Laki-laki" id="Laki-laki_kedua">
                              <label for="Laki-laki_kedua">Laki-laki</label>
                        </div>
                          <div class="col-md-6">
                              <input type="radio" name="gender_kedua" value="Perempuan" id="Perempuan_kedua">
                              <label for="Perempuan_kedua">Perempuan</label><br>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label >Tempat Lahir</label>
                              <input type="text" class="form-control" name="tl_kedua">
                              </div
                              ></div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label >Tanggal Lahir</label>
                                  <input type="date" class="form-control" name="tgl_kedua">
                                  </div
                                  ></div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label >Kewarganegaraan</label>
                                      <select class="form-control" name="kwn_kedua">
                                        <option value="Warga Negara Indonesia"> WNI </option>
                                        <option value="Warga Negara Asing"> WNA </option>
                                      </select>
                                      </div
                                      ></div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label >Pekerjaan</label>
                                          <input type="text" class="form-control" name="pekerjaan_kedua">
                                          </div
                                          ></div>
                                        </div>
                                        
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-grub">
                                              <label >Alamat</label>
                                              <input type="text" class="form-control" name="alamat_cus_kedua">
                                            </div>
                                          </div>

                                          <div class="col-md-6">
                                            <label >RT</label>
                                            <input type="number" class="form-control" name="rt_cus_kedua">
                                          </div>
                                          <div class="col-md-6">
                                            <label >RW</label>
                                            <input type="number" class="form-control" name="rw_cus_kedua">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Desa/Kelurahan</label>
                                            <input type="text" class="form-control" name="desa_cus_kedua">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Kecamatan</label>
                                            <input type="text" class="form-control" name="kec_cus_kedua">
                                          </div>
                                          <div class="col-md-6">
                                            <label >Kabupaten</label>
                                            <input type="text" class="form-control" name="kab_cus_kedua">
                                          </div>
                                        </div>
                                        </div>
                                        <hr>
                                        
              `;
              $("#Tambah_Data_Order_Customer").html(htmlku);
            } else {
      
              $("#Tambah_Data_Order_Customer").html('');
            }
          });
          
          </script>
