<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Customer
			<small>Data Customer</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Customer</a></li>
			<li class="active">Data Customer</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Data Customer</h3>
			</div>
			<div class="box-body">
				<a class="btn btn-primary" href="<?php echo base_url('Customer/tambah') ?>"><i class="fa fa-plus"> Tambah Customer </i></a>
				<hr>
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Action</th>
							<th>Nama</th>
							<th>Jenis</th>
							<th>Alamat</th>
							<th>Telp</th>
							<th>Pic</th>
							<th>Detail</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; foreach ($cus as $menu): ?>
						<tr>
							<td><a href="<?php echo base_url('Customer/edit/'.$menu->NO_ID_CUSTOMER) ?>" class="btn btn-social-icon btn-sm"><i class="fa fa-pencil" title="Edit"></i></a>
								<a href="<?php echo base_url('Customer/hapusCustomer/'.$menu-> NO_ID_CUSTOMER) ?>" title="hapus" onclick="javascript: return confirm('Anda Yakin Akan Menghapus ?')" class="btn btn-social-icon btn-sm"><i class="fa fa-trash" title="Hapus" style="color: red"></i></a></td>
							<td><?php echo $menu->NAMA_CUSTOMER ?></td>
							<td><?php echo $menu->JENIS_CUSTOMER ?></td>
							<td><?php echo $menu->ALAMAT_CUSTOMER ?><?php echo $menu->KABUPATEN_CUSTOMER ?></td>
							<td><?php echo $menu->NOMOR_TELEPHONE_CUSTOMER ?></td>
							<td><?php echo $menu->PIC_CUSTOMER ?></td>
							<td>
							<a href="<?php echo base_url('Customer/detailOrderCus/'.$menu->NO_ID_CUSTOMER) ?>" class="btn btn-social-icon"><i class="fa fa-shopping-cart" title="Order" style="color: green;"></i></a>
							<?php if ($menu->JENIS_CUSTOMER == "BANK") { ?>
								<a href="<?php echo base_url('Customer/goDet/'.$menu->NO_ID_CUSTOMER) ?>" class="btn btn-social-icon"><i class="fa fa-eye" title="Data Komparisi"></i></a>
							<?php } ?>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</section>
</div>
<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>