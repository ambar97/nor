<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Customer
			<small>Tambah</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Customer</a></li>
			<li class="active">Tambah Customer</li>
		</ol>
	</section>
	<section class="content">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Tambah Data Customer Baru</h3>
				</div>
				<form role="form" method="POST" action="<?php echo base_url('Customer/tambahCustomer') ?>">
					<div class="box-body">
						<div class="form-group">
							<label for="exampleInputEmail1">Nama Customer</label>
							<input type="text" class="form-control" required="" placeholder="Nama Customer" name="nm_cus">
						</div>
						<div class="form-group">
							<label>Jenis Customer</label>
							<select class="form-control" name="jenis">
								<option value="PERORANGAN">PERORANGAN</option>
								<option value="BANK">BANK</option>
								<option value="BPR">BPR</option>
								<option value="KSP">KSP</option>
								<option value="PT">PT</option>
								<option value="CV">CV</option>
							</select>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Alamat Customer</label>
							<input type="text" class="form-control" name="alamat" required="" placeholder="Alamat Lengkap Customer">
						</div>
						<div class="form-group row">	
							<div class=" col-md-4">
								<label>Kabupaten</label>
								<input type="text"  name="kab" class="form-control" required="">
							</div>
							<div class=" col-md-4">
								<label>Kecamatan</label>
								<input type="text"  name="kec" class="form-control" required="">
							</div>
							<div class=" col-md-4">
								<label>Desa / Kelurahan</label>
								<input type="text"  name="des" class="form-control" required="">
							</div>
							<div class=" col-md-6">
								<label>RT</label>
								<input type="text"  name="rt" class="form-control" required="">
							</div>
							<div class=" col-md-6">
								<label>RW</label>
								<input type="text"  name="rw" class="form-control" required="">
							</div>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">No Telp</label>
							<input type="text" class="form-control" name="telp" required="" placeholder="08xxxx">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Person In Change (PIC)</label>
							<input type="text" class="form-control" required="" name="pic" placeholder="Person yang mewakili customer / Nama customer itu sendiri">
						</div>
					</div>
					<div class="box-footer">
						<button style="float: right;" type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>