<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Proses Order</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
					<!-- <hr> -->
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>

								<!-- <th>Nama Customer</th> -->
								<th>Nama Proses</th>
								<th>Status</th>
								<th>Tgl Selesai</th>
								<th>Action</th>
								<?php if ($this->session->userdata('tipene') != 5): ?>
								<th>Cetak</th>	
								<?php endif ?>
								
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; ?>
							<?php foreach ($isi as $dd): ?>
							<tr>
								<td><?php echo $no++; ?></td>
								<td><?php echo $dd->NAMA_PROSES_ORDER ?></td>
								<td><?php echo $dd->STATUS_PROSES_ORDER_CUSTOMER ?></td>
								<td><?php echo $dd->TGLSTTS_PROSES_ORDER_CUSTOMER ?></td>
								<td>
									<?php if ($dd->STATUS_PROSES_ORDER_CUSTOMER != 'Selesai'): ?>
										<a href=""> <i class="fa fa-check"> Selesai</i> </a>
									<?php endif ?>
								</td>
								<?php if ($this->session->userdata('tipene') != 5): ?>
									<td>
									<?php $ff = substr($dd->NAMA_PROSES_ORDER,0,14); ?>
									<?php if ($ff == "Pembuatan Akta"): ?>
									<a href="#" type="button" class="btn btn-icon" data-toggle="modal" data-target="#modal-defaultq<?php echo $dd->NO_ID_OBYEK_ORDER_CUSTOMER ?>" ><i class="fa fa-print"></i> Cetak</a>
										<?php endif; ?>
								</td>
								<?php endif ?>
								
								<div class="modal fade" id="modal-defaultq<?php echo $dd->NO_ID_OBYEK_ORDER_CUSTOMER; ?>">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title">Tambah Data Pendukung Surat </h4>
												</div>
												<div class="modal-body">
													<form class="form-horizontal" method="POST" action="<?php echo base_url('ProsesPengikatan/update') ?>">
														<div class="box-body">
															<div class="form-group">
																<label >No Akta</label>
																<input type="hidden" value="<?php echo $dd->NO_ID_PROSES_ORDER_CUSTOMER; ?>" name="id_obyOrdCus" >
																<input type="text" class="form-control"  name="no_akt" required="" >
															</div>
															<div class="form-group">
																<label >Tanggal Akta</label>
																<input type="date" class="form-control"  name="tgl_akt" required="" >
															</div>
															<div class="form-group">
																<label >Jam</label>
																<input type="time" class="form-control"  name="jam_akt" required="" >
															</div>
															<!-- <div class="form-group">
																<label >Komparisi Pemilik Dokumen</label>
																<input type="text" class="form-control"  name="lgnUser" required="" >
															</div>
															<div class="form-group">
																<label >Komparisi Pasangan Pemilik Dokumen</label>
																<input type="text" class="form-control"  name="lgnUser" required="" >
															</div> -->
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
															<button type="submit" class="btn btn-primary">Save changes</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
		</section>
		<!-- /.content -->
	</div>
	<?php $this->load->view('side/footer') ?>
	<?php $this->load->view('side/js') ?>
