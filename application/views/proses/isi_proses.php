<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Proses Order</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
					<!-- <hr> -->
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<!-- <th>Nama Customer</th> -->
								<th>Nama Customer</th>
								<th>No Surat Order</th>
								<th>Customer Atas Nama</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach ($data as $data): ?>
							<tr>
								<td><?= $no++ ?></td>
								<td><?= $data->NAMA_CUSTOMER ?></td>
								<td><?= $data->NOMOR_SURAT_ORDER_CUSTOMER ?></td>
								<td><?= $data->DETAIL_ORDER_ATAS_NAMA ?></td>
								<td style="text-align: center;"> <a href="<?= base_url('ProsesPengikatan/load_index/'.$data->NO_ID_OBYEK_ORDER_CUSTOMER) ?>"> <i class="fa fa-list"></i> </a> </td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
		</section>
		<!-- /.content -->
	</div>
	<?php $this->load->view('side/footer') ?>
	<?php $this->load->view('side/js') ?>
