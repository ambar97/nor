<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Laporan
      <small>Laporan</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Laporan</a></li>
      <li class="active">Laporan</li>
    </ol>
  </section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form action="<?php echo base_url('Laporan'); ?>" method="post">
            Rentang waktu: <i class="fa fa-calendar"></i><input type="date" name="date_from" value="<?php echo $date_from; ?>"> - <input type="date" name="date_to" value="<?php echo $date_to; ?>"> <button class="btn btn-sm btn-primary" type="submit" name="button">Cari</button>
          </form>
          <div class=" table-responsive">
            <table class="table table-bordered table-striped text-center">
              <thead>
                <tr>
                  <th style="vertical-align:middle;" class="align-middle" rowspan="3">No</th>
                  <th style="vertical-align:middle;" rowspan="2" colspan="2">Akta</th>
                  <th style="vertical-align:middle;" rowspan="3">Bentuk Perbuatan Hukum</th>
                  <th style="vertical-align:middle;" rowspan="2" colspan="2">Nama Alamat dan NPWP/NIK</th>
                  <th style="vertical-align:middle;" rowspan="3">Jenis dan Nomor Hak</th>
                  <th style="vertical-align:middle;" rowspan="3">Letak Tanah dan Bangunan</th>
                  <th style="vertical-align:middle;" rowspan="2" colspan="2">Luas M<sup>2</sup</th>
                  <th style="vertical-align:middle;" rowspan="2" colspan="2">SPPT PBB</th>
                  <th style="vertical-align:middle;" rowspan="3">Harga Transaksi/Perolehan/Pengalihan Hak(Rp)</th>
                  <th style="vertical-align:middle;">Bukti Penyetoran</th>
                </tr>
                <tr>
                  <th style="vertical-align:middle;" rowspan="2">NTPN</th>
                  <th style="vertical-align:middle;">SSP Tanggal dan Jumlah</th>
                  <th style="vertical-align:middle;">SSB Tanggal dan Jumlah</th>
                </tr>
                <tr>
                  <th style="vertical-align:middle;">Nomor</th>
                  <th style="vertical-align:middle;">Tanggal</th>
                  <th style="vertical-align:middle;">Pihak Mengalihkan/Memberikan</th>
                  <th style="vertical-align:middle;">Pihak yang Menerima</th>
                  <th style="vertical-align:middle;">Tanah</th>
                  <th style="vertical-align:middle;">Bangunan</th>
                  <th style="vertical-align:middle;">NOP/Tahun</th>
                  <th style="vertical-align:middle;">NJOP(Rp)</th>
                  <th style="vertical-align:middle;">Rp</th>
                  <th style="vertical-align:middle;">Rp</th>
                </tr>
              </thead>
              <tbody>
                <?php if ($laporan == NULL) { ?>
                  <?php echo '<br><div class="alert alert-info">
                    <strong>Info!</strong> Data tidak ditemukan, silahkan isi rentang waktu di atas.
                    </div>';
                  ?>
                <?php } else { ?>
                <?php foreach ($laporan->result() as $l): ?>
                <tr>
                  <td>1</td>
                  <td><?php echo $l->NO_ID_PROSES_ORDER_CUSTOMER; ?></td>
                  <td><?php echo $l->TGL_AKTA; ?></td>
                  <td><?php echo $l->AKTA_ATAS_OBYEK; ?></td>
                  <td>
                    <?php echo $l->ATAS_NAMA_OBYEK.'<br>'.$l->ALAMAT_NAMA_OBYEK.'<br>'.$l->NIK_OBYEK; ?>
                  </td>
                  <td><?php echo $l->ATAS_NAMA_OBYEK_BARU.'<br>'.$l->ALAMAT_NAMA_OBYEK_BARU.'<br>'.$l->NIK_OBYEK_BARU; ?></td>
                  <td><?php echo $l->JENIS_OBYEK.' '.$l->NOMOR_OBYEK; ?></td>
                  <td><?php echo $l->ALAMAT_OBYEK; ?></td>
                  <td><?php echo $l->LUAS_OBYEK; ?></td>
                  <td>-</td>
                  <td><?php echo $l->NOMOR_OBYEK_PAJAK; ?></td>
                  <td><?php echo $l->NJOP; ?></td>
                  <td><?php echo $l->HARGA_TRANSAKSI; ?></td>
                  <td><?php echo $l->NTPN; ?></td>
                  <td>
                    <?php echo $l->SSP_TANGGAL.'<br>'.'Rp.'.$l->SSP_NOMINAL; ?>
                  </td>
                  <td>
                    <?php if ($l->SSB_NOMINAL==0) {
                      echo '-';
                    }else {
                      echo $l->SSB_TANGGAL.'<br>'.'Rp.'.$l->SSB_NOMINAL;
                    }; ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php } ?>
            </tbody>
          </table>
          </div
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
</div>

<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>
