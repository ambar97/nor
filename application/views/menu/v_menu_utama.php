<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Menu Utama</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<button class="btn btn-primary" data-toggle="modal" data-target="#modal-default"> <i class="fa fa-plus"> Tambah Menu </i></button>
				<div class="modal fade" id="modal-default">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Tambah Menu Baru</h4>
								</div>
								<div class="modal-body">
									<form class="form-horizontal" method="POST" action="<?php echo base_url('Menu_utama/tambah') ?>">
										<div class="box-body">
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-2 control-label">Nama Menu</label>
												<div class="col-sm-10">
													<input type="text" name="parent" value="0" hidden>
													<input type="text" class="form-control" placeholder="Nama Menu" name="nm_menu">
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-2 control-label">Alamat Menu</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" placeholder="Alamat Menu" name="almt_menu">
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-2 control-label">Nomor Urut Menu</label>
												<div class="col-sm-10">
													<input type="number" class="form-control" placeholder="Nomor Urut Menu" name="nmr_urut">
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-2 control-label">Nama Icon Menu</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" placeholder="Nama Icon Menu" name="nm_icon">
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary">Simpan </button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<!-- <th>Id</th> -->
								<th>Menu Induk</th>
								<th>Nama Menu</th>
								<th>Link Page</th>
								<th>Urutan Menu</th>
								<th>Icon</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; ?>
							<?php foreach ($data as $dd): ?>
							<tr>
								<td><?php echo $no++ ?></td>
								<!-- <td><?php echo $dd->id ?></td> -->
								<td><?php echo $dd->parent_id ?></td>
								<td><?php echo $dd->title ?></td>
								<td><?php echo $dd->url ?></td>
								<td><?php echo $dd->menu_order ?></td>
								<td><?php echo $dd->icon ?></td>
								<td>
									<button type="button" data-toggle="modal" data-target="<?php echo '#modal'.str_replace(' ', '', strtolower($dd->title)); ?> " name="button">Edit</button>
										<a href="<?php echo base_url('Menu_utama/hapusmenu/'.$dd-> id) ?>" title="hapus" onclick="javascript: return confirm('Anda Yakin Akan Menghapus ?')" class="btn btn-link btn-just-icon remove"><i class="fa fa-trash"></i></a>
									<a href="<?php echo base_url('Menu_utama/submenu/'.$dd->id) ?>" type="button" class="btn btn-icon"><i class="fa fa-list" title="Menu" style="color: orange"></i></a>
								</td>
								</tr>
							<?php endforeach ?>
						</tbody>
						<tfoot>
							<tr>
								<th>No</th>
								<th>Id</th>
								<th>Menu Induk</th>
								<th>Nama Menu</th>
								<th>Link Page</th>
								<th>Urutan Menu</th>
								<th>Icon</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
		</section>
		<!-- /.content -->
	</div>
	<?php $this->load->view('side/footer') ?>
	<?php $this->load->view('side/js') ?>

<?php foreach ($data as $d): ?>
	<div class="modal fade" id="<?php echo 'modal'.str_replace(' ', '', strtolower($d->title)); ?>">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit Menu <?php echo $d->title; ?></h4>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" method="POST" action="<?php echo base_url('Menu_utama/prosesEditMenu') ?>">
							<div class="box-body">
								<div class="form-group">
									<label>ID Menu</label>
									<div class="form-control">
										<input type="text" name="parent_id" value="<?php echo $d->parent_id; ?>" hidden>
										<input type="text" name="id_menu" value="<?php echo $d->id; ?>" readonly>
									</div>
								</div>
								<div class="form-group">
									<label >Nama Menu</label>
									<div class="form-control">
										<input type="text" class="form-control" placeholder="Nama Menu" name="nm_menu" value="<?php echo $d->title;; ?>">
									</div>
								</div>
								<div class="form-group">
									<label >Alamat Menu</label>
									<div class="form-control">
										<input type="text" class="form-control" placeholder="Alamat Menu" name="almt_menu" value="<?php echo $d->url; ?>">
									</div>
								</div>
								<div class="form-group">
									<label >Nomor Urut Menu</label>
									<div class="form-control">
										<input type="number" class="form-control" placeholder="Nomor Urut Menu" name="nmr_urut" value="<?php echo $d->menu_order; ?>">
									</div>
								</div>
								<div class="form-group">
									<label>Nama Icon Menu</label>
									<div class="form-control">
										<input type="text" class="form-control" placeholder="Nama Icon Menu" name="nm_icon" value="<?php echo $d->icon; ?>">
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary">Save changes</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
