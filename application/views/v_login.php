<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>I-NorSys | Log in</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url() ?>master/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>master/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>master/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>master/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>master/plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page" style="background-image: url(<?php echo base_url() ?>notaris.jpg); background-size: 100%" >

<div class="login-box">
	<div class="login-logo">
		<a style="color: white" href="<?php echo base_url() ?>">I-<b>NorSys</b></a>
	</div>
	<div class="login-box-body">
		<p class="login-box-msg">Sign in to start your session</p>

		<form action="<?php echo base_url('LoginUser/logindulu') ?>" method="post">
			<div class="form-group has-feedback">
				<input type="text" class="form-control" placeholder="Username" name="usernm" required="">
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
			</div>
			<div class="form-group has-feedback">
				<input type="password" required="" class="form-control" placeholder="Password" name="psw">
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>
			<div class="row">
				<div class="col-xs-8">
					<div class="checkbox icheck">
						<label>
							<!-- <input type="checkbox"> Remember Me -->
						</label>
					</div>
				</div>
				<div class="col-xs-4">
					<button type="submit" class="btn btn-primary btn-block btn-flat" >Sign In <i class="fa fa-send"></i></button>
				</div>
			</div>
		</form>
		<!-- <a href="#">I forgot my password</a><br> -->
		<a href="register.html" class="text-center">Register a new membership</a>

	</div>
	<?php $this->load->view('side/js') ?>