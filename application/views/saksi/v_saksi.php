<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Saksi</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
					<!-- <hr> -->
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama Saksi</th>
								<th>Isi Biodata Saksi</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; ?>
							<?php foreach ($saksi->result() as $dd): ?>
							<tr>
								<td><?php echo $no++; ?></td>
								<td><?php echo $dd->NAMA_SAKSI ?></td>
								<td><?php echo $dd->ISI_BIODATA ?></td>
								<td>
									<a href="<?php echo base_url('Customer/cetakAPHTbyUser/')?>" title="hapus" ><i class="fa fa-print"> Cetak</i></a>
								</td>
								</tr>
							<?php endforeach ?>
						</tbody>
						<tfoot>
							<tr>
                				<th>No</th>
								<th>Nama Saksi</th>
								<th>Isi Biodata Saksi</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
		</section>
		<!-- /.content -->
	</div>
	<?php $this->load->view('side/footer') ?>
	<?php $this->load->view('side/js') ?>
