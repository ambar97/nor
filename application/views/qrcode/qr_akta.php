<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Halaman Print A4</title>
</head>
<style type="text/css">
/* Kode CSS Untuk PAGE ini dibuat oleh http://jsfiddle.net/2wk6Q/1/ */
    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        font: 12pt "Tahoma";
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 210mm;
        min-height: 297mm;
        padding: 20mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px black solid;
        height: 257mm;
        text-align: center;
    }

    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 210mm;
            height: 297mm;
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
        #footer{
    height:50px;
    line-height:50px;
    background:#333;
    color:#fff;
}
    }
</style>
<body>
<div class="book">
    <div class="page">
        <div class="subpage">
          <p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.3pt;"><strong><span style="font-size:24px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">( NOTARIS )</span></strong></p>
          <p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.3pt;"><strong><span style="font-size:24px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">NUR AINI MAULIDA,S.H.,M.Kn.</span></strong></p>
          <p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.3pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">DAERAH KERJA KABUPATEN BANYUWANGI SK.MENTERI HUKUM DAN HAK ASASI MANUSIA REPUBLIK INDONESIA</span></p>
          <p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.3pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Nomor AHU.418-AH.02.01, Tahun 2013  <br>tanggal 30-08-2013  </span></p>
          <p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.3pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Jalan Brawijaya Nomor 18 Banyuwangi  <br>Telp.(0333) 4234411, Email: notarisnuraini@yahoo.co.id  </span></p>
          <hr style="border:1px solid black">
          <h1 style="margin-top:120px">SURAT KUASA MEMBEBANKAN HAK TANGGUNGAN</h1>
        <?php $errr = "NUR AINI MAULIDA.S.H.M.Kn" ?>
        <p></p>
        <div style="text-align:right; position:relative; margin-top:65%;" id="footer">
          <!-- <img style="text-align:right;" height="70px" src="<?php echo base_url('Cover/qr/').$errr ?>"> -->
        </div>
        </div>
    </div>

</div>
</body>
</html>
<script type="text/javascript">window.print();</script>
