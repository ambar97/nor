<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Customer
			<small>Data Customer</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Customer</a></li>
			<li class="active">Data Customer</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Data Customer</h3>
			</div>
			<div class="box-body">
				<a class="btn btn-primary" href="<?php echo base_url('Customer/tambah') ?>"><i class="fa fa-plus"> Tambah Customer </i></a>
				<hr>
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Id</th>
							<th>Nama</th>
							<th>Jenis</th>
							<th>Alamat</th>
							<th>Telp</th>
							<th>Pic</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; foreach ($cus as $menu): ?>
						<tr>
							<td><?php echo $menu->NO_ID_CUSTOMER ?></td>
							<td><?php echo $menu->NAMA_CUSTOMER ?></td>
							<td><?php echo $menu->JENIS_CUSTOMER ?></td>
							<td><?php echo $menu->ALAMAT_CUSTOMER ?><?php echo $menu->KABUPATEN_CUSTOMER ?></td>
							<td><?php echo $menu->NOMOR_TELEPHONE_CUSTOMER ?></td>
							<td><?php echo $menu->PIC_CUSTOMER ?></td>
							<td>
								<a href="<?php echo base_url('Customer/edit/'.$menu->NO_ID_CUSTOMER) ?>" type="button" class="btn btn-block btn-primary">Edit</a>
							<a type="button" class="btn btn-block btn-danger"  href="<?php echo base_url('Customer/hapusCustomer/'.$menu-> NO_ID_CUSTOMER) ?>" title="hapus" onclick="javascript: return confirm('Anda Yakin Akan Menghapus ?')">Hapus</a>
							<a href="<?php echo base_url('menu/Menu_utama/subMenu'.$menu->NO_ID_CUSTOMER) ?>" class="btn btn-block btn-success">Detail</a>
						</td>
							
						</tr>

					<?php endforeach ?>
				</tbody>
				<tfoot>
					<tr>
						<th>Rendering engine</th>
						<th>Browser</th>
						<th>Platform(s)</th>
						<th>Engine version</th>
						<th>CSS grade</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</section>
</div>
<?php $this->load->view('side/footer') ?>

<?php $this->load->view('side/js') ?>