<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Order
			<small>Edit Proses Order</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Order</a></li>
			<li class="active">Edit Proses Order</li>
		</ol>
	</section>

	<section class="content">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Edit Proses Order</h3>
			</div>
			<div class="box-body">
				<form role="form" method="POST" action="<?php echo base_url('Jenis_order/prosesEditOrder') ?>" enctype="multipart/form-data">
					<div class="box-body">
						<?php foreach ($e_ord2 as $edit ) { ?>
						<div class="form-group">
							<input type="hidden" name="id_jns_order" value="<?php echo $edit->NO_ID_JENIS_ORDER ?>">
							<!-- <label for="exampleInputEmail1">Id Proses Order</label> -->
							<input type="hidden" class="form-control" required="" value="<?php echo $edit->NO_ID_PROSES_ORDER ?>" name="id_pros" readonly>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Nama Proses Order</label>
							<input type="text" class="form-control" required="" value="<?php echo $edit->NAMA_PROSES_ORDER ?>" name="nm_pros">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Post Proses Order</label>
							<input type="text" class="form-control" required="" value="<?php echo $edit->POST_PROSES_ORDER ?>" name="pos_pros">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Keterangan</label>
							<input type="text" class="form-control" required="" value="<?php echo $edit->KETERANGAN_PROSES_ORDER ?>" name="ket">
						</div>
						<?php } ?>
					</div>
					<div class="box-footer">
						<button style="float: right;" type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>
