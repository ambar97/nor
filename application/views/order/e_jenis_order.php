<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Order
			<small>Edit Jenis Order</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Order</a></li>
			<li class="active">Edit Jenis Order</li>
		</ol>
	</section>

	<section class="content">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Edit Jenis Order</h3>
			</div>
			<div class="box-body">
				<form role="form" method="POST" action="<?php echo base_url('Jenis_order/updateJenisOrder') ?>">
					<?php foreach ($dataJns_order as $edit ) { ?>
					<div class="box-body">
						<div class="form-group">
							<input type="text" name="id_jns_order" value="<?php echo $edit->NO_ID_JENISORDER ?>" hidden>
							<label for="exampleInputEmail1">Jenis Order</label>
							<input type="text" class="form-control" required="" value="<?php echo $edit->NAMA_JENIS_ORDER ?>" name="jns_order">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Obyek Untuk Order</label>
							<input type="text" class="form-control" required="" value="<?php echo $edit->OBYEK_JENIS_ORDER ?>" name="oby_order">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Asal Obyek</label>
							<input type="text" class="form-control" required="" value="<?php echo $edit->ASAL_OBYEK_JENIS_ORDER ?>" name="asl_obyek">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Keterangan</label>
							<input type="text" class="form-control" required="" value="<?php echo $edit->KETERANGAN_JENIS_ORDER ?>" name="ket">
						</div>
					</div>
					<?php } ?>
					<div class="box-footer">
						<button style="float: right;" type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>
