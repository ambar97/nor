<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Home
			<small>User Profile</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">User Profile</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Profile</h3>
			</div>
      <div class="box-body">
        <center><img style="height:200px;" src="<?php echo base_url('notaris.jpg'); ?>" alt=""></center>
        <center><button class="btn btn-warning" type="button" name="button">Edit</button></center>
        <br>
        <table class="table">
          <?php foreach ($user->result() as $value): ?>
          <tr>
            <td><b>Nama</b> </td>
            <td><?php echo $value->NAMA_USER; ?></td>
            <td>Edit</td>
          </tr>
          <tr>
            <td><b>Nama Pengguna</b> </td>
            <td>asasdasda sadasdasd asdasdd</td>
            <td>Edit</td>
          </tr>
          <tr>
            <td><b>Password</b> </td>
            <td>Tersembunyi</td>
            <td>Edit</td>
          </tr>
          <tr>
            <td><b>Alamat</b> </td>
            <td>asasdasda sadasdasd asdasdd</td>
            <td>Edit</td>
          </tr>
          <tr>
            <td><b>Telepon</b> </td>
            <td>asasdasda sadasdasd asdasdd</td>
            <td>Edit</td>
          </tr>
        <?php endforeach; ?>
        </table>
      </div>
	</div>
</section>
</div>

<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>
