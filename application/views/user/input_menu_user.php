<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Menu User
            <small>Input Menu User</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Home</a>
            </li>
            <?php foreach ($user->result() as $u): ?>
            <li class="active">Menu
                <?php echo $nama_user = $u->NAMA_USER; ?></li>
            <?php $id_user = $u->ID_USER; ?>
            <?php endforeach; ?>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Menu
                    <?php echo $nama_user; ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <button
                    class="btn btn-primary"
                    data-toggle="modal"
                    data-target="#modal-default">
                    <i class="fa fa-plus">
                        Tambah Menu
                    </i>
                </button>
                <div class="modal fade" id="modal-default">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Tambah Menu untuk
                                    <?php echo $nama_user; ?></h4>
                            </div>
                            <div class="modal-body">
                                <form
                                    class="form-horizontal"
                                    method="POST"
                                    action="<?php echo base_url('Menu_user/prosesInputMenuUser') ?>">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Menu</label>
                                            <select name="MENU_ID">
                                                <?php foreach ($menu->result() as $m): ?>
                                                <option value="<?php echo $m->id; ?>"><?php echo $m->title; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <input
                                                type="text"
                                                name="MEMBER_ID"
                                                value="<?php echo $id_user; ?>"
                                                hidden="hidden">

                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Simpan
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <td>ID PEGAWAI</td>
                            <td>NAMA MENU</td>
                            <td>MENU INDUK</td>
                            <td>ACTION</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; foreach ($member_menu->result() as $menu): ?>
                        <?php $nama_menu = $this->M_model->selectwhere('menu', array('id'=>$menu->MENU_ID)); ?>
                        <?php foreach ($nama_menu->result() as $nm): ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <!-- <td><?php echo $id_member_menu = $menu->NO_ID_MEMBER_MENU; ?></td> -->
                            <td><?php echo $nm->title; ?></td>
                            <td><?php echo $nm->parent_id; ?></td>
                            <td>
                                <a
                                    href="<?php echo base_url('Menu_user/prosesHapusMenuUser/'.$id_user.'/'.$id_member_menu); ?>"
                                    class="btn btn-danger">Hapus</a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view('side/footer') ?>

<?php $this->load->view('side/js') ?>