-- Adminer 4.7.6 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `0_0_jenis_order`;
CREATE TABLE `0_0_jenis_order` (
  `NO_ID_JENISORDER` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `NAMA_JENIS_ORDER` varchar(150) DEFAULT NULL,
  `OBYEK_JENIS_ORDER` varchar(150) DEFAULT NULL,
  `ASAL_OBYEK_JENIS_ORDER` varchar(150) DEFAULT NULL,
  `KETERANGAN_JENIS_ORDER` longtext,
  PRIMARY KEY (`NO_ID_JENISORDER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `0_0_jenis_order` (`NO_ID_JENISORDER`, `NAMA_JENIS_ORDER`, `OBYEK_JENIS_ORDER`, `ASAL_OBYEK_JENIS_ORDER`, `KETERANGAN_JENIS_ORDER`) VALUES
(0000000001,	'HAK TANGGUNGAN (HT)',	'SHM',	'SHM',	'Order tanpa perlu melakukan perubahan status terhadap Obyek Tanggungan'),
(0000000002,	'HAK TANGGUNGAN (HT)',	'SHM',	'SHM',	'Order untuk dilakukan perubahan terhadap status kepemilikan Obyek Hak Tanggungan, Obyek harus dibalik nama terlebih dahulu'),
(0000000003,	'HAK TANGGUNGAN (HT)',	'SHM',	'AJB/Letter C',	'Order untuk peningkatan obyek yang masih berupa obyek yang tidak bisa diikat dengan SKMHT ataupun Hak Tanggungan, sehingga perlu dilakukan proses balik nama ataupun proses peningkatan status obyek tersebut'),
(0000000004,	'SURAT KUASA MEMBEBANKAN HAK TANGGUNGAN (SKMHT)',	'SHM',	'SHM',	'Pengikatan SKMHT untuk obyek tanpa adanya perubahan status ataupun balik nama'),
(0000000005,	'SURAT KUASA MEMBEBANKAN HAK TANGGUNGAN (SKMHT)',	'SHM',	'SHM',	'Order untuk dilakukan perubahan terhadap status kepemilikan Obyek Hak Tanggungan, Obyek harus dibalik nama terlebih dahulu'),
(0000000006,	'SURAT KUASA MEMBEBANKAN HAK TANGGUNGAN (SKMHT)',	'SHM',	'AJB/Letter C',	'Order untuk peningkatan obyek yang masih berupa obyek yang tidak bisa diikat dengan SKMHT ataupun Hak Tanggungan, sehingga perlu dilakukan proses balik nama ataupun proses peningkatan status obyek tersebut');

DROP TABLE IF EXISTS `0_0_proses_order`;
CREATE TABLE `0_0_proses_order` (
  `NO_ID_JENIS_ORDER` int(10) unsigned zerofill NOT NULL,
  `NO_ID_PROSES_ORDER` int(15) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `NAMA_PROSES_ORDER` varchar(250) DEFAULT NULL,
  `POST_PROSES_ORDER` varchar(250) DEFAULT NULL,
  `KETERANGAN_PROSES_ORDER` longtext,
  PRIMARY KEY (`NO_ID_PROSES_ORDER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `0_0_proses_order` (`NO_ID_JENIS_ORDER`, `NO_ID_PROSES_ORDER`, `NAMA_PROSES_ORDER`, `POST_PROSES_ORDER`, `KETERANGAN_PROSES_ORDER`) VALUES
(0000000001,	000000000000001,	'Pembuatan Akta SKMHT',	'Pembuat Akta',	'Pembuatan untuk Akta SKMHT yang akan dimintakan tanda tangan kepada Pemimpin Customer'),
(0000000001,	000000000000002,	'Pembuatan Akta APHT',	'Pembuat Akta',	'Proses pembuatan Akta APHT di Kantor Notaris'),
(0000000001,	000000000000003,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	'BPN',	'Proses ini berada di BPN'),
(0000000001,	000000000000004,	'Kepala Subseksi Pendaftaran Hak Tanah',	'Kepala Subseksi Pendaftaran Hak Tanah BPN',	'Proses ini berada di BPN'),
(0000000001,	000000000000005,	'Kepala Seksi Hubungan Hukum Pertanahan',	'Kepala Seksi Hubungan Hukum Pertanahan BPN',	'Proses ini berada di BPN'),
(0000000001,	000000000000006,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	'Pelaksana Subseksi Pendaftaran Hak Tanah BPN',	'Proses ini berada di BPN dengan seksi yang berbeda'),
(0000000001,	000000000000007,	'Loket Penyanan Penyerahan',	'Loket Penyanan Penyerahan BPN',	'Penyerahan Akta yang sudah selesai'),
(0000000001,	000000000000008,	'Pembayaran Biaya Akta dan Sertifikat HT',	'Pembayaran seluruh biaya di Loket BPN',	'Pembayaran biaya dan penyerahan Akta Hak Tanggungan dan Sertifikat Hak Tanggungan'),
(0000000002,	000000000000009,	'Pembuatan Akta Jual Beli (AJB)',	'Proses berada di kantor Notaris',	'Proses pembuatan Akta Jual Beli (AJB) antara penjual atau pembeli untuk selanjutnya dilanjutkan dengan proses SKMHT dan APHT'),
(0000000002,	000000000000010,	'SPS PNBP',	'SPS PNBP',	'SPS PNBP'),
(0000000002,	000000000000011,	'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT',	'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT (BPN)',	'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT'),
(0000000002,	000000000000012,	'Kepala Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT',	'Kepala Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT (BPN)',	'Kepala Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT'),
(0000000002,	000000000000013,	'Kepala Seksi Hubungan Hukum Pertanahan',	'Kepala Seksi Hubungan Hukum Pertanahan (BPN)',	'Kepala Seksi Hubungan Hukum Pertanahan\r\n\r\n\r\n'),
(0000000002,	000000000000014,	'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT',	'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT (BPN)',	'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT'),
(0000000002,	000000000000015,	'Loket Pelayanan Penyerahan',	'Loket Pelayanan Penyerahan (BPN)',	'Loket Pelayanan Penyerahan'),
(0000000002,	000000000000016,	'Pendaftaran',	'Pendaftaran di BPN',	'Pendaftaran seluruh formulir Pengikatan di BPN'),
(0000000002,	000000000000017,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	'Pelaksana Subseksi Pendaftaran Hak Tanah BPN',	'Pelaksana Subseksi Pendaftaran Hak Tanah'),
(0000000002,	000000000000018,	'Kepala Subseksi Pendaftaran Hak Tanah',	'Kepala Subseksi Pendaftaran Hak Tanah BPN',	'Kepala Subseksi Pendaftaran Hak Tanah BPN'),
(0000000002,	000000000000019,	'Kepala Seksi Hubungan Hukum Pertanahan',	'Kepala Seksi Hubungan Hukum Pertanahan BPN',	'Kepala Seksi Hubungan Hukum Pertanahan'),
(0000000002,	000000000000027,	'sitepar',	'sitepar',	'siteparawa');

DROP TABLE IF EXISTS `0_1_data_customer`;
CREATE TABLE `0_1_data_customer` (
  `NO_ID_CUSTOMER` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `NAMA_CUSTOMER` varchar(250) DEFAULT NULL,
  `JENIS_CUSTOMER` varchar(50) DEFAULT NULL,
  `ALAMAT_CUSTOMER` varchar(255) DEFAULT NULL,
  `KABUPATEN_CUSTOMER` varchar(100) DEFAULT NULL,
  `KECAMATAN_CUSTOMER` varchar(100) DEFAULT NULL,
  `DESA_KELURAHAN_CUSTOMER` varchar(100) DEFAULT NULL,
  `RT_CUSTOMER` varchar(5) DEFAULT NULL,
  `RW_CUSTOMER` varchar(5) DEFAULT NULL,
  `NOMOR_TELEPHONE_CUSTOMER` varchar(15) DEFAULT NULL,
  `PIC_CUSTOMER` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`NO_ID_CUSTOMER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `0_1_data_customer` (`NO_ID_CUSTOMER`, `NAMA_CUSTOMER`, `JENIS_CUSTOMER`, `ALAMAT_CUSTOMER`, `KABUPATEN_CUSTOMER`, `KECAMATAN_CUSTOMER`, `DESA_KELURAHAN_CUSTOMER`, `RT_CUSTOMER`, `RW_CUSTOMER`, `NOMOR_TELEPHONE_CUSTOMER`, `PIC_CUSTOMER`) VALUES
(0000000001,	'PT. Bank Negara Indonesia (Persero) Tbk.',	'BANK',	'Jl. Brigjend Katamso No, 46 Banyuwangi',	'Banyuwangi',	'Banyuwangi',	'kelurahan Tukang Kayu',	NULL,	NULL,	'0333-421946',	'HESTY'),
(0000000002,	'PT. Bank Tabungan Negara (Persero) Tbk.',	'BANK',	'Jalan Brawijaya Ruko Brawijaya B1-B2',	'Banyuwangi',	'Banyuwangi',	'kelurahan Tukang Kayu',	'001',	'002',	'0333',	'PT. MALINDO'),
(0000000006,	'HIDAYATIN CS',	'PERORANGAN',	'JL. PB. SUDIRMAN ',	'BANYUWANGI',	'BANYUWANGI',	'PANDEREJO',	'001',	'001',	'085231576156',	'HIDAYATIN CS');

DROP TABLE IF EXISTS `0_1_detail_customer`;
CREATE TABLE `0_1_detail_customer` (
  `NO_ID_DETAIL_CUSTOMER` int(15) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `NO_ID_CUSTOMER` int(15) unsigned zerofill DEFAULT NULL,
  `NAMA_COMPARATOR_CUSTOMER` varchar(250) DEFAULT NULL,
  `NO_KTP_COMPARATOR_CUSTOMER` varchar(50) NOT NULL,
  `ALAMAT_COMPARATOR_CUSTOMER` longtext,
  `TEMPAT_LAHIR` varchar(50) NOT NULL,
  `TANGGAL_LAHIR` date NOT NULL,
  `KABUPATEN_COMPARATOR_CUSTOMER` varchar(250) DEFAULT NULL,
  `KECAMATAN_COMPARATOR_CUSTOMER` varchar(250) DEFAULT NULL,
  `DESA_KELURAHAN_COMPARATOR_CUSTOMER` varchar(250) DEFAULT NULL,
  `RT_COMPARATOR_CUSTOMER` varchar(50) DEFAULT NULL,
  `RW_COMPARATOR_CUSTOMER` varchar(50) DEFAULT NULL,
  `ISI_COMPARATOR_CUSTOMER` longtext,
  `KOMPARISI_PUSAT` longtext NOT NULL,
  `STATUS_COMPARATOR` text NOT NULL,
  PRIMARY KEY (`NO_ID_DETAIL_CUSTOMER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `0_1_detail_customer` (`NO_ID_DETAIL_CUSTOMER`, `NO_ID_CUSTOMER`, `NAMA_COMPARATOR_CUSTOMER`, `NO_KTP_COMPARATOR_CUSTOMER`, `ALAMAT_COMPARATOR_CUSTOMER`, `TEMPAT_LAHIR`, `TANGGAL_LAHIR`, `KABUPATEN_COMPARATOR_CUSTOMER`, `KECAMATAN_COMPARATOR_CUSTOMER`, `DESA_KELURAHAN_COMPARATOR_CUSTOMER`, `RT_COMPARATOR_CUSTOMER`, `RW_COMPARATOR_CUSTOMER`, `ISI_COMPARATOR_CUSTOMER`, `KOMPARISI_PUSAT`, `STATUS_COMPARATOR`) VALUES
(000000000000001,	000000000000001,	'I PUTU GEDE RESTU',	'3515082501700006',	'Jl. Pinang AA2/14',	'SINGARAJA',	'1970-01-25',	'Kabupaten Sidoarjo',	'Kecamatan Sidoarjo',	'Kelurahan Banjarbendo',	'016c',	'007',	'Tuan I PUTU GEDE RESTU, lahir di Singaraja, tanggal 25 (Dua Puluh Lima) bulan Januari tahun 1970 (seribu sembilan ratus tujuh puluh), Warga Negara Indonesia, Pemimpin cabang Kantor Cabang Banyuwang, Perseroan Terbatas PT. Bank Negara Indonesia (Persero) Tbk., bertempat tinggal di Jalan Pinang AA2/14 RT 016 RW 007, Desa/Kelurahan Banjarbendo, Kecamatan Sidoarjo, Kabupaten Sidoarjo, Kabupaten Sidoarjo, Pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3515082501700006. Pada saat ini berada di Banyuwangi',	'asd',	'AKTIF'),
(000000000000002,	000000000000002,	'LIZZIYA FIJRIANI',	'3509194702680002',	'Jalan Nusantara D-12A Lingkungan Condro',	'Surabaya',	'1968-02-04',	'Kabupaten Jember',	'Kecamatan Kaliwates',	'Kelurahan Kaliwates',	'004',	'012',	'Nyonya LIZZIYA FIJRIANI, Lahir di Surabaya, tanggal 04-02-1968 (empat Februari tahun seribu Sembilan ratus enam puluh delapan) Warga Negara Indonesia, Karyawan BUMN, bertempat tinggal di Jalan Nusantara D-12A Lingkungan Condro, Rukun Tetangga 004, Rukun Warga 012, Kelurahan Kaliwates, Kecamatan Kaliwates, Kabupaten Jember. Pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3509194702680002. Pada saat ini berada di Banyuwangi.',	'Menurut keterangannya dalam hal ini bertindak dalam kedudukannya selaku Deputy Branch Manajer Business PT. BANK TABUNGAN NEGARA (Persero), Tbk Cabang Banyuwangi, berdasarkan Surat Keputusan Direksi Nomor 168/DIR/2017, tanggal 06 April 2017 jo Akta Kuasa Direksi PT. BANK TABUNGAN NEGARA (Persero) Nomor 02, tanggal 25 Oktober 2011, dibuat dihadapan HANNA WIDJAJA, Sarjana Hukum, Magister Sains Notaris di Jakarta, dengan demikian sah bertindak untuk dan atas nama PT. BANK TABUNGAN NEGARA (Persero), Tbk, sesuai dengan Anggaran Dasar yang dimuat terakhir dalam Akta Notaris FATHIAH HELMI, Sarjana Hukum Nomor 7, tanggal 12 Oktober 2009 yang telah mendapat persetujuan sesuai Keputusan Menteri Hukum dan Hak Asasi Manusia Republik Indonesia IVo.AHU-49309.AH. 01.02 Tahun 2009 tanggal 13 Oktober 2009, juncto Akta Pernyataan  Direksi PT. BANK TABUNGAN NEGARA (Persero), Tbk Nomor 43, tanggal 29 Oktober 2009, juncto Akta Keputusan RUPS Luar Biasa Nomor 71, tanggal 30 Desember -2009 beralamat di Jalan Brawijaya Ruko Brawijaya B1-B2 Banyuwangi.',	'AKTIF'),
(000000000000003,	000000000000003,	'qwe',	'33333333',	'asd',	'qwe',	'2019-12-04',	'qwe',	'we',	'qweqwe',	'qwe',	'qw',	'qwe',	'qweq',	'AKTIF'),
(000000000000004,	000000000000003,	'asdasd',	'3333333333333',	'fsd',	'dfsdf',	'2019-11-27',	'dsf',	'sdfsd',	'sdf',	'd',	'fsdf',	'sdf',	'sdfsdf',	'AKTIF'),
(000000000000006,	000000000000005,	'petepar',	'33333333',	'asd',	'asd',	'2019-12-10',	'asd',	'asd',	'asd',	'asdcc',	'asd',	'asd',	'asdasd',	'AKTIF'),
(000000000000007,	000000000000006,	'jon',	'998877766555',	'pangsud',	'Madiun',	'1997-08-26',	'Kab. Madiun',	'Taman',	'Mangunharjo',	'002c',	'003',	'sitepar pergi ke bandara lion air karena ingin pergi ke new york',	'sitepar pergi ke bandara lion air karena ingin pergi ke amsterdam',	'AKTIF');

DROP TABLE IF EXISTS `0_2_data_saksi`;
CREATE TABLE `0_2_data_saksi` (
  `NO_ID_SAKSI` int(10) NOT NULL AUTO_INCREMENT,
  `NAMA_SAKSI` varchar(100) NOT NULL,
  `ISI_BIODATA` text NOT NULL,
  PRIMARY KEY (`NO_ID_SAKSI`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `0_2_data_saksi` (`NO_ID_SAKSI`, `NAMA_SAKSI`, `ISI_BIODATA`) VALUES
(1,	'INDAH RATNASARI',	'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001'),
(2,	'AMALINDA PRATIWI',	'lahir di Banyuwangi, tanggal 21-01-2000 (dua puluh satu Januari tahun dua ribu), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Jalan Batang Hari, Rukun Tetangga 002, Rukun Warga 005, Kelurahan Penataban, Kecamatan Giri, Kabupaten Banyuwangi. Pemilik Kartu Tanda Penduduk Nomor 3510176101000001');

DROP TABLE IF EXISTS `0_user`;
CREATE TABLE `0_user` (
  `ID_USER` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `NAMA_USER` varchar(255) DEFAULT NULL,
  `LOGIN_USER` varchar(255) DEFAULT NULL,
  `PSWD_USER` varchar(255) DEFAULT NULL,
  `STATUS_LOGIN` int(11) NOT NULL DEFAULT '0',
  `ADDRS_USER` varchar(255) DEFAULT NULL,
  `BSCSLR_USER` int(11) DEFAULT NULL,
  `PHONE_USER` varchar(255) DEFAULT NULL,
  `PICT_USER` varchar(255) DEFAULT NULL,
  `STATUS_USER` varchar(255) DEFAULT NULL,
  `TYPE_USER` int(11) NOT NULL,
  `id_insert` int(5) unsigned zerofill NOT NULL,
  PRIMARY KEY (`ID_USER`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `0_user` (`ID_USER`, `NAMA_USER`, `LOGIN_USER`, `PSWD_USER`, `STATUS_LOGIN`, `ADDRS_USER`, `BSCSLR_USER`, `PHONE_USER`, `PICT_USER`, `STATUS_USER`, `TYPE_USER`, `id_insert`) VALUES
(00001,	'SUPER ADMIN1',	'ADMIN1',	'e00cf25ad42683b3df678c61f42c6bda',	0,	'',	0,	'',	'',	'AKTIF',	1,	00001),
(00002,	'NUR AINI MAULIDA',	'NURAINI02',	'359a5096b794d883f7faa67334f82ff9',	1,	'Jl. Brawijaya No. 18 Banyuwangi',	NULL,	'081232633100',	'nurainipict.jpeg',	'AKTIF',	4,	00001),
(00004,	'AMALINDA PRATIWI',	'LINDA21',	'cd3d317991a454395391187fa586a6b6',	0,	'Jl. Batanghari Gg Kektanu No. 40 Penataban',	NULL,	'081259201874',	'linda01.jpeg',	'AKTIF',	0,	00001),
(00006,	'Anas Abiem Bahar',	'biem',	'd51a22ed45c4da9bd0dc10044da8cf60',	0,	'asas',	0,	'5454',	NULL,	'AKTIF',	0,	00001),
(00007,	'Christopher Nanda J',	'cnjonathan',	'0a799bba991611d5ca64c86f6eae7d70',	0,	'Madiun',	0,	'0895354573776',	NULL,	NULL,	0,	00001),
(00011,	'INDAH RATNASARI',	'INDAH01I',	'fd2b57617b1f225ffb929e63d843fa54',	0,	'Desa Olehsari RT 004 RW 002 Kecamatan Glagah, Kabupaten Banyuwangi',	0,	'082245143469',	NULL,	'AKTIF',	3,	00002),
(00009,	'AMALINDA PRATIWI',	'LINDA01I',	'feafc62c68c030312fc4b5327150e85f',	1,	'Jl. Batanghari Gg Kektanu No. 40 Penataban',	0,	'081259201874',	NULL,	'AKTIF',	3,	00002),
(00010,	'AMALINDA PRATIWI',	'LINDA02P',	'88a4ec7423c31078f38027b309fe3614',	1,	'Jl. Batanghari Gg Kektanu No. 40 Penataban',	0,	'081259201874',	NULL,	'AKTIF',	2,	00002),
(00012,	'INDAH RATNASARI',	'INDAH02P',	'135629e8d25ffe122e728a2b1dfc2914',	0,	'Desa Olehsari RT 004 RW 002 Kecamatan Glagah, Kabupaten Banyuwangi',	0,	'082245143469',	NULL,	'AKTIF',	2,	00002),
(00013,	'NADIA UMARO',	'NADIA01I',	'b507128ac77b0e78a543d74be0c2c816',	0,	'Jl. RW Monginsidi RT 002 RW 003 Kelurahan Pengajuran, Kecamatan Banyuwangi, Kabupaten Banyuwangi',	0,	'082332820336',	NULL,	'AKTIF',	3,	00002),
(00014,	'NADIA UMARO',	'NADIA02P',	'9a28afdf4128b6bb55cda691b1d27530',	0,	'Jl. RW Monginsidi RT 002 RW 003 Kelurahan Pengajuran, Kecamatan Banyuwangi, Kabupaten Banyuwangi',	0,	'082332820336',	NULL,	'AKTIF',	2,	00002),
(00015,	'VINA ISTIAWATI AGUSTIN',	'VINA01I',	'651d53ce41d387a2eb6a38e864dfce40',	0,	'Jl. Karimunjawa No. 23 Kelurahan Lateng, Kecamatan Banyuwangi',	0,	'08970660615',	NULL,	'AKTIF',	3,	00002),
(00016,	'VINA ISTIAWATI AGUSTIN',	'VINA02P',	'd11b4303b11e16ebf9346a8a6009d6bc',	0,	'Jl. Karimunjawa No. 23 Kelurahan Lateng, Kecamatan Banyuwangi',	0,	'08970660615',	NULL,	'AKTIF',	2,	00002);

DROP TABLE IF EXISTS `1_0_data_order_customer`;
CREATE TABLE `1_0_data_order_customer` (
  `NO_ID_ORDER_CUSTOMER` int(15) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `NO_ID_CUSTOMER` int(10) unsigned zerofill DEFAULT NULL,
  `NOMOR_SURAT_ORDER_CUSTOMER` varchar(250) DEFAULT NULL,
  `TANGGAL_SURAT_ORDER_CUSTOMER` date DEFAULT NULL,
  `NOMOR_LAIN_ORDER_CUSTOMER` varchar(250) DEFAULT NULL,
  `TANGGAL_NOMOR_LAIN_ORDER_CUSTOMER` date DEFAULT NULL,
  `KETERANGAN_ORDER_CUSTOMER` longtext,
  PRIMARY KEY (`NO_ID_ORDER_CUSTOMER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `1_0_data_order_customer` (`NO_ID_ORDER_CUSTOMER`, `NO_ID_CUSTOMER`, `NOMOR_SURAT_ORDER_CUSTOMER`, `TANGGAL_SURAT_ORDER_CUSTOMER`, `NOMOR_LAIN_ORDER_CUSTOMER`, `TANGGAL_NOMOR_LAIN_ORDER_CUSTOMER`, `KETERANGAN_ORDER_CUSTOMER`) VALUES
(000000000000001,	0000000004,	'3333',	'2019-12-04',	'222',	'2019-12-24',	'asdasd'),
(000000000000002,	0000000005,	'234',	'2019-12-11',	'234',	'2019-12-13',	'asudgausgdbahgbdysgfbsgdladasjhdhagsfvakjshgdbassgdasjhdgajsd'),
(000000000000003,	0000000006,	'99887766',	'2019-12-04',	'384765348',	'2019-12-25',	'petepar'),
(000000000000004,	0000000006,	'23476',	'2019-12-18',	'384765348',	'2019-12-28',	'petear ai awa'),
(000000000000005,	0000000001,	'BWI/6.1/257',	'2019-11-18',	'NOR/276/XI/2019',	'2019-11-19',	'Order untuk pengikatan Hak Tanggungan 2 SHM an. H. TUMIRIN'),
(000000000000006,	0000000001,	'BWI/6/3536',	'2019-11-19',	'',	'2019-11-19',	'Cheking dan Pengikatan HT I');

DROP TABLE IF EXISTS `1_1_detail_order_customer`;
CREATE TABLE `1_1_detail_order_customer` (
  `NO_ID_DETAIL_ORDER` int(15) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `NO_ID_ORDER_CUSTOMER` int(15) unsigned zerofill DEFAULT NULL,
  `NO_ID_JENISORDER` int(10) unsigned zerofill DEFAULT NULL,
  `DETAIL_ORDER_ATAS_NAMA` varchar(250) DEFAULT NULL,
  `PASANGAN_DETAIL_ORDER` varchar(250) NOT NULL,
  `BIODATA_PASANGAN_DETAIL_ORDER` text NOT NULL,
  `SAKSI_SATU` varchar(250) NOT NULL,
  `BIODATA_SAKSI_SATU` text NOT NULL,
  `SAKSI_DUA` varchar(250) NOT NULL,
  `BIODATA_SAKSI_DUA` text NOT NULL,
  `NOMINAL_DETAIL_ORDER` int(15) DEFAULT NULL,
  `KETERANGAN_DETAIL_ORDER` longtext,
  PRIMARY KEY (`NO_ID_DETAIL_ORDER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `1_1_detail_order_customer` (`NO_ID_DETAIL_ORDER`, `NO_ID_ORDER_CUSTOMER`, `NO_ID_JENISORDER`, `DETAIL_ORDER_ATAS_NAMA`, `PASANGAN_DETAIL_ORDER`, `BIODATA_PASANGAN_DETAIL_ORDER`, `SAKSI_SATU`, `BIODATA_SAKSI_SATU`, `SAKSI_DUA`, `BIODATA_SAKSI_DUA`, `NOMINAL_DETAIL_ORDER`, `KETERANGAN_DETAIL_ORDER`) VALUES
(000000000000001,	000000000000001,	0000000001,	'yuyuyuyu sdfsdfsd',	'nana',	'jaksjdlasdlknaskd',	'INDAH RATNASARI',	'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001',	'AMALINDA PRATIWI',	'lahir di Banyuwangi, tanggal 21-01-2000 (dua puluh satu Januari tahun dua ribu), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Jalan Batang Hari, Rukun Tetangga 002, Rukun Warga 005, Kelurahan Penataban, Kecamatan Giri, Kabupaten Banyuwangi. Pemilik Kartu Tanda Penduduk Nomor 3510176101000001',	60000,	'Mbohh lahhhh cok'),
(000000000000002,	000000000000001,	0000000001,	'sadasd',	'bamabam',	'daskdnlabsdjbasdamsdnlahsdlk',	'INDAH RATNASARI',	'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001',	'AMALINDA PRATIWI',	'lahir di Banyuwangi, tanggal 21-01-2000 (dua puluh satu Januari tahun dua ribu), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Jalan Batang Hari, Rukun Tetangga 002, Rukun Warga 005, Kelurahan Penataban, Kecamatan Giri, Kabupaten Banyuwangi. Pemilik Kartu Tanda Penduduk Nomor 3510176101000001',	750000,	'fadfadfdf'),
(000000000000003,	000000000000001,	0000000003,	'Rangga',	'',	'',	'',	'',	'',	'',	14000000,	'Saya tidak tahu'),
(000000000000004,	000000000000000,	0000000005,	'Toper Gans',	'',	'',	'',	'',	'',	'',	2147483647,	'shhdshsdah'),
(000000000000005,	NULL,	0000000003,	'Toper',	'',	'',	'',	'',	'',	'',	32432,	'dsfsdfds'),
(000000000000010,	000000000000003,	0000000004,	'RANGGA AKHIR APRIAN',	'',	'',	'',	'',	'',	'',	100000000,	'ADA DEH'),
(000000000000011,	000000000000005,	0000000002,	'fghj',	'yui',	'tyuio',	'AMALINDA PRATIWI',	'lahir di Banyuwangi, tanggal 21-01-2000 (dua puluh satu Januari tahun dua ribu), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Jalan Batang Hari, Rukun Tetangga 002, Rukun Warga 005, Kelurahan Penataban, Kecamatan Giri, Kabupaten Banyuwangi. Pemilik Kartu Tanda Penduduk Nomor 3510176101000001',	'INDAH RATNASARI',	'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001',	121212,	'sfs'),
(000000000000012,	000000000000006,	0000000001,	'SITI FATIMAH',	'RANGGA ALFIADORI',	'Lingkungan Pancoran, RT 002, RW 001, Desa/Kelurahan Banjarsari, Kec. Glagah, Kab. Banyuwangi',	'INDAH RATNASARI',	'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001',	'AMALINDA PRATIWI',	'lahir di Banyuwangi, tanggal 21-01-2000 (dua puluh satu Januari tahun dua ribu), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Jalan Batang Hari, Rukun Tetangga 002, Rukun Warga 005, Kelurahan Penataban, Kecamatan Giri, Kabupaten Banyuwangi. Pemilik Kartu Tanda Penduduk Nomor 3510176101000001',	100000000,	'');

DROP TABLE IF EXISTS `1_2_detail_obyek_order_customer`;
CREATE TABLE `1_2_detail_obyek_order_customer` (
  `NO_ID_OBYEK_ORDER_CUSTOMER` int(15) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `NO_ID_DETAIL_ORDER` int(15) unsigned zerofill DEFAULT NULL,
  `JENIS_OBYEK` varchar(250) DEFAULT NULL,
  `NO_ID_JENISORDER` int(15) NOT NULL,
  `NOMOR_OBYEK` varchar(250) DEFAULT NULL,
  `TANGGAL_OBYEK` date DEFAULT NULL,
  `JENIS_IDENTIFIKASI_OBYEK` varchar(250) DEFAULT NULL,
  `NOMOR_IDENTIFIKASI_OBYEK` varchar(250) DEFAULT NULL,
  `TANGGAL_IDENTIFIKASI_OBYEK` date DEFAULT NULL,
  `ATAS_NAMA_OBYEK` varchar(250) DEFAULT NULL,
  `LUAS_OBYEK` int(10) NOT NULL,
  `NOMOR_OBYEK_PAJAK` varchar(250) NOT NULL,
  `KETERANGAN_LAIN_OBYEK` longtext,
  `AKTA_ATAS_OBYEK` varchar(250) DEFAULT NULL,
  `TANGGAL_AKTA_ATAS_OBYEK` date DEFAULT NULL,
  `NOMINAL_PARTIAL_OBYEK` int(15) NOT NULL,
  PRIMARY KEY (`NO_ID_OBYEK_ORDER_CUSTOMER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `1_2_detail_obyek_order_customer` (`NO_ID_OBYEK_ORDER_CUSTOMER`, `NO_ID_DETAIL_ORDER`, `JENIS_OBYEK`, `NO_ID_JENISORDER`, `NOMOR_OBYEK`, `TANGGAL_OBYEK`, `JENIS_IDENTIFIKASI_OBYEK`, `NOMOR_IDENTIFIKASI_OBYEK`, `TANGGAL_IDENTIFIKASI_OBYEK`, `ATAS_NAMA_OBYEK`, `LUAS_OBYEK`, `NOMOR_OBYEK_PAJAK`, `KETERANGAN_LAIN_OBYEK`, `AKTA_ATAS_OBYEK`, `TANGGAL_AKTA_ATAS_OBYEK`, `NOMINAL_PARTIAL_OBYEK`) VALUES
(000000000000002,	000000000000001,	'SHGU',	1,	'54454454',	'2019-12-26',	'544',	'565',	'2019-12-18',	'Mbohh lahh',	0,	'4234234',	'0',	NULL,	NULL,	88766),
(000000000000003,	000000000000001,	'SHM',	1,	'123',	'2019-12-13',	'fadg',	'23423',	'2019-12-14',	'dasfasdf',	90,	'12345',	'0',	NULL,	NULL,	145),
(000000000000010,	000000000000008,	'AJB/LETTER C/PETHOK',	3,	'120000',	'2019-12-20',	'kasdknaksd',	'213123123',	'2019-12-27',	'mdkmdkmkmf',	890000,	'80000000',	'bsnfsnkdfksmdflakdflk akdsnfklansdlf asdfln',	NULL,	NULL,	5599999),
(000000000000011,	000000000000006,	'SHGU',	1,	'900000',	'2019-12-21',	'asjndaslndlasknd',	'21312312',	'2019-12-21',	'mamanamasn',	800000,	'7000',	'mbuhhh lahh cokk cokk',	NULL,	NULL,	500000),
(000000000000012,	000000000000009,	'SHM',	1,	'12345',	'2019-12-05',	'lha pye',	'8247923',	'2019-12-31',	'kmanaman',	0,	'70030303',	'taekk per per calll wsssss yoo call',	NULL,	NULL,	63838384),
(000000000000013,	000000000000010,	'AJB/LETTER C/PETHOK',	4,	'123456',	'2019-12-27',	'ooke dehh',	'78908282',	'2019-12-28',	'TOPER',	7000,	'50000',	'yA',	NULL,	NULL,	40000),
(000000000000014,	000000000000011,	'SHGU',	2,	'2323',	'2019-12-26',	'fghj',	'2345',	'2019-12-19',	'tyjk',	332,	'3456',	'cfcvgbhnjkm',	NULL,	NULL,	0),
(000000000000015,	000000000000012,	'SHM',	1,	'01432',	'2015-08-10',	'SU',	'00027/Banjarsari/2014',	'2014-09-17',	'RANGGA ALFIADORI',	3565,	'351016001101701140',	'',	NULL,	NULL,	100000000);

DROP TABLE IF EXISTS `1_3_dokumen`;
CREATE TABLE `1_3_dokumen` (
  `NO_ID_DOKUMEN` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `NO_ID_DETAIL_ORDER` int(10) unsigned zerofill NOT NULL,
  `JENIS_DOKUMEN_UPLOAD` varchar(100) NOT NULL,
  `FILE_NAME_GAMBAR` text NOT NULL,
  PRIMARY KEY (`NO_ID_DOKUMEN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `1_3_dokumen` (`NO_ID_DOKUMEN`, `NO_ID_DETAIL_ORDER`, `JENIS_DOKUMEN_UPLOAD`, `FILE_NAME_GAMBAR`) VALUES
(0000000001,	0000000001,	'SERTIFIKAT',	'gallery/dokumen/1485482352-whatsapp_78680.png'),
(0000000002,	0000000001,	'SERTIFIKAT',	'gallery/dokumen/logo_-_Copy.jpg'),
(0000000003,	0000000011,	'KARTU KELUARGA',	'gallery/dokumen/bca.png');

DROP TABLE IF EXISTS `2_0_proses_order_customer`;
CREATE TABLE `2_0_proses_order_customer` (
  `NO_ID_PROSES_ORDER_CUSTOMER` int(15) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `NO_ID_OBYEK_ORDER_CUSTOMER` int(15) unsigned zerofill DEFAULT NULL,
  `NO_ID_JENISORDER` int(10) unsigned zerofill DEFAULT NULL,
  `NO_ID_PROSES_ORDER` int(10) unsigned zerofill DEFAULT NULL,
  `NAMA_PROSES_ORDER` varchar(250) DEFAULT NULL,
  `NOMOR_AKTA` int(25) DEFAULT NULL,
  `TGL_AKTA` date DEFAULT '1900-01-01',
  `JAM` varchar(10) DEFAULT NULL,
  `ID_USER` int(5) unsigned zerofill DEFAULT NULL,
  `NOMINAL_BIAYA_PROSES` int(10) DEFAULT NULL,
  `STATUS_PROSES_ORDER_CUSTOMER` varchar(50) DEFAULT NULL,
  `TGLSTTS_PROSES_ORDER_CUSTOMER` date DEFAULT NULL,
  PRIMARY KEY (`NO_ID_PROSES_ORDER_CUSTOMER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `2_0_proses_order_customer` (`NO_ID_PROSES_ORDER_CUSTOMER`, `NO_ID_OBYEK_ORDER_CUSTOMER`, `NO_ID_JENISORDER`, `NO_ID_PROSES_ORDER`, `NAMA_PROSES_ORDER`, `NOMOR_AKTA`, `TGL_AKTA`, `JAM`, `ID_USER`, `NOMINAL_BIAYA_PROSES`, `STATUS_PROSES_ORDER_CUSTOMER`, `TGLSTTS_PROSES_ORDER_CUSTOMER`) VALUES
(000000000000001,	000000000000002,	0000000001,	0000000001,	'Pembuatan Akta SKMHT',	0,	'0000-00-00',	'',	00005,	24234234,	NULL,	NULL),
(000000000000002,	000000000000002,	0000000001,	0000000002,	'Pembuatan Akta APHT',	0,	'0000-00-00',	'',	NULL,	234234,	NULL,	NULL),
(000000000000003,	000000000000002,	0000000001,	0000000003,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	234234,	NULL,	NULL),
(000000000000004,	000000000000002,	0000000001,	0000000004,	'Kepala Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	234234324,	NULL,	NULL),
(000000000000005,	000000000000002,	0000000001,	0000000005,	'Kepala Seksi Hubungan Hukum Pertanahan',	0,	'0000-00-00',	'',	NULL,	234234,	NULL,	NULL),
(000000000000006,	000000000000002,	0000000001,	0000000006,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	234234,	NULL,	NULL),
(000000000000007,	000000000000002,	0000000001,	0000000007,	'Loket Penyanan Penyerahan',	0,	'0000-00-00',	'',	NULL,	234234,	NULL,	NULL),
(000000000000008,	000000000000002,	0000000001,	0000000008,	'Pembayaran Biaya Akta dan Sertifikat HT ',	0,	'0000-00-00',	'',	NULL,	234234,	NULL,	NULL),
(000000000000009,	000000000000001,	0000000001,	0000000001,	'Pembuatan Akta SKMHT',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000010,	000000000000001,	0000000001,	0000000002,	'Pembuatan Akta APHT',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000011,	000000000000001,	0000000001,	0000000003,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000012,	000000000000001,	0000000001,	0000000004,	'Kepala Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000013,	000000000000001,	0000000001,	0000000005,	'Kepala Seksi Hubungan Hukum Pertanahan',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000014,	000000000000001,	0000000001,	0000000006,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000015,	000000000000001,	0000000001,	0000000007,	'Loket Penyanan Penyerahan',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000016,	000000000000001,	0000000001,	0000000008,	'Pembayaran Biaya Akta dan Sertifikat HT ',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000017,	000000000000002,	0000000001,	0000000001,	'Pembuatan Akta SKMHT',	0,	'0000-00-00',	'',	00002,	23424234,	NULL,	NULL),
(000000000000018,	000000000000002,	0000000001,	0000000002,	'Pembuatan Akta APHT',	0,	'0000-00-00',	'',	NULL,	100,	'Selesai',	'2019-12-11'),
(000000000000019,	000000000000002,	0000000001,	0000000003,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000020,	000000000000002,	0000000001,	0000000004,	'Kepala Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000021,	000000000000002,	0000000001,	0000000005,	'Kepala Seksi Hubungan Hukum Pertanahan',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000022,	000000000000002,	0000000001,	0000000006,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000023,	000000000000002,	0000000001,	0000000007,	'Loket Penyanan Penyerahan',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000024,	000000000000002,	0000000001,	0000000008,	'Pembayaran Biaya Akta dan Sertifikat HT ',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000025,	000000000000003,	0000000001,	0000000001,	'Pembuatan Akta SKMHT',	NULL,	NULL,	NULL,	NULL,	2,	NULL,	NULL),
(000000000000026,	000000000000003,	0000000001,	0000000002,	'Pembuatan Akta APHT',	NULL,	NULL,	NULL,	NULL,	2,	NULL,	NULL),
(000000000000027,	000000000000003,	0000000001,	0000000003,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	NULL,	NULL,	NULL,	NULL,	2,	NULL,	NULL),
(000000000000028,	000000000000003,	0000000001,	0000000004,	'Kepala Subseksi Pendaftaran Hak Tanah',	NULL,	NULL,	NULL,	NULL,	2222,	NULL,	NULL),
(000000000000029,	000000000000003,	0000000001,	0000000005,	'Kepala Seksi Hubungan Hukum Pertanahan',	NULL,	NULL,	NULL,	NULL,	22222,	NULL,	NULL),
(000000000000030,	000000000000003,	0000000001,	0000000006,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	NULL,	NULL,	NULL,	NULL,	2222,	NULL,	NULL),
(000000000000031,	000000000000003,	0000000001,	0000000007,	'Loket Penyanan Penyerahan',	NULL,	NULL,	NULL,	NULL,	222,	NULL,	NULL),
(000000000000032,	000000000000003,	0000000001,	0000000008,	'Pembayaran Biaya Akta dan Sertifikat HT ',	32323,	'2019-12-04',	'23:23',	NULL,	2000,	NULL,	NULL),
(000000000000033,	000000000000012,	0000000001,	0000000001,	'Pembuatan Akta SKMHT',	0,	'0000-00-00',	'',	NULL,	46000,	'Selesai',	'2019-12-14'),
(000000000000034,	000000000000012,	0000000001,	0000000002,	'Pembuatan Akta APHT',	0,	'0000-00-00',	'',	NULL,	650595,	NULL,	NULL),
(000000000000035,	000000000000012,	0000000001,	0000000003,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000036,	000000000000012,	0000000001,	0000000004,	'Kepala Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000037,	000000000000012,	0000000001,	0000000005,	'Kepala Seksi Hubungan Hukum Pertanahan',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000038,	000000000000012,	0000000001,	0000000006,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000039,	000000000000012,	0000000001,	0000000007,	'Loket Penyanan Penyerahan',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000040,	000000000000012,	0000000001,	0000000008,	'Pembayaran Biaya Akta dan Sertifikat HT ',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000041,	000000000000012,	0000000001,	0000000001,	'Pembuatan Akta SKMHT',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000042,	000000000000012,	0000000001,	0000000002,	'Pembuatan Akta APHT',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000043,	000000000000012,	0000000001,	0000000003,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000044,	000000000000012,	0000000001,	0000000004,	'Kepala Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000045,	000000000000012,	0000000001,	0000000005,	'Kepala Seksi Hubungan Hukum Pertanahan',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000046,	000000000000012,	0000000001,	0000000006,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000047,	000000000000012,	0000000001,	0000000007,	'Loket Penyanan Penyerahan',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000048,	000000000000012,	0000000001,	0000000008,	'Pembayaran Biaya Akta dan Sertifikat HT ',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000049,	000000000000011,	0000000001,	0000000001,	'Pembuatan Akta SKMHT',	0,	'0000-00-00',	'',	NULL,	500000,	'Dalam Proses',	'2019-12-28'),
(000000000000050,	000000000000011,	0000000001,	0000000002,	'Pembuatan Akta APHT',	0,	'0000-00-00',	'',	NULL,	7500,	'Selesai',	'2019-12-12'),
(000000000000051,	000000000000011,	0000000001,	0000000003,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000052,	000000000000011,	0000000001,	0000000004,	'Kepala Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000053,	000000000000011,	0000000001,	0000000005,	'Kepala Seksi Hubungan Hukum Pertanahan',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000054,	000000000000011,	0000000001,	0000000006,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000055,	000000000000011,	0000000001,	0000000007,	'Loket Penyanan Penyerahan',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000056,	000000000000011,	0000000001,	0000000008,	'Pembayaran Biaya Akta dan Sertifikat HT ',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000057,	000000000000011,	0000000001,	0000000001,	'Pembuatan Akta SKMHT',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000058,	000000000000011,	0000000001,	0000000002,	'Pembuatan Akta APHT',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000059,	000000000000011,	0000000001,	0000000003,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000060,	000000000000011,	0000000001,	0000000004,	'Kepala Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000061,	000000000000011,	0000000001,	0000000005,	'Kepala Seksi Hubungan Hukum Pertanahan',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000062,	000000000000011,	0000000001,	0000000006,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000063,	000000000000011,	0000000001,	0000000007,	'Loket Penyanan Penyerahan',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000064,	000000000000011,	0000000001,	0000000008,	'Pembayaran Biaya Akta dan Sertifikat HT ',	0,	'0000-00-00',	'',	NULL,	0,	NULL,	NULL),
(000000000000065,	000000000000014,	0000000002,	0000000009,	'Pembuatan Akta Jual Beli (AJB)',	12,	'2019-12-04',	'08:30',	00011,	2500000,	NULL,	NULL),
(000000000000066,	000000000000014,	0000000002,	0000000010,	'SPS PNBP',	NULL,	NULL,	NULL,	00010,	0,	NULL,	NULL),
(000000000000067,	000000000000014,	0000000002,	0000000011,	'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT',	NULL,	NULL,	NULL,	00010,	0,	NULL,	NULL),
(000000000000068,	000000000000014,	0000000002,	0000000012,	'Kepala Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT',	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL),
(000000000000069,	000000000000014,	0000000002,	0000000013,	'Kepala Seksi Hubungan Hukum Pertanahan',	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL),
(000000000000070,	000000000000014,	0000000002,	0000000014,	'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT',	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL),
(000000000000071,	000000000000014,	0000000002,	0000000015,	'Loket Pelayanan Penyerahan',	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL),
(000000000000072,	000000000000014,	0000000002,	0000000016,	'Pendaftaran',	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL),
(000000000000073,	000000000000014,	0000000002,	0000000017,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL),
(000000000000074,	000000000000014,	0000000002,	0000000018,	'Kepala Subseksi Pendaftaran Hak Tanah',	56,	'2019-12-25',	'00:00',	NULL,	0,	NULL,	NULL),
(000000000000075,	000000000000015,	0000000001,	0000000001,	'Pembuatan Akta SKMHT',	NULL,	NULL,	NULL,	00010,	0,	NULL,	NULL),
(000000000000076,	000000000000015,	0000000001,	0000000002,	'Pembuatan Akta APHT',	NULL,	NULL,	NULL,	00010,	0,	NULL,	NULL),
(000000000000077,	000000000000015,	0000000001,	0000000003,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	NULL,	NULL,	NULL,	00010,	0,	NULL,	NULL),
(000000000000078,	000000000000015,	0000000001,	0000000004,	'Kepala Subseksi Pendaftaran Hak Tanah',	NULL,	NULL,	NULL,	00010,	0,	'Dalam Proses',	'2019-12-03'),
(000000000000079,	000000000000015,	0000000001,	0000000005,	'Kepala Seksi Hubungan Hukum Pertanahan',	NULL,	NULL,	NULL,	00010,	0,	NULL,	NULL),
(000000000000080,	000000000000015,	0000000001,	0000000006,	'Pelaksana Subseksi Pendaftaran Hak Tanah',	NULL,	NULL,	NULL,	00010,	0,	NULL,	NULL),
(000000000000081,	000000000000015,	0000000001,	0000000007,	'Loket Penyanan Penyerahan',	NULL,	NULL,	NULL,	00010,	0,	NULL,	NULL),
(000000000000082,	000000000000015,	0000000001,	0000000008,	'Pembayaran Biaya Akta dan Sertifikat HT',	NULL,	NULL,	NULL,	00010,	0,	NULL,	NULL);

DROP TABLE IF EXISTS `member_menu`;
CREATE TABLE `member_menu` (
  `MEMBER_ID` int(11) DEFAULT NULL,
  `MENU_ID` tinyint(3) DEFAULT NULL,
  `NO_ID_MEMBER_MENU` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`NO_ID_MEMBER_MENU`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `member_menu` (`MEMBER_ID`, `MENU_ID`, `NO_ID_MEMBER_MENU`) VALUES
(1,	1,	0000000001),
(1,	2,	0000000002),
(1,	3,	0000000003),
(1,	4,	0000000004),
(1,	5,	0000000005),
(1,	6,	0000000006),
(1,	58,	0000000157),
(1,	8,	0000000008),
(1,	9,	0000000009),
(1,	10,	0000000010),
(1,	11,	0000000011),
(1,	12,	0000000012),
(1,	13,	0000000013),
(1,	14,	0000000014),
(1,	15,	0000000015),
(1,	16,	0000000016),
(1,	17,	0000000017),
(1,	18,	0000000018),
(1,	19,	0000000019),
(1,	20,	0000000020),
(1,	21,	0000000021),
(2,	59,	0000000159),
(2,	39,	0000000160),
(1,	56,	0000000156),
(2,	56,	0000000158),
(1,	26,	0000000026),
(1,	27,	0000000027),
(1,	28,	0000000028),
(1,	29,	0000000029),
(1,	30,	0000000030),
(1,	31,	0000000031),
(1,	32,	0000000032),
(1,	33,	0000000033),
(1,	34,	0000000034),
(16,	1,	0000000038),
(16,	2,	0000000039),
(16,	4,	0000000040),
(16,	5,	0000000041),
(16,	6,	0000000042),
(16,	7,	0000000043),
(16,	18,	0000000044),
(16,	19,	0000000045),
(16,	20,	0000000046),
(16,	21,	0000000047),
(16,	27,	0000000048),
(16,	28,	0000000049),
(16,	32,	0000000050),
(16,	33,	0000000051),
(16,	34,	0000000052),
(1,	40,	0000000155),
(1,	55,	0000000154),
(6,	55,	0000000153),
(6,	3,	0000000152),
(6,	5,	0000000057),
(1,	60,	0000000151),
(1,	59,	0000000150),
(6,	8,	0000000060),
(6,	9,	0000000061),
(6,	10,	0000000062),
(6,	11,	0000000063),
(6,	12,	0000000064),
(6,	13,	0000000065),
(6,	14,	0000000066),
(6,	15,	0000000067),
(6,	16,	0000000068),
(6,	17,	0000000069),
(6,	18,	0000000070),
(6,	19,	0000000071),
(6,	20,	0000000072),
(6,	21,	0000000073),
(7,	3,	0000000149),
(6,	53,	0000000148),
(6,	4,	0000000147),
(6,	1,	0000000146),
(6,	26,	0000000078),
(6,	27,	0000000079),
(6,	28,	0000000080),
(6,	29,	0000000081),
(6,	30,	0000000082),
(6,	31,	0000000083),
(6,	32,	0000000084),
(6,	33,	0000000085),
(6,	34,	0000000086),
(8,	2,	0000000110),
(8,	1,	0000000109),
(43,	3,	0000000108),
(43,	5,	0000000107),
(43,	2,	0000000106),
(43,	NULL,	0000000105),
(43,	15,	0000000104),
(43,	7,	0000000103),
(43,	6,	0000000102),
(43,	1,	0000000100),
(43,	28,	0000000101),
(8,	3,	0000000111),
(8,	29,	0000000123),
(8,	5,	0000000113),
(8,	6,	0000000114),
(8,	7,	0000000115),
(8,	8,	0000000116),
(8,	9,	0000000117),
(8,	10,	0000000118),
(8,	11,	0000000119),
(8,	12,	0000000120),
(8,	13,	0000000121),
(8,	14,	0000000122),
(8,	30,	0000000124),
(8,	31,	0000000125),
(8,	28,	0000000126),
(2,	1,	0000000127),
(2,	2,	0000000128),
(2,	6,	0000000129),
(2,	7,	0000000130),
(2,	3,	0000000131),
(2,	25,	0000000144),
(2,	17,	0000000133),
(2,	5,	0000000134),
(2,	28,	0000000135),
(4,	1,	0000000136),
(4,	4,	0000000141),
(4,	3,	0000000140),
(4,	2,	0000000139),
(1,	53,	0000000142),
(1,	39,	0000000143),
(2,	4,	0000000145),
(2,	58,	0000000161),
(2,	40,	0000000162),
(8,	56,	0000000163),
(9,	1,	0000000164),
(9,	3,	0000000165),
(9,	56,	0000000166),
(10,	1,	0000000167),
(10,	3,	0000000168),
(10,	55,	0000000169),
(11,	1,	0000000170),
(11,	3,	0000000171),
(11,	56,	0000000172),
(12,	1,	0000000173),
(12,	3,	0000000174),
(12,	55,	0000000175),
(13,	1,	0000000176),
(13,	3,	0000000177),
(13,	56,	0000000178),
(11,	55,	0000000179);

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL DEFAULT '',
  `menu_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `menu` (`id`, `parent_id`, `title`, `url`, `menu_order`, `icon`) VALUES
(1,	0,	'HOME',	'Home',	1,	'fa-home'),
(3,	0,	'ORDER',	'Customer',	2,	'fa-cart-plus'),
(55,	3,	'Proses',	'ProsesPengikatan',	2,	'fa-american-sign-language-interpreting'),
(40,	39,	'USER',	'Menu_user',	1,	'fa-user'),
(39,	0,	'ADMINISTRATOR',	'#',	4,	'fa-database'),
(59,	0,	'LAPORAN',	'#',	3,	'fa-print'),
(56,	3,	'Order',	'Customer',	1,	'fa-cart-plus'),
(60,	0,	'MENU INDUK',	'Menu_utama',	5,	'fa-list'),
(58,	39,	'DOKUMENTASI PENGIKATAN',	'Jenis_order',	2,	'fa-star');

-- 2020-04-07 08:56:45
