-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 25 Des 2019 pada 01.14
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `i_norsys`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `1_3_dokumen`
--

CREATE TABLE `1_3_dokumen` (
  `NO_ID_DOKUMEN` int(10) UNSIGNED ZEROFILL NOT NULL,
  `NO_ID_DETAIL_ORDER` int(10) UNSIGNED ZEROFILL NOT NULL,
  `JENIS_DOKUMEN_UPLOAD` varchar(100) NOT NULL,
  `FILE_NAME_GAMBAR` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `1_3_dokumen`
--

INSERT INTO `1_3_dokumen` (`NO_ID_DOKUMEN`, `NO_ID_DETAIL_ORDER`, `JENIS_DOKUMEN_UPLOAD`, `FILE_NAME_GAMBAR`) VALUES
(0000000001, 0000000001, 'SERTIFIKAT', 'gallery/dokumen/1485482352-whatsapp_78680.png'),
(0000000002, 0000000001, 'SERTIFIKAT', 'gallery/dokumen/logo_-_Copy.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `1_3_dokumen`
--
ALTER TABLE `1_3_dokumen`
  ADD PRIMARY KEY (`NO_ID_DOKUMEN`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `1_3_dokumen`
--
ALTER TABLE `1_3_dokumen`
  MODIFY `NO_ID_DOKUMEN` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
