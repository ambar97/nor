-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 11, 2020 at 08:06 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nor_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `0_0_jenis_order`
--

CREATE TABLE `0_0_jenis_order` (
  `NO_ID_JENISORDER` int(10) NOT NULL,
  `NAMA_JENIS_ORDER` varchar(150) DEFAULT NULL,
  `OBYEK_JENIS_ORDER` varchar(150) DEFAULT NULL,
  `ASAL_OBYEK_JENIS_ORDER` varchar(150) DEFAULT NULL,
  `KETERANGAN_JENIS_ORDER` longtext DEFAULT NULL,
  `JENIS_CUSTOMER` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_0_jenis_order`
--

INSERT INTO `0_0_jenis_order` (`NO_ID_JENISORDER`, `NAMA_JENIS_ORDER`, `OBYEK_JENIS_ORDER`, `ASAL_OBYEK_JENIS_ORDER`, `KETERANGAN_JENIS_ORDER`, `JENIS_CUSTOMER`) VALUES
(1, 'HAK TANGGUNGAN (HT)', 'SHM', 'SHM', 'Order tanpa perlu melakukan perubahan status terhadap Obyek Tanggungan', 'BANK'),
(2, 'HAK TANGGUNGAN (HT)', 'SHM', 'SHM', 'Order untuk dilakukan perubahan terhadap status kepemilikan Obyek Hak Tanggungan, Obyek harus dibalik nama terlebih dahulu', 'BANK'),
(3, 'HAK TANGGUNGAN (HT)', 'SHM', 'AJB/Letter C', 'Order untuk peningkatan obyek yang masih berupa obyek yang tidak bisa diikat dengan SKMHT ataupun Hak Tanggungan, sehingga perlu dilakukan proses balik nama ataupun proses peningkatan status obyek tersebut', 'BANK'),
(4, 'SURAT KUASA MEMBEBANKAN HAK TANGGUNGAN (SKMHT)', 'SHM', 'SHM', 'Pengikatan SKMHT untuk obyek tanpa adanya perubahan status ataupun balik nama', 'BANK'),
(5, 'SURAT KUASA MEMBEBANKAN HAK TANGGUNGAN (SKMHT)', 'SHM', 'SHM', 'Order untuk dilakukan perubahan terhadap status kepemilikan Obyek Hak Tanggungan, Obyek harus dibalik nama terlebih dahulu', 'BANK'),
(6, 'SURAT KUASA MEMBEBANKAN HAK TANGGUNGAN (SKMHT)', 'SHM', 'AJB/Letter C', 'Order untuk peningkatan obyek yang masih berupa obyek yang tidak bisa diikat dengan SKMHT ataupun Hak Tanggungan, sehingga perlu dilakukan proses balik nama ataupun proses peningkatan status obyek tersebut', 'BANK'),
(7, 'Akta Jual Beli', 'SHM', 'SHM', 'Proses Jual Beli Dari SHM ke SHM', 'PERORANGAN');

-- --------------------------------------------------------

--
-- Table structure for table `0_0_proses_order`
--

CREATE TABLE `0_0_proses_order` (
  `NO_ID_JENIS_ORDER` int(10) NOT NULL,
  `NO_ID_PROSES_ORDER` int(15) NOT NULL,
  `NAMA_PROSES_ORDER` varchar(250) DEFAULT NULL,
  `POST_PROSES_ORDER` varchar(250) DEFAULT NULL,
  `KETERANGAN_PROSES_ORDER` longtext DEFAULT NULL,
  `OUTPUT` tinyint(1) DEFAULT NULL COMMENT '1=pencetak, 2=bpn'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_0_proses_order`
--

INSERT INTO `0_0_proses_order` (`NO_ID_JENIS_ORDER`, `NO_ID_PROSES_ORDER`, `NAMA_PROSES_ORDER`, `POST_PROSES_ORDER`, `KETERANGAN_PROSES_ORDER`, `OUTPUT`) VALUES
(1, 1, 'Pembuatan Akta SKMHT', 'Pembuat Akta', 'Pembuatan untuk Akta SKMHT yang akan dimintakan tanda tangan kepada Pemimpin Customer', 1),
(1, 2, 'Pembuatan Akta APHT', 'Pembuat Akta', 'Proses pembuatan Akta APHT di Kantor Notaris', 1),
(1, 3, 'Pelaksana Subseksi Pendaftaran Hak Tanah', 'BPN', 'Proses ini berada di BPN', 2),
(1, 4, 'Kepala Subseksi Pendaftaran Hak Tanah', 'Kepala Subseksi Pendaftaran Hak Tanah BPN', 'Proses ini berada di BPN', 2),
(1, 5, 'Kepala Seksi Hubungan Hukum Pertanahan', 'Kepala Seksi Hubungan Hukum Pertanahan BPN', 'Proses ini berada di BPN', 2),
(1, 6, 'Pelaksana Subseksi Pendaftaran Hak Tanah', 'Pelaksana Subseksi Pendaftaran Hak Tanah BPN', 'Proses ini berada di BPN dengan seksi yang berbeda', 2),
(1, 7, 'Loket Penyanan Penyerahan', 'Loket Penyanan Penyerahan BPN', 'Penyerahan Akta yang sudah selesai', 2),
(1, 8, 'Pembayaran Biaya Akta dan Sertifikat HT', 'Pembayaran seluruh biaya di Loket BPN', 'Pembayaran biaya dan penyerahan Akta Hak Tanggungan dan Sertifikat Hak Tanggungan', 2),
(2, 9, 'Pembuatan Akta Jual Beli (AJB)', 'Proses berada di kantor Notaris', 'Proses pembuatan Akta Jual Beli (AJB) antara penjual atau pembeli untuk selanjutnya dilanjutkan dengan proses SKMHT dan APHT', 1),
(2, 10, 'SPS PNBP', 'SPS PNBP', 'SPS PNBP', 2),
(2, 11, 'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT', 'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT (BPN)', 'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT', 2),
(2, 12, 'Kepala Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT', 'Kepala Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT (BPN)', 'Kepala Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT', 2),
(2, 13, 'Kepala Seksi Hubungan Hukum Pertanahan', 'Kepala Seksi Hubungan Hukum Pertanahan (BPN)', 'Kepala Seksi Hubungan Hukum Pertanahan\r\n\r\n\r\n', 2),
(2, 14, 'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT', 'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT (BPN)', 'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT', 2),
(2, 15, 'Loket Pelayanan Penyerahan', 'Loket Pelayanan Penyerahan (BPN)', 'Loket Pelayanan Penyerahan', 2),
(2, 16, 'Pendaftaran', 'Pendaftaran di BPN', 'Pendaftaran seluruh formulir Pengikatan di BPN', 2),
(2, 17, 'Pelaksana Subseksi Pendaftaran Hak Tanah', 'Pelaksana Subseksi Pendaftaran Hak Tanah BPN', 'Pelaksana Subseksi Pendaftaran Hak Tanah', 2),
(2, 18, 'Kepala Subseksi Pendaftaran Hak Tanah', 'Kepala Subseksi Pendaftaran Hak Tanah BPN', 'Kepala Subseksi Pendaftaran Hak Tanah BPN', 2),
(2, 19, 'Kepala Seksi Hubungan Hukum Pertanahan', 'Kepala Seksi Hubungan Hukum Pertanahan BPN', 'Kepala Seksi Hubungan Hukum Pertanahan', 2);

-- --------------------------------------------------------

--
-- Table structure for table `0_1_data_customer`
--

CREATE TABLE `0_1_data_customer` (
  `NO_ID_CUSTOMER` int(10) NOT NULL,
  `NAMA_CUSTOMER` varchar(250) DEFAULT NULL,
  `JENIS_CUSTOMER` varchar(50) DEFAULT NULL,
  `ALAMAT_CUSTOMER` varchar(255) DEFAULT NULL,
  `KABUPATEN_CUSTOMER` varchar(100) DEFAULT NULL,
  `KECAMATAN_CUSTOMER` varchar(100) DEFAULT NULL,
  `DESA_KELURAHAN_CUSTOMER` varchar(100) DEFAULT NULL,
  `RT_CUSTOMER` varchar(5) DEFAULT NULL,
  `RW_CUSTOMER` varchar(5) DEFAULT NULL,
  `NOMOR_TELEPHONE_CUSTOMER` varchar(15) DEFAULT NULL,
  `PIC_CUSTOMER` varchar(250) DEFAULT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_1_data_customer`
--

INSERT INTO `0_1_data_customer` (`NO_ID_CUSTOMER`, `NAMA_CUSTOMER`, `JENIS_CUSTOMER`, `ALAMAT_CUSTOMER`, `KABUPATEN_CUSTOMER`, `KECAMATAN_CUSTOMER`, `DESA_KELURAHAN_CUSTOMER`, `RT_CUSTOMER`, `RW_CUSTOMER`, `NOMOR_TELEPHONE_CUSTOMER`, `PIC_CUSTOMER`, `id_user`) VALUES
(1, 'Joko Smith', 'PERORANGAN', 'Jl. Mayjend Sutoyo No.10, Patokan, Kec. Kraksaan, Probolinggo, Jawa Timur 67282', 'Jember', 'Sumbersari', 'Kartoharjo/Taman', '003', '002', '0895354573776', 'Joko Smith', 11),
(2, 'BANK Mandiri', 'BANK', 'Jl. Mayjend Sutoyo No.10, Patokan, Kec. Kraksaan, Probolinggo, Jawa Timur 67282SS', 'Madiun', 'Taman', 'Kartoharjo', '002', '003', '0895354573776', 'Stephen Hawking', 0),
(4, 'BANK BTN', 'PERORANGAN', 'Jalan Brawijaya Ruko Brawijaya B1-B2 ', 'Banyuwangi', 'Banyuwangi', 'Kebalenan', '001', '003', '081234661699', 'DIMAS', 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_1_detail_customer`
--

CREATE TABLE `0_1_detail_customer` (
  `NO_ID_DETAIL_CUSTOMER` int(15) NOT NULL,
  `NO_ID_CUSTOMER` int(15) DEFAULT NULL,
  `NAMA_COMPARATOR_CUSTOMER` varchar(250) DEFAULT NULL,
  `NO_KTP_COMPARATOR_CUSTOMER` varchar(50) NOT NULL,
  `ALAMAT_COMPARATOR_CUSTOMER` longtext DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(50) NOT NULL,
  `TANGGAL_LAHIR` date NOT NULL,
  `KABUPATEN_COMPARATOR_CUSTOMER` varchar(250) DEFAULT NULL,
  `KECAMATAN_COMPARATOR_CUSTOMER` varchar(250) DEFAULT NULL,
  `DESA_KELURAHAN_COMPARATOR_CUSTOMER` varchar(250) DEFAULT NULL,
  `RT_COMPARATOR_CUSTOMER` varchar(50) DEFAULT NULL,
  `RW_COMPARATOR_CUSTOMER` varchar(50) DEFAULT NULL,
  `ISI_COMPARATOR_CUSTOMER` longtext DEFAULT NULL,
  `KOMPARISI_PUSAT` longtext NOT NULL,
  `STATUS_COMPARATOR` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `0_2_data_saksi`
--

CREATE TABLE `0_2_data_saksi` (
  `NO_ID_SAKSI` int(10) NOT NULL,
  `NAMA_SAKSI` varchar(100) NOT NULL,
  `ISI_BIODATA` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_2_data_saksi`
--

INSERT INTO `0_2_data_saksi` (`NO_ID_SAKSI`, `NAMA_SAKSI`, `ISI_BIODATA`) VALUES
(1, 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001'),
(2, 'AMALINDA PRATIWI', 'lahir di Banyuwangi, tanggal 21-01-2000 (dua puluh satu Januari tahun dua ribu), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Jalan Batang Hari, Rukun Tetangga 002, Rukun Warga 005, Kelurahan Penataban, Kecamatan Giri, Kabupaten Banyuwangi. Pemilik Kartu Tanda Penduduk Nomor 3510176101000001');

-- --------------------------------------------------------

--
-- Table structure for table `0_3_user_cust`
--

CREATE TABLE `0_3_user_cust` (
  `NO_ID_USR` int(12) NOT NULL,
  `NO_ID_CUST` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_3_user_cust`
--

INSERT INTO `0_3_user_cust` (`NO_ID_USR`, `NO_ID_CUST`) VALUES
(12, 1),
(12, 2),
(14, 6),
(14, 7),
(4, 1),
(4, 2),
(4, 4),
(13, 5),
(13, 6);

-- --------------------------------------------------------

--
-- Table structure for table `0_user`
--

CREATE TABLE `0_user` (
  `ID_USER` int(5) NOT NULL,
  `NAMA_USER` varchar(255) DEFAULT NULL,
  `LOGIN_USER` varchar(255) DEFAULT NULL,
  `PSWD_USER` varchar(255) DEFAULT NULL,
  `STATUS_LOGIN` int(11) NOT NULL DEFAULT 0,
  `ADDRS_USER` varchar(255) DEFAULT NULL,
  `BSCSLR_USER` int(11) DEFAULT NULL,
  `PHONE_USER` varchar(255) DEFAULT NULL,
  `PICT_USER` varchar(255) DEFAULT NULL,
  `STATUS_USER` varchar(255) DEFAULT NULL,
  `TYPE_USER` tinyint(4) NOT NULL COMMENT '1=jskit,  3=inputer 4=super_admin , 5=BPN',
  `id_insert` int(5) UNSIGNED ZEROFILL NOT NULL,
  `tempat_lahir` varchar(99) NOT NULL,
  `tanggal_lahir` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_user`
--

INSERT INTO `0_user` (`ID_USER`, `NAMA_USER`, `LOGIN_USER`, `PSWD_USER`, `STATUS_LOGIN`, `ADDRS_USER`, `BSCSLR_USER`, `PHONE_USER`, `PICT_USER`, `STATUS_USER`, `TYPE_USER`, `id_insert`, `tempat_lahir`, `tanggal_lahir`) VALUES
(1, 'SUPER ADMIN1', 'ADMIN1', 'e00cf25ad42683b3df678c61f42c6bda', 1, '', 0, '', 'foto_00001.jpeg', 'AKTIF', 1, 00001, '', '0000-00-00'),
(2, 'NUR AINI MAULIDA', 'NURAINI02', '359a5096b794d883f7faa67334f82ff9', 0, 'Jl. Brawijaya No. 18 Banyuwangi', NULL, '081232633100', 'foto_2.png', 'AKTIF', 4, 00001, '', '0000-00-00'),
(4, 'Coba', 'Coba', '5ebe2294ecd0e0f08eab7690d2a6ee69', 0, 'Jl. Batanghari Gg Kektanu No. 40 Penataban', 0, '081259201874', 'linda01.jpeg', 'AKTIF', 2, 00002, 'asadad', '2020-08-06'),
(11, 'INDAH RATNASARI', 'cobai', '5ebe2294ecd0e0f08eab7690d2a6ee69', 0, 'Desa Olehsari RT 004 RW 002 Kecamatan Glagah, Kabupaten Banyuwangi', 0, '082245143469', NULL, 'AKTIF', 3, 00002, '', '0000-00-00'),
(13, 'NADIA UMARO', 'NADIAp', 'aa2d6e4f578eb0cfaba23beef76c2194', 0, 'Jl. RW Monginsidi RT 002 RW 003 Kelurahan Pengajuran, Kecamatan Banyuwangi, Kabupaten Banyuwangi', 0, '082332820336', NULL, 'AKTIF', 2, 00002, '', '0000-00-00'),
(15, 'VINA ISTIAWATI AGUSTIN', 'VINAb', '5ebe2294ecd0e0f08eab7690d2a6ee69', 1, 'Jl. Karimunjawa No. 23 Kelurahan Lateng, Kecamatan Banyuwangi', 0, '08970660615', 'foto_15.jpg', 'AKTIF', 5, 00002, '', '0000-00-00'),
(17, 'LOGIN KILLER', 'SUPERADMIN', '17c4520f6cfd1ab53d8745e84681eb49', 0, NULL, NULL, NULL, NULL, 'AKTIF', 1, 00001, '', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `1_0_data_order_customer`
--

CREATE TABLE `1_0_data_order_customer` (
  `NO_ID_ORDER_CUSTOMER` int(15) NOT NULL,
  `NO_ID_CUSTOMER` int(10) DEFAULT NULL,
  `NOMOR_SURAT_ORDER_CUSTOMER` varchar(250) DEFAULT NULL,
  `TANGGAL_SURAT_ORDER_CUSTOMER` date DEFAULT NULL,
  `NOMOR_LAIN_ORDER_CUSTOMER` varchar(250) DEFAULT NULL,
  `TANGGAL_NOMOR_LAIN_ORDER_CUSTOMER` date DEFAULT NULL,
  `KETERANGAN_ORDER_CUSTOMER` longtext DEFAULT NULL,
  `NOMINAL_KREDIT` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `1_0_data_order_customer`
--

INSERT INTO `1_0_data_order_customer` (`NO_ID_ORDER_CUSTOMER`, `NO_ID_CUSTOMER`, `NOMOR_SURAT_ORDER_CUSTOMER`, `TANGGAL_SURAT_ORDER_CUSTOMER`, `NOMOR_LAIN_ORDER_CUSTOMER`, `TANGGAL_NOMOR_LAIN_ORDER_CUSTOMER`, `KETERANGAN_ORDER_CUSTOMER`, `NOMINAL_KREDIT`) VALUES
(1, 1, 'BWI/6.1/257', '2020-02-01', 'NOR/276/XI/2019', '2020-02-01', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tincidunt, ligula eu viverra feugiat, tellus tortor faucibus arcu, eu accumsan sem ligula et mauris. Morbi fermentum ornare consequat. Curabitur a risus bibendum, volutpat nunc sit amet, luctus lacus. Donec fermentum accumsan libero, a dignissim tortor. Fusce nunc urna, mollis vel leo eu, dapibus gravida lacus. Nullam auctor purus libero, nec aliquam sapien semper quis. Vivamus imperdiet mattis egestas. ', 0),
(2, 2, 'BWI/6.1/257	', '2020-03-03', '', '2020-03-03', '', 0),
(3, 3, 'BWI/6.1/25333', '2020-04-10', '1232223333', '2020-04-10', 'Lorem ipsum color do sit amet Lorem ipsum color do sit amet Lorem ipsum color do sit amet Lorem ipsum color do sit amet Lorem ipsum color do sit amet Lorem ipsum color do sit amet Lorem ipsum color do sit amet Lorem ipsum color do sit amet Lorem ipsum color do sit amet Lorem ipsum color do sit amet Lorem ipsum color do sit amet Lorem ipsum color do sit amet Lorem ipsum color do sit amet Lorem ipsum color do sit amet ', 0),
(4, 5, 'BWI/2.1/00785/R', '2020-04-20', '', '0000-00-00', 'Order HT I SHM No. 256 an. ', 0),
(5, 4, 'e0050/00224/SP3K/XI/2019', '2019-11-27', '', '0000-00-00', 'AJB SHM 2578/GAMBIRAN KE YENI PALUPI INDRASARI \r\nSKMHT\r\nPENGAKUAN HUTANG Rp. 58.000.000,-\r\nKUASA MENJUAL\r\nHAK TANGGUNGAN Rp. 87.000.000,-', 0),
(6, 4, 'e0016/00224/sp3k/II/2020', '2020-10-02', '', '0000-00-00', '', 0),
(7, 6, 'juhhy', '2020-12-01', '123fhfj', '2020-01-01', 'none', 0),
(8, 7, 'gfgf25', '2020-06-08', 'ggvgh245', '2020-06-04', 'membalik nama rumah', 0);

-- --------------------------------------------------------

--
-- Table structure for table `1_1_detail_order_customer`
--

CREATE TABLE `1_1_detail_order_customer` (
  `NO_ID_DETAIL_ORDER` int(15) NOT NULL,
  `NO_ID_ORDER_CUSTOMER` int(15) DEFAULT NULL,
  `NO_ID_JENISORDER` int(10) DEFAULT NULL,
  `DETAIL_ORDER_ATAS_NAMA` varchar(250) DEFAULT NULL,
  `PASANGAN_DETAIL_ORDER` varchar(250) NOT NULL,
  `BIODATA_PASANGAN_DETAIL_ORDER` text NOT NULL,
  `SAKSI_SATU` varchar(250) NOT NULL,
  `BIODATA_SAKSI_SATU` text NOT NULL,
  `SAKSI_DUA` varchar(250) NOT NULL,
  `BIODATA_SAKSI_DUA` text NOT NULL,
  `NOMINAL_DETAIL_ORDER` int(15) DEFAULT NULL,
  `KETERANGAN_DETAIL_ORDER` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `1_1_detail_order_customer`
--

INSERT INTO `1_1_detail_order_customer` (`NO_ID_DETAIL_ORDER`, `NO_ID_ORDER_CUSTOMER`, `NO_ID_JENISORDER`, `DETAIL_ORDER_ATAS_NAMA`, `PASANGAN_DETAIL_ORDER`, `BIODATA_PASANGAN_DETAIL_ORDER`, `SAKSI_SATU`, `BIODATA_SAKSI_SATU`, `SAKSI_DUA`, `BIODATA_SAKSI_DUA`, `NOMINAL_DETAIL_ORDER`, `KETERANGAN_DETAIL_ORDER`) VALUES
(1, 1, 1, 'Joko Smith', 'Bu Joko Smith', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tincidunt, ligula eu viverra feugiat, tellus tortor faucibus arcu, eu accumsan sem ligula et mauris. Morbi fermentum ornare consequat. Curabitur a risus bibendum, volutpat nunc sit amet, luctus lacus. Donec fermentum accumsan libero, a dignissim tortor. Fusce nunc urna, mollis vel leo eu, dapibus gravida lacus. Nullam auctor purus libero, nec aliquam sapien semper quis. Vivamus imperdiet mattis egestas. ', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 3000000, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tincidunt, ligula eu viverra feugiat, tellus tortor faucibus arcu, eu accumsan sem ligula et mauris. Morbi fermentum ornare consequat. Curabitur a risus bibendum, volutpat nunc sit amet, luctus lacus. Donec fermentum accumsan libero, a dignissim tortor. Fusce nunc urna, mollis vel leo eu, dapibus gravida lacus. Nullam auctor purus libero, nec aliquam sapien semper quis. Vivamus imperdiet mattis egestas. '),
(3, 2, 1, 'Stephen Hawking', '', '', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 2000000, ''),
(4, 3, 1, 'Christopher', 'Victoria', 'Jember Perum Mojopahit Kaliwates', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 900000, 'Lunas'),
(5, 5, 2, 'YENI PALUPI INDRASARI, S.Pd', 'MATALI S.Pd.M.M', '', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 'AMALINDA PRATIWI', 'lahir di Banyuwangi, tanggal 21-01-2000 (dua puluh satu Januari tahun dua ribu), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Jalan Batang Hari, Rukun Tetangga 002, Rukun Warga 005, Kelurahan Penataban, Kecamatan Giri, Kabupaten Banyuwangi. Pemilik Kartu Tanda Penduduk Nomor 3510176101000001', 87000000, 'Hak Tanggungan Peringkat Pertama (I)'),
(6, 6, 6, 'AINUR ROFIK', 'AYU NURHAYATI', 'Lahir di Singaraja, tanggal 04 Mei 1970, Mengurus Rumah Tangga, bertempat tinggal sama dengan suaminya tersebut diatas, Pemegang Nomor Kartu Tanda Penduduk 5201084405700001.', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 'AMALINDA PRATIWI', 'lahir di Banyuwangi, tanggal 21-01-2000 (dua puluh satu Januari tahun dua ribu), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Jalan Batang Hari, Rukun Tetangga 002, Rukun Warga 005, Kelurahan Penataban, Kecamatan Giri, Kabupaten Banyuwangi. Pemilik Kartu Tanda Penduduk Nomor 3510176101000001', 0, '81000000'),
(7, 7, 1, 'debitur', 'pasangan debitur', 'sasa', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 'AMALINDA PRATIWI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 300, 'pengikatan atas 2 sertifikat'),
(8, 7, 7, 'gyuhio', 'uio', 'uio', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 7890, 'ghjkl'),
(9, 8, 7, 'alifiah', 'anas ', 'gg', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 12121, '121212'),
(10, 9, 1, 'errty', 'rty', 'ty', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 1212, '323');

-- --------------------------------------------------------

--
-- Table structure for table `1_2_detail_obyek_order_customer`
--

CREATE TABLE `1_2_detail_obyek_order_customer` (
  `NO_ID_OBYEK_ORDER_CUSTOMER` int(15) NOT NULL,
  `NO_ID_DETAIL_ORDER` int(15) DEFAULT NULL,
  `JENIS_OBYEK` varchar(250) DEFAULT NULL,
  `NO_ID_JENISORDER` int(15) NOT NULL,
  `NOMOR_OBYEK` varchar(250) DEFAULT NULL,
  `TANGGAL_OBYEK` date DEFAULT NULL,
  `JENIS_IDENTIFIKASI_OBYEK` varchar(250) DEFAULT NULL,
  `NOMOR_IDENTIFIKASI_OBYEK` varchar(250) DEFAULT NULL,
  `TANGGAL_IDENTIFIKASI_OBYEK` date DEFAULT NULL,
  `ATAS_NAMA_OBYEK` varchar(250) DEFAULT NULL,
  `ALAMAT_NAMA_OBYEK` text NOT NULL,
  `NIK_OBYEK` varchar(50) NOT NULL,
  `ATAS_NAMA_OBYEK_BARU` varchar(100) NOT NULL,
  `ALAMAT_NAMA_OBYEK_BARU` text NOT NULL,
  `NIK_OBYEK_BARU` varchar(50) NOT NULL,
  `ALAMAT_OBYEK` text NOT NULL,
  `LUAS_OBYEK` int(10) NOT NULL,
  `NOMOR_OBYEK_PAJAK` varchar(250) NOT NULL,
  `NJOP` int(15) NOT NULL,
  `HARGA_TRANSAKSI` int(15) NOT NULL,
  `NTPN` varchar(250) NOT NULL,
  `SSP_TANGGAL` date NOT NULL,
  `SSP_NOMINAL` int(15) NOT NULL,
  `SSB_TANGGAL` date NOT NULL,
  `SSB_NOMINAL` int(15) NOT NULL,
  `KETERANGAN_LAIN_OBYEK` longtext DEFAULT NULL,
  `NO_AJB` varchar(250) DEFAULT NULL,
  `TANGGAL_AJB` date DEFAULT NULL,
  `NOMINAL_PARTIAL_OBYEK` int(15) NOT NULL,
  `URAIAN_OBYEK` text NOT NULL,
  `NAMA_PPAT` varchar(250) NOT NULL,
  `LOKASI_PPAT` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `1_2_detail_obyek_order_customer`
--

INSERT INTO `1_2_detail_obyek_order_customer` (`NO_ID_OBYEK_ORDER_CUSTOMER`, `NO_ID_DETAIL_ORDER`, `JENIS_OBYEK`, `NO_ID_JENISORDER`, `NOMOR_OBYEK`, `TANGGAL_OBYEK`, `JENIS_IDENTIFIKASI_OBYEK`, `NOMOR_IDENTIFIKASI_OBYEK`, `TANGGAL_IDENTIFIKASI_OBYEK`, `ATAS_NAMA_OBYEK`, `ALAMAT_NAMA_OBYEK`, `NIK_OBYEK`, `ATAS_NAMA_OBYEK_BARU`, `ALAMAT_NAMA_OBYEK_BARU`, `NIK_OBYEK_BARU`, `ALAMAT_OBYEK`, `LUAS_OBYEK`, `NOMOR_OBYEK_PAJAK`, `NJOP`, `HARGA_TRANSAKSI`, `NTPN`, `SSP_TANGGAL`, `SSP_NOMINAL`, `SSB_TANGGAL`, `SSB_NOMINAL`, `KETERANGAN_LAIN_OBYEK`, `NO_AJB`, `TANGGAL_AJB`, `NOMINAL_PARTIAL_OBYEK`, `URAIAN_OBYEK`, `NAMA_PPAT`, `LOKASI_PPAT`) VALUES
(2, 1, 'SHM', 1, 'Joko', '2020-02-01', 'Jenis', 'Nomor', '2020-02-01', 'Joko', '', '', '', '', '', '', 300, '111', 0, 0, '', '0000-00-00', 0, '0000-00-00', 0, ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tincidunt, ligula eu viverra feugiat, tellus tortor faucibus arcu, eu accumsan sem ligula et mauris. Morbi fermentum ornare consequat. Curabitur a risus bibendum, volutpat nunc sit amet, luctus lacus. Donec fermentum accumsan libero, a dignissim tortor. Fusce nunc urna, mollis vel leo eu, dapibus gravida lacus. Nullam auctor purus libero, nec aliquam sapien semper quis. Vivamus imperdiet mattis egestas.', NULL, NULL, 1000000, '', '', ''),
(3, 2, 'AJB/LETTER C/PETHOK', 2, '251/LTRC/2020', '2020-02-03', '-', '-', '0000-00-00', 'Abiem', '', '', '', '', '', '', 300, '351000999', 0, 0, '', '0000-00-00', 0, '0000-00-00', 0, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id cursus mi. Donec viverra orci ac odio imperdiet auctor. Mauris et ligula elementum, tempus diam et, tempor ligula. Aenean ac porttitor nulla, id iaculis sapien. Quisque interdum justo eget blandit mattis. Donec quis pellentesque purus. Nullam id sollicitudin lorem, blandit mollis dolor. Nulla in finibus nibh. Donec eu tellus turpis. Curabitur sit amet est quis dui euismod tempus. In hac habitasse platea dictumst. ', NULL, NULL, 3000000, '', '', ''),
(4, 3, 'SHM', 1, 'BWI/6.1/222', '2020-03-03', '', '', '2020-03-03', 'Stephen Hawking', '', '', '', '', '', '', 600, 'BWI/6.1/111', 0, 0, '', '0000-00-00', 0, '0000-00-00', 0, '', NULL, NULL, 0, '', '', ''),
(5, 3, 'SHM', 1, '123', '2020-03-09', 'asd', '123', '2020-03-09', 'asd', '', '', '', '', '', '', 123, '123', 0, 0, '', '0000-00-00', 0, '0000-00-00', 0, 'asd', NULL, NULL, 123, '', '', ''),
(6, 4, 'SHM', 1, '11223344', '2020-04-10', 'Jenis Identifikasi Obyek', '1122334455', '2020-04-10', 'Christopher', '', '', '', '', '', '', 900, '99887766', 0, 0, '', '0000-00-00', 0, '0000-00-00', 0, 'Keterangan Lain Lain', NULL, NULL, 9000000, '', '', ''),
(7, 5, 'SHM', 2, '2578', '2013-07-26', 'SURAT UKUR', '00084/Gambiran/2013', '2013-06-11', '1. Hj. DWENTI HARDIANI 2. NOVI FERAWATI  3. ANDIKA DWI PRAYOGA', '', '', '', '', '', '', 168, '35.10.070.007.009-0188.0', 0, 0, '', '0000-00-00', 0, '0000-00-00', 0, 'Nomor Identifikasi Bidang (NIB) 12.37.07.07.02901', NULL, NULL, 87000, '', '', ''),
(8, 7, 'SHM', 1, '5678', '2020-12-31', 'qwqw', '35yd', '2006-12-31', 'nama sertifikat', '', '', '', '', '', '', 200, 'NOP Sertifikat', 0, 0, '', '0000-00-00', 0, '0000-00-00', 0, '', NULL, NULL, 0, '', '', ''),
(9, 9, 'SHGB', 7, '121', '2020-06-08', 'orang', '1212', '2020-06-07', 'tanah', '', '', '', '', '', '', 200, '12', 0, 0, '', '0000-00-00', 0, '0000-00-00', 0, '', NULL, NULL, 12, '', '', ''),
(10, 10, 'SHGU', 1, 'rt', '2019-12-31', 'rtr', '54545', '2020-12-31', '5t6', '', '', '', '', '', '', 554545, '865678', 0, 0, '', '0000-00-00', 0, '0000-00-00', 0, 'gyhjk', NULL, NULL, 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `1_3_dokumen`
--

CREATE TABLE `1_3_dokumen` (
  `NO_ID_DOKUMEN` int(10) NOT NULL,
  `NO_ID_DETAIL_ORDER` int(10) NOT NULL,
  `JENIS_DOKUMEN_UPLOAD` varchar(100) NOT NULL,
  `FILE_NAME_GAMBAR` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `1_3_dokumen`
--

INSERT INTO `1_3_dokumen` (`NO_ID_DOKUMEN`, `NO_ID_DETAIL_ORDER`, `JENIS_DOKUMEN_UPLOAD`, `FILE_NAME_GAMBAR`) VALUES
(1, 5, 'KTP', 'gallery/dokumen/KTP.jpg'),
(2, 3, 'KTP', 'gallery/dokumen/Screenshot_from_2020-02-23_10-14-37.png'),
(3, 5, 'KTP', 'gallery/dokumen/ktp_suami.jpg'),
(4, 3, 'KTP', 'gallery/dokumen/Screenshot_from_2020-03-19_17-30-07.png'),
(5, 5, 'KARTU KELUARGA', 'gallery/dokumen/kk.jpg'),
(6, 5, 'SURAT NIKAH', 'gallery/dokumen/surat_nikah.jpg'),
(7, 5, 'SERTIFIKAT', 'gallery/dokumen/HM_page-0001.jpg'),
(8, 5, 'SERTIFIKAT', 'gallery/dokumen/HM_page-0002.jpg'),
(9, 5, 'KTP', 'gallery/dokumen/HM_page-0003.jpg'),
(10, 5, 'SERTIFIKAT', 'gallery/dokumen/HM_page-0004.jpg'),
(11, 5, 'KTP', 'gallery/dokumen/HM_page-0005.jpg'),
(12, 5, 'KTP', 'gallery/dokumen/HM_page-0006.jpg'),
(13, 5, 'SPPT/PBB', 'gallery/dokumen/SPPT.jpg'),
(14, 6, 'KTP', 'gallery/dokumen/ktp.jpg'),
(15, 6, 'KARTU KELUARGA', 'gallery/dokumen/kk1.jpg'),
(16, 6, 'SURAT NIKAH', 'gallery/dokumen/surat_nikah1.jpg'),
(17, 6, 'SERTIFIKAT', 'gallery/dokumen/GB_116_page-0001.jpg'),
(18, 6, 'SERTIFIKAT', 'gallery/dokumen/GB_116_page-0002.jpg'),
(19, 6, 'SERTIFIKAT', 'gallery/dokumen/GB_116_page-0003.jpg'),
(20, 6, 'SERTIFIKAT', 'gallery/dokumen/GB_116_page-0004.jpg'),
(21, 6, 'SERTIFIKAT', 'gallery/dokumen/GB_116_page-0005.jpg'),
(22, 6, 'SPPT/PBB', 'gallery/dokumen/sppt.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `2_0_proses_order_customer`
--

CREATE TABLE `2_0_proses_order_customer` (
  `NO_ID_PROSES_ORDER_CUSTOMER` int(15) NOT NULL,
  `NO_ID_OBYEK_ORDER_CUSTOMER` int(15) DEFAULT NULL,
  `NO_ID_JENISORDER` int(10) DEFAULT NULL,
  `NO_ID_PROSES_ORDER` int(10) DEFAULT NULL,
  `NAMA_PROSES_ORDER` varchar(250) DEFAULT NULL,
  `NOMOR_AKTA` int(25) DEFAULT NULL,
  `TGL_AKTA` date DEFAULT NULL,
  `JAM` varchar(10) DEFAULT NULL,
  `ID_USER` int(5) DEFAULT NULL,
  `NOMINAL_BIAYA_PROSES` int(10) DEFAULT NULL,
  `STATUS_PROSES_ORDER_CUSTOMER` varchar(50) DEFAULT NULL,
  `TGLSTTS_PROSES_ORDER_CUSTOMER` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `2_0_proses_order_customer`
--

INSERT INTO `2_0_proses_order_customer` (`NO_ID_PROSES_ORDER_CUSTOMER`, `NO_ID_OBYEK_ORDER_CUSTOMER`, `NO_ID_JENISORDER`, `NO_ID_PROSES_ORDER`, `NAMA_PROSES_ORDER`, `NOMOR_AKTA`, `TGL_AKTA`, `JAM`, `ID_USER`, `NOMINAL_BIAYA_PROSES`, `STATUS_PROSES_ORDER_CUSTOMER`, `TGLSTTS_PROSES_ORDER_CUSTOMER`) VALUES
(1, 2, 1, 1, 'Pembuatan Akta SKMHT', NULL, NULL, NULL, 2, 300000, 'Selesai', NULL),
(2, 2, 1, 2, 'Pembuatan Akta APHT', NULL, NULL, NULL, 2, 0, 'Selesai', NULL),
(3, 2, 1, 3, 'Pelaksana Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, 2, 1000000, 'Dalam Proses', NULL),
(4, 2, 1, 4, 'Kepala Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(5, 2, 1, 5, 'Kepala Seksi Hubungan Hukum Pertanahan', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(6, 2, 1, 6, 'Pelaksana Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(7, 2, 1, 7, 'Loket Penyanan Penyerahan', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(8, 2, 1, 8, 'Pembayaran Biaya Akta dan Sertifikat HT', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(9, 3, 2, 9, 'Pembuatan Akta Jual Beli (AJB)', NULL, NULL, NULL, NULL, 1, 'Selesai', '2020-02-03 00:00:00'),
(10, 3, 2, 10, 'SPS PNBP', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(11, 3, 2, 11, 'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(12, 3, 2, 12, 'Kepala Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(13, 3, 2, 13, 'Kepala Seksi Hubungan Hukum Pertanahan', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(14, 3, 2, 14, 'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(15, 3, 2, 15, 'Loket Pelayanan Penyerahan', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(16, 3, 2, 16, 'Pendaftaran', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(17, 3, 2, 17, 'Pelaksana Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(18, 3, 2, 18, 'Kepala Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(19, 4, 1, 1, 'Pembuatan Akta SKMHT', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(20, 4, 1, 2, 'Pembuatan Akta APHT', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(21, 4, 1, 3, 'Pelaksana Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(22, 4, 1, 4, 'Kepala Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(23, 4, 1, 5, 'Kepala Seksi Hubungan Hukum Pertanahan', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(24, 4, 1, 6, 'Pelaksana Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(25, 4, 1, 7, 'Loket Penyanan Penyerahan', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(26, 4, 1, 8, 'Pembayaran Biaya Akta dan Sertifikat HT', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(27, 5, 1, 1, 'Pembuatan Akta SKMHT', NULL, '2020-03-09', NULL, NULL, 0, 'Dalam Proses', '2020-03-03 00:00:00'),
(28, 5, 1, 2, 'Pembuatan Akta APHT', NULL, '2020-03-09', NULL, NULL, 0, NULL, NULL),
(29, 5, 1, 3, 'Pelaksana Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(30, 5, 1, 4, 'Kepala Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(31, 5, 1, 5, 'Kepala Seksi Hubungan Hukum Pertanahan', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(32, 5, 1, 6, 'Pelaksana Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(33, 5, 1, 7, 'Loket Penyanan Penyerahan', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(34, 5, 1, 8, 'Pembayaran Biaya Akta dan Sertifikat HT', NULL, '2020-03-09', NULL, NULL, 0, NULL, NULL),
(35, 6, 1, 1, 'Pembuatan Akta SKMHT', NULL, '2020-04-10', NULL, NULL, 1000000, 'Selesai', '2020-04-11 00:00:00'),
(36, 6, 1, 2, 'Pembuatan Akta APHT', NULL, '2020-04-10', NULL, NULL, 1000000, 'Selesai', '2020-04-10 00:00:00'),
(37, 6, 1, 3, 'Pelaksana Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 1000000, NULL, NULL),
(38, 6, 1, 4, 'Kepala Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 1000000, NULL, NULL),
(39, 6, 1, 5, 'Kepala Seksi Hubungan Hukum Pertanahan', NULL, NULL, NULL, NULL, 1000000, NULL, NULL),
(40, 6, 1, 6, 'Pelaksana Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 1000000, NULL, NULL),
(41, 6, 1, 7, 'Loket Penyanan Penyerahan', NULL, NULL, NULL, NULL, 1000000, NULL, NULL),
(42, 6, 1, 8, 'Pembayaran Biaya Akta dan Sertifikat HT', NULL, NULL, NULL, NULL, 1000000, NULL, NULL),
(43, 7, 2, 9, 'Pembuatan Akta Jual Beli (AJB)', NULL, '2020-04-20', NULL, NULL, 0, 'Dalam Proses', '2020-04-20 00:00:00'),
(44, 7, 2, 10, 'SPS PNBP', NULL, NULL, NULL, 12, 0, NULL, NULL),
(45, 7, 2, 11, 'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(46, 7, 2, 12, 'Kepala Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(47, 7, 2, 13, 'Kepala Seksi Hubungan Hukum Pertanahan', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(48, 7, 2, 14, 'Pelaksana Subseksi Pemeliharaan Data Hak Tanah dan Pembinaan PPAT', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(49, 7, 2, 15, 'Loket Pelayanan Penyerahan', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(50, 7, 2, 16, 'Pendaftaran', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(51, 7, 2, 17, 'Pelaksana Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(52, 7, 2, 18, 'Kepala Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(53, 8, 1, 1, 'Pembuatan Akta SKMHT', NULL, NULL, NULL, NULL, 20000, NULL, NULL),
(54, 8, 1, 2, 'Pembuatan Akta APHT', NULL, NULL, NULL, NULL, 5000, NULL, NULL),
(55, 8, 1, 3, 'Pelaksana Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 3000, NULL, NULL),
(56, 8, 1, 4, 'Kepala Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 6000, NULL, NULL),
(57, 8, 1, 5, 'Kepala Seksi Hubungan Hukum Pertanahan', NULL, NULL, NULL, NULL, 57777, NULL, NULL),
(58, 8, 1, 6, 'Pelaksana Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 567567, NULL, NULL),
(59, 8, 1, 7, 'Loket Penyanan Penyerahan', NULL, NULL, NULL, NULL, 56567, NULL, NULL),
(60, 8, 1, 8, 'Pembayaran Biaya Akta dan Sertifikat HT', NULL, NULL, NULL, NULL, 778704, NULL, NULL),
(61, 10, 1, 1, 'Pembuatan Akta SKMHT', NULL, NULL, NULL, NULL, 400, NULL, NULL),
(62, 10, 1, 2, 'Pembuatan Akta APHT', NULL, NULL, NULL, NULL, 300, NULL, NULL),
(63, 10, 1, 3, 'Pelaksana Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 300, 'Selesai', '2020-06-08 00:00:00'),
(64, 10, 1, 4, 'Kepala Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 300, NULL, NULL),
(65, 10, 1, 5, 'Kepala Seksi Hubungan Hukum Pertanahan', NULL, NULL, NULL, NULL, 286, NULL, NULL),
(66, 10, 1, 6, 'Pelaksana Subseksi Pendaftaran Hak Tanah', NULL, NULL, NULL, NULL, 300, NULL, NULL),
(67, 10, 1, 7, 'Loket Penyanan Penyerahan', NULL, NULL, NULL, NULL, 300, NULL, NULL),
(68, 10, 1, 8, 'Pembayaran Biaya Akta dan Sertifikat HT', NULL, NULL, NULL, NULL, 300, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `member_menu`
--

CREATE TABLE `member_menu` (
  `MEMBER_ID` int(11) DEFAULT NULL,
  `MENU_ID` tinyint(3) DEFAULT NULL,
  `NO_ID_MEMBER_MENU` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member_menu`
--

INSERT INTO `member_menu` (`MEMBER_ID`, `MENU_ID`, `NO_ID_MEMBER_MENU`) VALUES
(1, 1, 1),
(1, 2, 2),
(1, 3, 3),
(1, 4, 4),
(1, 5, 5),
(1, 6, 6),
(1, 58, 157),
(1, 8, 8),
(1, 9, 9),
(1, 10, 10),
(1, 11, 11),
(1, 12, 12),
(1, 13, 13),
(1, 14, 14),
(1, 15, 15),
(1, 16, 16),
(1, 17, 17),
(1, 18, 18),
(1, 19, 19),
(1, 20, 20),
(1, 21, 21),
(2, 59, 159),
(2, 39, 160),
(1, 56, 156),
(2, 56, 158),
(1, 26, 26),
(1, 27, 27),
(1, 28, 28),
(1, 29, 29),
(1, 30, 30),
(1, 31, 31),
(1, 32, 32),
(1, 33, 33),
(1, 34, 34),
(16, 1, 38),
(16, 2, 39),
(16, 4, 40),
(16, 5, 41),
(16, 6, 42),
(16, 7, 43),
(16, 18, 44),
(16, 19, 45),
(16, 20, 46),
(16, 21, 47),
(16, 27, 48),
(16, 28, 49),
(16, 32, 50),
(16, 33, 51),
(16, 34, 52),
(1, 40, 155),
(1, 55, 154),
(6, 55, 153),
(6, 3, 152),
(6, 5, 57),
(1, 60, 151),
(1, 59, 150),
(6, 8, 60),
(6, 9, 61),
(6, 10, 62),
(6, 11, 63),
(6, 12, 64),
(6, 13, 65),
(6, 14, 66),
(6, 15, 67),
(6, 16, 68),
(6, 17, 69),
(6, 18, 70),
(6, 19, 71),
(6, 20, 72),
(6, 21, 73),
(7, 3, 149),
(6, 53, 148),
(6, 4, 147),
(6, 1, 146),
(6, 26, 78),
(6, 27, 79),
(6, 28, 80),
(6, 29, 81),
(6, 30, 82),
(6, 31, 83),
(6, 32, 84),
(6, 33, 85),
(6, 34, 86),
(8, 2, 110),
(8, 1, 109),
(43, 3, 108),
(43, 5, 107),
(43, 2, 106),
(43, NULL, 105),
(43, 15, 104),
(43, 7, 103),
(43, 6, 102),
(43, 1, 100),
(43, 28, 101),
(8, 3, 111),
(8, 29, 123),
(8, 5, 113),
(8, 6, 114),
(8, 7, 115),
(8, 8, 116),
(8, 9, 117),
(8, 10, 118),
(8, 11, 119),
(8, 12, 120),
(8, 13, 121),
(8, 14, 122),
(8, 30, 124),
(8, 31, 125),
(8, 28, 126),
(2, 1, 127),
(2, 2, 128),
(2, 6, 129),
(2, 7, 130),
(2, 3, 131),
(2, 25, 144),
(2, 17, 133),
(2, 5, 134),
(2, 28, 135),
(4, 1, 136),
(4, 4, 141),
(4, 3, 140),
(4, 2, 139),
(1, 53, 142),
(1, 39, 143),
(2, 4, 145),
(2, 58, 161),
(2, 40, 162),
(8, 56, 163),
(9, 1, 164),
(9, 3, 165),
(9, 56, 166),
(10, 1, 167),
(10, 3, 168),
(10, 55, 169),
(11, 1, 170),
(11, 3, 171),
(11, 56, 172),
(12, 1, 173),
(12, 3, 174),
(12, 55, 175),
(13, 1, 176),
(13, 3, 177),
(13, 55, 189),
(17, 1, 179),
(17, 61, 180),
(18, 61, 181),
(2, 62, 182),
(2, 63, 183),
(11, 39, 184),
(11, 58, 185),
(2, 55, 186),
(4, 55, 188),
(15, 1, 190),
(15, 3, 191),
(15, 55, 192);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `parent_id` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(100) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL DEFAULT '',
  `menu_order` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `icon` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `parent_id`, `title`, `url`, `menu_order`, `icon`) VALUES
(1, 0, 'HOME', 'Home', 1, 'fa-home'),
(3, 0, 'ORDER', 'Customer', 2, 'fa-cart-plus'),
(55, 3, 'Proses', 'ProsesPengikatan', 2, 'fa-american-sign-language-interpreting'),
(40, 39, 'USER', 'Menu_user', 1, 'fa-users'),
(39, 0, 'ADMINISTRATOR', '#', 4, 'fa-database'),
(59, 0, 'LAPORAN', 'Laporan', 3, 'fa-print'),
(56, 3, 'Order', 'Customer', 1, 'fa-cart-plus'),
(60, 0, 'MENU INDUK', 'Menu_utama', 5, 'fa-list'),
(58, 39, 'DOC PENGIKATAN', 'Jenis_order', 3, 'fa-star'),
(61, 0, 'Login Killer', 'Login_killer', 0, NULL),
(63, 0, 'POST HARGA', 'ProsesPengikatan/List_proses', 9, 'fa fa-money'),
(64, 39, 'TUGAS KARYAWAN', 'Bagi_menu', 2, 'fa-list');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `0_0_jenis_order`
--
ALTER TABLE `0_0_jenis_order`
  ADD PRIMARY KEY (`NO_ID_JENISORDER`);

--
-- Indexes for table `0_0_proses_order`
--
ALTER TABLE `0_0_proses_order`
  ADD PRIMARY KEY (`NO_ID_PROSES_ORDER`);

--
-- Indexes for table `0_1_data_customer`
--
ALTER TABLE `0_1_data_customer`
  ADD PRIMARY KEY (`NO_ID_CUSTOMER`);

--
-- Indexes for table `0_1_detail_customer`
--
ALTER TABLE `0_1_detail_customer`
  ADD PRIMARY KEY (`NO_ID_DETAIL_CUSTOMER`);

--
-- Indexes for table `0_2_data_saksi`
--
ALTER TABLE `0_2_data_saksi`
  ADD PRIMARY KEY (`NO_ID_SAKSI`);

--
-- Indexes for table `0_user`
--
ALTER TABLE `0_user`
  ADD PRIMARY KEY (`ID_USER`);

--
-- Indexes for table `1_0_data_order_customer`
--
ALTER TABLE `1_0_data_order_customer`
  ADD PRIMARY KEY (`NO_ID_ORDER_CUSTOMER`);

--
-- Indexes for table `1_1_detail_order_customer`
--
ALTER TABLE `1_1_detail_order_customer`
  ADD PRIMARY KEY (`NO_ID_DETAIL_ORDER`);

--
-- Indexes for table `1_2_detail_obyek_order_customer`
--
ALTER TABLE `1_2_detail_obyek_order_customer`
  ADD PRIMARY KEY (`NO_ID_OBYEK_ORDER_CUSTOMER`);

--
-- Indexes for table `1_3_dokumen`
--
ALTER TABLE `1_3_dokumen`
  ADD PRIMARY KEY (`NO_ID_DOKUMEN`);

--
-- Indexes for table `2_0_proses_order_customer`
--
ALTER TABLE `2_0_proses_order_customer`
  ADD PRIMARY KEY (`NO_ID_PROSES_ORDER_CUSTOMER`);

--
-- Indexes for table `member_menu`
--
ALTER TABLE `member_menu`
  ADD PRIMARY KEY (`NO_ID_MEMBER_MENU`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `0_0_jenis_order`
--
ALTER TABLE `0_0_jenis_order`
  MODIFY `NO_ID_JENISORDER` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `0_0_proses_order`
--
ALTER TABLE `0_0_proses_order`
  MODIFY `NO_ID_PROSES_ORDER` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `0_1_data_customer`
--
ALTER TABLE `0_1_data_customer`
  MODIFY `NO_ID_CUSTOMER` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `0_1_detail_customer`
--
ALTER TABLE `0_1_detail_customer`
  MODIFY `NO_ID_DETAIL_CUSTOMER` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_2_data_saksi`
--
ALTER TABLE `0_2_data_saksi`
  MODIFY `NO_ID_SAKSI` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `0_user`
--
ALTER TABLE `0_user`
  MODIFY `ID_USER` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `1_0_data_order_customer`
--
ALTER TABLE `1_0_data_order_customer`
  MODIFY `NO_ID_ORDER_CUSTOMER` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `1_1_detail_order_customer`
--
ALTER TABLE `1_1_detail_order_customer`
  MODIFY `NO_ID_DETAIL_ORDER` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `1_2_detail_obyek_order_customer`
--
ALTER TABLE `1_2_detail_obyek_order_customer`
  MODIFY `NO_ID_OBYEK_ORDER_CUSTOMER` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `1_3_dokumen`
--
ALTER TABLE `1_3_dokumen`
  MODIFY `NO_ID_DOKUMEN` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `2_0_proses_order_customer`
--
ALTER TABLE `2_0_proses_order_customer`
  MODIFY `NO_ID_PROSES_ORDER_CUSTOMER` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `member_menu`
--
ALTER TABLE `member_menu`
  MODIFY `NO_ID_MEMBER_MENU` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
