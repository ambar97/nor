-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 24 Des 2019 pada 16.34
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `i_norsys`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `1_1_detail_order_customer`
--

CREATE TABLE `1_1_detail_order_customer` (
  `NO_ID_DETAIL_ORDER` int(15) UNSIGNED ZEROFILL NOT NULL,
  `NO_ID_ORDER_CUSTOMER` int(15) UNSIGNED ZEROFILL DEFAULT NULL,
  `NO_ID_JENISORDER` int(10) UNSIGNED ZEROFILL DEFAULT NULL,
  `DETAIL_ORDER_ATAS_NAMA` varchar(250) DEFAULT NULL,
  `PASANGAN_DETAIL_ORDER` varchar(250) NOT NULL,
  `BIODATA_PASANGAN_DETAIL_ORDER` text NOT NULL,
  `SAKSI_SATU` varchar(250) NOT NULL,
  `BIODATA_SAKSI_SATU` text NOT NULL,
  `SAKSI_DUA` varchar(250) NOT NULL,
  `BIODATA_SAKSI_DUA` text NOT NULL,
  `NOMINAL_DETAIL_ORDER` int(15) DEFAULT NULL,
  `KETERANGAN_DETAIL_ORDER` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `1_1_detail_order_customer`
--

INSERT INTO `1_1_detail_order_customer` (`NO_ID_DETAIL_ORDER`, `NO_ID_ORDER_CUSTOMER`, `NO_ID_JENISORDER`, `DETAIL_ORDER_ATAS_NAMA`, `PASANGAN_DETAIL_ORDER`, `BIODATA_PASANGAN_DETAIL_ORDER`, `SAKSI_SATU`, `BIODATA_SAKSI_SATU`, `SAKSI_DUA`, `BIODATA_SAKSI_DUA`, `NOMINAL_DETAIL_ORDER`, `KETERANGAN_DETAIL_ORDER`) VALUES
(000000000000001, 000000000000001, 0000000001, 'yuyuyuyu sdfsdfsd', 'nana', 'jaksjdlasdlknaskd', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 'AMALINDA PRATIWI', 'lahir di Banyuwangi, tanggal 21-01-2000 (dua puluh satu Januari tahun dua ribu), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Jalan Batang Hari, Rukun Tetangga 002, Rukun Warga 005, Kelurahan Penataban, Kecamatan Giri, Kabupaten Banyuwangi. Pemilik Kartu Tanda Penduduk Nomor 3510176101000001', 60000, 'Mbohh lahhhh cok'),
(000000000000002, 000000000000001, 0000000001, 'sadasd', 'bamabam', 'daskdnlabsdjbasdamsdnlahsdlk', 'INDAH RATNASARI', 'lahir di Banyuwangi, tanggal 24-01-1992 (Dua puluh empat Januari tahun seribu sembilan ratus sembilan puluh dua ), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Dusun Joyosari, Rukun Tetangga 004, Rukun Warga 002, Desa Olehsari, Kecamatan Glagah, Kabupaten Banyuwangi, pemilik Kartu Tanda Penduduk dengan Nomor Induk Kependudukan 3510156401920001', 'AMALINDA PRATIWI', 'lahir di Banyuwangi, tanggal 21-01-2000 (dua puluh satu Januari tahun dua ribu), Warga Negara Indonesia, Pegawai Pejabat Pembuat Akta Tanah, bertempat tinggal di Jalan Batang Hari, Rukun Tetangga 002, Rukun Warga 005, Kelurahan Penataban, Kecamatan Giri, Kabupaten Banyuwangi. Pemilik Kartu Tanda Penduduk Nomor 3510176101000001', 750000, 'fadfadfdf'),
(000000000000003, 000000000000001, 0000000003, 'Rangga', '', '', '', '', '', '', 14000000, 'Saya tidak tahu'),
(000000000000004, 000000000000000, 0000000005, 'Toper Gans', '', '', '', '', '', '', 2147483647, 'shhdshsdah'),
(000000000000005, NULL, 0000000003, 'Toper', '', '', '', '', '', '', 32432, 'dsfsdfds'),
(000000000000006, 000000000000006, 0000000001, 'Hiyaa', '', '', '', '', '', '', 334534, '435345'),
(000000000000008, 000000000000006, 0000000003, 'cok', '', '', '', '', '', '', 12121, '898989'),
(000000000000009, 000000000000006, 0000000001, 'kamu', '', '', '', '', '', '', 8900090, 'cok'),
(000000000000010, 000000000000003, 0000000004, 'RANGGA AKHIR APRIAN', '', '', '', '', '', '', 100000000, 'ADA DEH');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `1_1_detail_order_customer`
--
ALTER TABLE `1_1_detail_order_customer`
  ADD PRIMARY KEY (`NO_ID_DETAIL_ORDER`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `1_1_detail_order_customer`
--
ALTER TABLE `1_1_detail_order_customer`
  MODIFY `NO_ID_DETAIL_ORDER` int(15) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
